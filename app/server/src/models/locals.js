const {Schema, model} = require('mongoose');

const locals  = new Schema({
   name:{},
   descriptions:{},
   address:{},
   phone:{},
   email:{},
   hours:[{day:{},start:Date,end:Date}],
  
   sections:[{
       type : Schema.Types.ObjectId,
       ref : 'Sections'
   }],
   products:[{
    type : Schema.Types.ObjectId,
    ref : 'Products'
   }],
   img:{}
},{
    timestamps:true,
    versionKey:false
});

module.exports = model('Locals', locals );