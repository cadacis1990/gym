const { Schema, model } = require('mongoose');

const record = new Schema({

    user: {
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    local: {
        type: Schema.Types.ObjectId,
        ref: 'Locals'
    },
    section: {
        type: Schema.Types.ObjectId,
        ref: 'Sections'
    },
    product: {
        type: Schema.Types.ObjectId,
        ref: 'Products'
    },
    charge:{},
    status:{
        type:Boolean,
        default:true
    },
    last_pay:{
        type:Date
    },
    type:{
        type:String,
        default:"monthly"
    }

   
}, {
        timestamps: true,
        versionKey: false
    });

module.exports = model('Records', record);