// modelo para user
const { Schema, model } = require("mongoose");
const bcrypt = require("bcrypt");

const Users = new Schema(
  {
    firstname: {
      type: String,
      default: "",
    },
    lastname: {
      type: String,
      default: "",
    },
    fullname: {
      type: String,
      default: "",
    },
    notify: {
      type: String,
      default: "",
    },
    subscriptions: [
      {
        type: Schema.Types.ObjectId,
        ref: "Records",
      },
    ],
    email: {
      type: String,
      unique: true,
      index: true,
    },
    confirm: {
      type: String,
      default: "",
    },
    recovery: {
      type: String,
      default: "",
    },
    phone: {
      type: String,
      default: "",
    },
    attendance: {
      type: String,
      default: "",
    },
    address_line1: {
      type: String,
      default: "",
    },
    address_line2: {
      type: String,
      default: "",
    },
    avatar: {
      type: String,
      default: "false",
    },
    pay_method: [
      {
        type: Schema.Types.ObjectId,
        ref: "Payments",
      },
    ],
    pay_status: {
      type: Boolean,
      default: true,
    },
    client_status: {
      type: Boolean,
      default: false,
    },
    password: {},
    lastlogin: {},
    loged: {
      type: Boolean,
      default: false,
    },
    status: {},

    roles: [
      {
        type: Schema.Types.ObjectId,
        ref: "Roles",
      },
    ],
    stripe_cus: {
      type: String,
      default: "false",
    },
    products: [
      {
        type: Schema.Types.ObjectId,
        ref: "Products",
      },
    ],

    local: {},
    social: {
      type: Object,
      default: { fb: "", tw: "", ig: "" },
    },

    sections: [
      {
        type: Schema.Types.ObjectId,
        ref: "Sections",
      },
    ],
  },
  {
    timestamps: true,
    versionKey: false,
  }
);
//encriptar password en el save
Users.pre("save", async function save(next) {
  if (!this.isModified("password")) {
    return next();
  }
  try {
    var salt_ = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt_);
    return next();
  } catch (err) {
    return next(err);
  }
});

Users.pre("updateOne", async function save(next) {
  const data = this.getUpdate();
  if (!data.password) {
    return next();
  }

  try {
    var salt_ = await bcrypt.genSalt(10);
    data.password = await bcrypt.hash(data.password, salt_);
    this.update({}, data);
    next();
  } catch (err) {
    return next(err);
  }
});

module.exports = model("Users", Users);
