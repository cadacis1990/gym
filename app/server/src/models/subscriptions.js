import {Schema, model  } from "mongoose";

const subscriptions  = new Schema({
   name:{},
   price:{},
   type:{},
   descriptions:{},
   users: {
    type : Schema.Types.ObjectId,
    ref: 'Users'
},

},{
    timestamps:true,
    versionKey:false
});

module.exports = model('Subscriptions', subscriptions);