import express from "express";
const router = express.Router();

const {registerPaymentCash, getPayments, getPaymentsById, deletePaymentMethod, createPayment, changeCard} = require('../controller/payments.controller');


router.post('/cash',  (req, res) => {
   registerPaymentCash(req, res);
});
router.put('/changecard',  (req, res) => {
 
  changeCard(req, res);
});
router.get('/',  (req, res) => {
  getPayments(req, res);
});
router.get('/byid',  (req, res) => {
    getPaymentsById(req, res);
});

router.put('/',  (req, res) => {
   
});
router.post('/card',  (req, res) => {
  createPayment(req,res)
});

router.delete('/',  (req, res) => {
     deletePaymentMethod(req, res)
});
module.exports = router;