const express = require('express');
const router = express.Router();
const {createSection, updateSection, getSection, removeSection, getAviable, getByDate, updateHandleStatus, getActive} = require('../controller/sections.controller');
//obtener usuarios
router.get('/',  (req, res) => {
    getSection(req, res);
});
router.get('/active',  (req, res) => {
    getActive(req, res);
});
router.get('/getbydate',  (req, res) => {
    getByDate(req, res)
  });
router.put('/updstatus',  (req, res) => {
   updateHandleStatus(req, res)
 });
//crar roles
router.post('/',  (req, res) => {
    createSection(req, res);
});
//crar roles
router.put('/',  (req, res) => {
  updateSection(req, res);
});
//crar roles
router.delete('/',  (req, res) => {
  removeSection(req, res);
});



module.exports = router;