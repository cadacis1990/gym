const express = require('express');
const router = express.Router();
const {createLocals,updateLocals, getLocals,getLocalsByAdmin, removeLocals, getLocalsById, updateHour} = require('../controller/locals.controller');

//obtener usuarios
router.get('/',  (req, res) => {
  getLocals(req, res);
});
//obtener usuarios
router.get('/byid/',  (req, res) => {
  console.log("aqui");
    getLocalsById(req, res);
  });
//obtener usuarios
router.get('/getbyadmin',  (req, res) => {
    getLocalsByAdmin(req, res);
  });
//crar roles
router.post('/',  (req, res) => {
    createLocals(req, res);
});
//crar roles
router.put('/',  (req, res) => {
  updateLocals(req, res);
});
//crar roles
router.delete('/:id',  (req, res) => {
  removeLocals(req, res);
});
router.put('/updatehour',  (req, res) => {
  updateHour(req, res);
});

module.exports = router;