const mongoose = require('mongoose');
const { db_url } = require('./config.js');
//Set up default mongoose connection
(async () => {
    const db = await mongoose.connect(db_url, {
        useNewUrlParser: true,
    });
    console.log('Data base is conected to', db.connection.name);
})();