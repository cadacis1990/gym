const localsController = {};
const Locals = require('../models/locals');
const Users = require('../models/user');
const Sections = require('../models/sections');
const Products = require('../models/products');
var mongoose = require('mongoose');

localsController.createLocals = async (req, res) => {
  
    var hours = [   {day:1,start:new Date('2020-01-01 12:00'),end:new Date('2020-01-01 12:00')},
                    {day:2,start:new Date('2020-01-01 12:00'),end:new Date('2020-01-01 12:00')},
                    {day:3,start:new Date('2020-01-01 12:00'),end:new Date('2020-01-01 12:00')},
                    {day:4,start:new Date('2020-01-01 12:00'),end:new Date('2020-01-01 12:00')},
                    {day:5,start:new Date('2020-01-01 12:00'),end:new Date('2020-01-01 12:00')},
                    {day:6,start:new Date('2020-01-01 12:00'),end:new Date('2020-01-01 12:00')},
                    {day:7,start:new Date('2020-01-01 12:00'),end:new Date('2020-01-01 12:00')},
                ]

    var newlocal = new Locals({ name: req.body.name, 
                                descriptions: req.body.description, 
                                address: req.body.address, 
                                phone: req.body.phone, 
                                email: req.body.email, 
                                hours:hours });
    try {
        await newlocal.save();
        res.json({
            status: 'ok',
            message: 'Nuevo local Creado'
        })

    } catch (error) {
        res.json({
            status: 'error',
            message: 'Was imposible create new local'
        })
        console.log(error);
    }
}
localsController.updateLocals = async (req, res) => {s
    try {
        findlocal = await Locals.findOne({ _id: req.body._id });
        if (!findlocal) {
            res.status(404).json({
                status: 'error',
                message: 'User not Found'
            })
            return
        }
        await Locals.updateOne({ _id: req.body._id }, { name: req.body.name, descriptions: req.body.descriptions, address: req.body.address })
        res.json({
            status: 'ok',
            message: 'User updated'
        })
        return


    } catch (error) {
        console.log(error);
        res.status(404).json({
            status: 'error',
            message: 'It was not possible to update the Local'
        })
    }

}
localsController.getLocals = async (req, res) => {
    
    try {
        var locals = await Locals.find().populate({
            path:"sections products",   
              
                 populate:{
                    path:"users",
                    select:"firstname lastname email pay_method pay_status client_status roles products createdAt avatar" ,
                         
                             populate:{
                                 path:"roles products pay_method"
                               
                              },
                 }          
       });
    
        res.json({
            status: "ok",
            locals: locals
        })
    } catch (error) {
        res.json({
            status: 'error',
            message: 'Impossible to get the locales from the server'
        })
        console.log(error);

    }
}
localsController.getLocalsById = async (req, res) => {
   var _id = req.query._id
  
    try {
        var findlocal = await Locals.findOne({ _id:_id}).populate({
            path:"sections",   
              
                 populate:{
                    path:"users",
                    select:"fullname email pay_method pay_status client_status roles products createdAt avatar" ,
                         
                             populate:{
                                 path:"roles products"
                              },
                 }          
       })
         res.json({
             status:"ok",
             local:findlocal
         })
         return
    } catch (error) {
        res.json({
            status: 'error',
            message: 'Impossible to get the locales from the server'
        })
        console.log(error);

    }
}
localsController.getLocalsByAdmin = async (req, res) => {
    localdata = [];
    try {

        var locals = await Locals.find().populate('users sections').lean();

        var users = locals.user;
        res.json({
            status: "ok",
            locals: locals
        })
    } catch (error) {
        res.json({
            status: 'error',
            message: 'Impossible to get the locales from the server'
        })
        console.log(error);

    }
}
localsController.removeLocals = async (req, res) => {
    var id = mongoose.Types.ObjectId(req.params.id)
    try {
      
        var findlocal = await Locals.findOne({_id:id }).populate("sections products")
      
        if (!findlocal) {
            res.status(200).json({
                status: 'error',
                message: 'Local no Encontrado'
            })
            return
        }
        for (let index = 0; index < findlocal.sections.length; index++) {
            if (findlocal.sections[index].users.length > 0) {
                res.status(200).json({
                    status: 'error',
                    message: 'Este local tiene usuarios suscritos, no se puede eliminar'
                })
                return
            }
        }
        
         if (findlocal.sections.length>0) {
            for (let index = 0; index < findlocal.sections.length; index++) {
              var findSection = await Sections.findByIdAndDelete(findlocal.sections[index]) 
            }
         } 
        
        if (findlocal.products.length > 0) {
            findlocal.products.map(async(item, key)=>{
                await Products.deleteOne({_id:item._id})
                console.log("product "+item._id+" deleted");
             })
            
        }
      
        if (findlocal.sections.length > 0) {
            findlocal.sections.map( async (item, key)=>{
                await Sections.deleteOne({_id:item._id})
                console.log("section "+item._id+" deleted");
            })
        }

        await Locals.deleteOne({ _id:id })
        console.log("local "+id+" deleted");
 
        res.json({
            status: 'ok',
            message: 'Local Borrado'
        })
        return
    } catch (error) {
        console.log(error);
        res.status(200).json({
            status: 'error',
            message: 'Error para encontrar el local desde el Servidor'
        })
    }
}
localsController.updateHour = async (req, res) => {
    try {
       
       var data =  await Locals.findOne({_id:req.body._id}).select("hours")
       var news = data.hours;
         for (let index = 0; index < data.hours.length; index++) {
           
            if (data.hours[index].day == req.body.day) {
                 news.splice(index,1,{day:req.body.day, start:req.body.start, end:req.body.end})
                 await Locals.updateOne({_id:req.body._id},{hours:news})
   
                 res.json({
                     status:"ok",
                     hours:news,
                     message:"Actualizacion correcta"

                 })
                 return
            }
         }
         res.json({
            status:"error",
            message:"Imposible cambiar el horario"
        })
    } catch (error) {
        console.log(error);
        res.json({
            status:"error",
            message:"Error en el servidor al cambiar el horario"
        })
    }
}

module.exports = localsController;