import config from "../config.js";
const userController = {};
const User = require("../models/user");
const Roles = require("../models/roles");
const Locals = require("../models/locals");
const Records = require("../models/records");
const Sections = require("../models/sections");
const Products = require("../models/products");
const Payments = require("../models/payments");
const { emailConfirmToken } = require("../services/token");
const emailServices = require("../services/email");
const fs = require("fs").promises;
const path = require("path");
const services = require("../services/token");
const stripe = require("stripe")(config.stripe_pk);

//crear user tipo cliente.
userController.createUser = async (req, res) => {
  var user_role = await Roles.findOne({ name: "register" });
  user_role = user_role._id;
  var user_info = req.body.user;
  var product = req.body.user.product;
  var stripe_info = req.body.stripe.token;

  var verify_email = await User.countDocuments({ email: user_info.email });
  if (verify_email > 0) {
    res.json({
      status: "error",
      message: "Email alredy exist",
    });
    return;
  }

  try {
    const customer = await stripe.customers.create({
      name: user_info.firstname + " " + user_info.lastname,
      email: user_info.email,
      source: stripe_info.id,
      address: {
        city: user_info.city,
        country: user_info.country,
        line1: "",
        line2: "",
        state: user_info.state,
      },
    });

    const charge = await stripe.charges.create({
      amount: parseFloat(user_info.product.price) * 100,
      currency: "usd",
      customer: customer.id,
    });

    var pay_met = new Payments({ pay_method: charge.source });
    pay_met = await pay_met.save();

    const newuser = new User({
      firstname: user_info.firstname,
      lastname: user_info.lastname,
      stripe_cus: customer.id,
      email: user_info.email,
      phone: user_info.phone,
      address_line1: user_info.address_line1,
      address_line2: user_info.address_line2,
      confirm: "",
      avatar: "false",
      pay_method: pay_met._id,
      password: user_info.password,
      roles: [user_role],
      products: product._id,
      sections: product.section_id,
    });
    var local_first = await Locals.find();
    var record = new Records({
      user: newuser._id,
      last_pay: new Date().toString(),
      section: product.section_id,
      charge: parseFloat(user_info.product.price) * 100,
      product: product._id,
      local: local_first[0]._id,
    });
    newuser.subscriptions = record._id;
    await record.save();
    const confirm = emailConfirmToken(newuser._id);
    newuser.confirm = confirm;
    await newuser.save();
    await Sections.updateOne(
      { _id: product.section_id },
      { $push: { users: newuser._id } }
    );
    await Products.updateOne(
      { _id: product._id },
      { $push: { users: newuser._id } }
    );
    var byEmail = await User.findOne({ _id: newuser._id });
    await emailServices.emailConfirm(byEmail, req);
    res.json({
      status: "ok",
      message:
        "We have sent an email to " +
        newuser.email +
        " to activate his account",
    });
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: error.raw.message,
    });
  }
};
//crear usuario desde administrador.
userController.createUserByAdmin = async (req, res) => {
  //obtener body por destructuring
  const user = new User();
  const confirm = emailConfirmToken(user._id);

  const {
    firstname,
    lastname,
    email,
    phone,
    address,
    password,
    avatar,
    client_status,
    payStatus,
    product,
    section,
  } = req.body;

  user.firstname = firstname;
  user.lastname = lastname;
  user.email = email;
  user.phone = phone;
  user.address = address;
  user.password = password;
  user.avatar = avatar;
  user.client_status = client_status;
  user.pay_status = payStatus;
  user.products = product;
  user.sections = section;
  user.confirm = confirm;

  if (req.body.pay_method == "cash") {
    var payment = await Payments.find();

    for (let index = 0; index < payment.length; index++) {
      if (payment[index].pay_method.last4 == "Cash") {
        user.pay_method = payment[index]._id;
      }
    }
  } else {
    user.pay_method = [];
  }

  if (req.body.role) {
    user.roles = [req.body.role];
  } else {
    var clientRole = await Roles.findOne({ name: "register" });
    user.roles = [clientRole._id];
  }

  try {
    var findClient = await User.findOne({ email: user.email });
    if (findClient) {
      res.json({
        status: "error",
        message: "El usuario con este email ya existe",
      });
      return;
    }

    const findSection = await Sections.findById(user.sections);
    if (!findSection.status) {
      res.json({
        status: "error",
        message: "This schedule is full or blocked by the administrator",
      });
      return;
    }
    const arraySection = findSection.users;
    arraySection.push(user._id);
    const updated = await Sections.updateOne(
      { _id: user.sections },
      { users: arraySection }
    );
    if (!updated) {
      res.json({
        status: "error",
        message: "Imposible asociar el cliente a seccion",
      });
      return;
    }

    const updated_ = await Products.updateOne(
      { _id: req.body.product },
      { $push: { users: user._id } }
    );

    if (!updated_) {
      res.json({
        status: "error",
        message: "Imposible asociar el cliente a un paquete",
      });
      return;
    }
    var price = await Products.findOne({ _id: req.body.product });
    price = price.price * 100;
    var type = price.type;

    const record = new Records({
      stripe_cus: "false",
      local: req.body.local_id,
      last_pay: new Date().toString(),
      section: req.body.section,
      product: req.body.product,
      charge: price,
      type: type,
    });

    user.subscriptions = record;
    var dbresult = await user.save();
    record.user = user._id;
    await record.save();

    if (!dbresult) {
      res.json({
        status: "error",
        message: "Imposible crear el usuario en el server",
      });
      return;
    }
    await emailServices.emailConfirm(user, req);

    if (findSection.users.length - 1 >= parseInt(findSection.maxUser)) {
      await Sections.updateOne({ _id: user.sections }, { status: false });
    }
    res.json({
      status: "ok",
      message:
        "We have sent an email to " + user.email + " to activate his account",
    });
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: "Error al crear el cliente en el servidor",
    });
  }
};
//actualizar usuario y no permitir roles.
/* userController.updateUsertes = async (req, res) => {
  var id = req.body.id;
  const { fullname, email, phone, address, password } = req.body;
  var update_user = {};
  //craer opjeto para update

  if (fullname) {
    update_user.fullname = fullname;
  }
  if (email) {
    var getmyemail = await User.findOne({ _id: id });
    getmyemail = getmyemail.email;
    var findOneMail = await User.countDocuments({ email: email });

    if (findOneMail > 0 && email != getmyemail) {
      var error = "Email " + email + " Alredy Exist";
      res.json({
        status: "error",
        message: error,
      });
      return;
    }
    if (findOneMail == 0 && email != getmyemail) {
      await User.updateOne({ _id: id }, { email: "" });
      update_user.confirm = emailConfirmToken({ id });
      update_user.email = email;
    }
  }
  if (phone) {
    update_user.phone = phone;
  }
  if (address) {
    update_user.address = address;
  }

  if (password) {
    if (lastpass) {
      try {
        var validatePass = await User.findOne({ _id: id });
        validatePass = await bcrypt.compare(lastpass, validatePass.password);

        if (validatePass) {
          var salt = await bcrypt.genSalt(10);
          var hash = await bcrypt.hash(password, salt);
          update_user.password = hash;
        } else {
          res.status(401).json({
            status: "error",
            message: "Last Password is Wrong ",
          });
          return;
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      res.status(404).json({
        status: "error",
        message: "Last Password is Required",
      });
      return;
    }
  }

  try {
    await User.updateOne({ _id: id }, update_user);
    res.json({
      status: "ok",
      message: "Updated User",
    });
  } catch (error) {
    console.log(error);
  }
};
 */
//actualizar usuario y permitir roles.
userController.updateUser = async (req, res) => {
  const id = req.body.id;

  var update_user = {};
  //craer opjeto para update
  if (req.body.fullname) {
    update_user.fullname = req.body.fullname;
  }
  if (req.body.roles) {
    update_user.roles = req.body.roles;
  }
  if (req.body.email) {
    var getmyemail = await User.findOne({ _id: id });
    getmyemail = getmyemail.email;
    var findOneMail = await User.countDocuments({ email: req.body.email });

    if (findOneMail > 0 && req.body.email != getmyemail) {
      var error = "Email " + req.body.email + " Alredy Exist";
      res.json({
        status: "error",
        message: error,
      });
      return;
    }
    if (findOneMail == 0 && req.body.email != getmyemail) {
      update_user.confirm = emailConfirmToken({ id });
      update_user.email = req.body.email;
    }
  }
  if (req.body.phone) {
    update_user.phone = req.body.phone;
  }
  if (req.body.address) {
    update_user.address = req.body.address;
  }
  /* if (req.body.pay_method == "Cash") {
    var payment = Payments.findOne({ pay_method: req.body.pay_method });
    update_user.pay_method = userActionById;
  } */
  if (req.body.password) {
    update_user.password = req.body.password;
  }
  if (req.body.subscriptions) {
    update_user.subscriptions = req.body.subscriptions;
  }

  try {
    update_user = await User.updateOne({ _id: id }, update_user);

    res.json({
      status: "ok",
      message: "Usuario Actualizado",
    });
  } catch (error) {
    console.log(error);
    res.status(404).json({
      status: "error",
      message: "Error al Actualizar el usuario",
    });
  }
};
// responder todos los usuarios si no se pasa el parametro id de lo contrario responder un solo user
userController.getUser = async (req, res) => {
  if (!req.query._id) {
    try {
      var alluser = await User.find()
        .populate("products subscriptions roles sections pay_method")
        .sort({ createdAt: -1 });

      //eliminar hash de los password antes de enviar respuestas
      for (let index = 0; index < alluser.length; index++) {
        alluser[index].password = "";
      }

      //Responder con arreglo de todos los usuarios
      res.json({
        status: "ok",
        users: alluser,
      });
      return;
    } catch (error) {
      console.log(error);
      res.json({
        status: "error",
        message: "Error de proceso en el server",
      });
    }
  }

  try {
    var users = await User.findById({ _id: req.query._id }).populate([
      {
        path: "products roles sections pay_method",
      },
      {
        path: "subscriptions",
        populate: {
          path: "local section product",
        },
      },
    ]);
    //eliminar hash  password antes de enviar respuestas
    users.avatar = "/uploads/avatar/" + users.avatar;
    users.password = "";
    //Responder solo usuario
    res.json({
      status: "ok",
      users,
    });
  } catch (error) {
    res.status(404).json({
      status: "error",
      message: error.message,
    });
    console.log(error);
  }
};
// responder todos los usuarios si no se pasa el parametro id de lo contrario responder un solo user
userController.getUserByLocal = async (req, res) => {
  try {
    var _id = req.query.id;
    var locals = await Locals.find({ _id: _id })
      .populate({
        path: "sections",
        populate: {
          path: "users",
          select: "-password",
          populate: { path: "products sections pay_method" },
        },
      })
      .sort({ createdAt: -1 });
    var users = [];
    locals.map((value, key) => {
      value.sections.map((value, key) => {
        value.users.map((value, key) => {
          users.push(value);
          return value;
        });
        return value;
      });
      return value;
    });
    //ordenar

    users = users.sort(function (a, b) {
      if (a.createdAt > b.createdAt) {
        return -1;
      }
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      // a must be equal to b
      return 0;
    });
    //eliminar hash de los password antes de enviar respuestas
    res.json({
      status: "ok",
      users,
    });
    return;
    //Responder con arreglo de todos los usuarios
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: "Error de proceso en el server",
    });
  }
};
userController.getStatus = async (req, res) => {
  try {
    if (req.headers.jwt == "undefined") {
      res.json({
        status: "error",
        message: "Invalid Token",
        user: {
          roles: [{ name: "client" }],
        },
      });
      return;
    }

    var data = services.decodeToken(req.headers.jwt);
    if (!data.id) {
      res.json({
        status: "error",
        message: "Invalid Token",
        user: {
          roles: [{ name: "client" }],
        },
      });
      return;
    }

    var user = await User.findById({ _id: data.id }).populate([
      {
        path: "products roles sections pay_method",
      },
      {
        path: "subscriptions",
        populate: {
          path: "local section product",
        },
      },
    ]);

    //eliminar hash  password antes de enviar respuestas

    if (!user) {
      res.json({
        status: "ok",
        user: {
          roles: [{ name: "client" }],
        },
      });
      return;
    }

    user.avatar = "/uploads/avatar/" + user.avatar;
    user.password = "";

    res.json({
      status: "ok",
      user: user,
    });
  } catch (error) {
    res.json({
      status: "error",
      message: "Server Error",
      user: {
        roles: [{ name: "client" }],
      },
    });
    console.log(error);
    return;
  }
};
userController.getUserBySections = async (req, res) => {
  try {
    var section = await Sections.findOne({
      _id: req.body.sections_id,
    }).populate("users");
    var usersarr = [];
    var start = new Date();
    start.setHours(0, 0, 0, 0);
    var end = new Date();
    end.setHours(23, 59, 59, 999);

    for (let i = 0; i < section.users.length; i++) {
      var result = await User.findOne({ _id: section.users[i]._id })
        .select("fullname")
        .select("avatar")
        .select("email")
        .select("attendance");
      var check_record = await Records.findOne({
        user: section.users[i]._id,
        section: section._id,
        createdAt: { $gte: start, $lt: end },
      });

      if (check_record) {
        result.attendance = 1;
      } else {
        result.attendance = 0;
      }

      usersarr.push(result);
    }
    res.json({
      status: "ok",
      users: usersarr,
    });
  } catch (error) {
    console.log(error);
  }
};
//delete user
userController.deleteUser = async (req, res, id) => {
  try {
    var _id = req.query._id;
    var count = await User.countDocuments({ _id: _id });
    var user = await User.findById(_id);

    if (count <= 0) {
      res.json({
        status: "error",
        message: "No se encuentra el usuario",
      });
      return;
    }

    if (user.products.length > 0) {
      res.json({
        status: "error",
        message:
          "El usuario esta activo en una suscripcion debe cancelarla antes",
      });
      return;
    }

    await Sections.updateMany({}, { $pull: { users: _id } });
    await Products.updateMany({}, { $pull: { users: _id } });
    await User.deleteOne({ _id: _id });
    res.json({
      status: "ok",
      message: "Usuario Eliminado",
    });
  } catch (error) {
    console.log("Delete User is not posible this is the error: " + error);
    res.json({
      status: "error",
      message: "Is not posible deleted user by Id: " + _id,
    });
  }
};
userController.userStatusSwitch = async (req, res) => {
  var _id = req.query._id;

  var user = await User.findById(_id);
  var status = user.client_status;

  if (!user) {
    res.json({
      status: "error",
      message: "Este usuario puede haber sido eliminado",
    });
    return;
  }
  await User.updateOne({ _id: _id }, { client_status: !status });
  res.json({
    status: "ok",
    message: status
      ? "El usuario " + user.fullname + " termino su entrenamiento"
      : "El usuario " + user.fullname + " comenzo su entrenamiento",
  });
};
userController.userPaySwitch = async (req, res) => {
  var _id = req.query._id;

  var user = await User.findById(_id);
  var pay_status = user.pay_status;

  if (!user) {
    res.json({
      status: "error",
      message: "Este usuario puede haber sido eliminado",
    });
    return;
  }
  await User.updateOne({ _id: _id }, { pay_status: !pay_status });
  res.json({
    status: "ok",
    message: pay_status
      ? "El usuario " + user.fullname + " ahora esta pendiente de pago"
      : "El usuario " + user.fullname + " ahora esta como pagado",
  });
};
userController.userRemoveSuscriptions = async (req, res) => {
  if (!req.query.id || !req.query.product) {
    res.json({
      status: "error",
      message: "Id no existe",
    });
    return;
  }
  var user = req.query.user;
  var suscription = req.query.product;
  var idRecord = req.query.id;
  var section = req.query.section;

  try {
    var payme = await User.findOne({ _id: user });

    var paymet = payme.pay_method[0];

    var stripe_cus = payme.stripe_cus;

    var cardsave = await Payments.findOne({ _id: paymet });

    cardsave = cardsave.pay_method.id;

    if (stripe_cus != "false") {
      await stripe.customers.deleteSource(stripe_cus, cardsave);
    }

    await Records.deleteOne({ _id: idRecord });
    await Payments.deleteOne({ _id: payme.pay_method[0] });
    await Products.findOneAndUpdate(
      { _id: suscription },
      { $pull: { users: user } }
    );
    await Sections.findOneAndUpdate(
      { _id: section },
      { $pull: { users: user } }
    );
    await User.findOneAndUpdate(
      { products: { $elemMatch: { $in: suscription } } },
      { $pull: { products: suscription } }
    );
    await User.findOneAndUpdate(
      { subscriptions: { $elemMatch: { $in: idRecord } } },
      { $pull: { subscriptions: idRecord } }
    );
    await User.findOneAndUpdate(
      { sections: { $elemMatch: { $in: section } } },
      { $pull: { sections: section } }
    );
    await User.findOneAndUpdate({ _id: user }, { pay_method: [] });

    await User.updateOne({ _id: user }, { $pull: { products: user } });
    res.json({
      status: "ok",
      message: "Suscripcion Eliminada ",
    });
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: "Error en el server",
    });
  }
};
userController.userUploadFile = async (req, res) => {
  var id = req.body.id;

  try {
    var query = await User.findOne({ _id: id });
    var lastavatar = query.avatar;

    if (lastavatar != "false") {
      await User.updateOne({ _id: id }, { avatar: req.file.filename });
      res.json({
        status: "ok",
        message: "Avatar actualizado con exito",
      });

      await fs.unlink(
        path.join(__dirname, "../../../../build/uploads/avatar/" + lastavatar)
      );
      return;
    }
    await User.updateOne({ _id: id }, { avatar: req.file.filename });
    res.json({
      status: "ok",
      message: "Avatar actualizado con exito",
    });
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: "Error al actualizar el avatar",
    });
  }
};

userController.deletePayment = async (req, res) => {
  if (!req.query._id || !req.query.user_id) {
    res.json({
      status: "error",
      message: "Parametros Incorrectos",
    });
    return;
  }

  var user_id = req.query.user_id;
  var method = req.query._id;
  var user = await User.findOne({ _id: user_id });

  if (!user) {
    res.json({
      status: "error",
      message: "El usuario ya no existe",
    });
    return;
  }
  //Eliminar de stripe esperar respuesta y luego eliminar de la db

  var delPayment = await User.updateOne(
    { _id: user_id },
    { $pull: { pay_method: method } }
  );

  if (delPayment.modifiedCount < 1) {
    res.json({
      status: "error",
      message: "Imposible actualizar en el server",
    });
    return;
  }
  res.json({
    status: "ok",
    message: "Metodo de Pago Eliminado",
  });
};

userController.addProduct = async (req, res) => {
  const {
    section,
    local,
    product,
    id,
    name,
    email,
    city,
    country,
    line1,
    line2,
    state,
  } = req.body.user;
  const { token } = req.body.stripe;

  try {
    var section_db = await Sections.findOne({ _id: section });
    await User.findOne({ _id: id });

    if (section_db.users.length >= section_db.maxUser) {
      res.json({
        status: "error",
        message: "This training hour is full",
      });
      return;
    }

    const countRecordSimilar = await Records.countDocuments({
      user: id,
      product: product,
      section: section,
    });
    if (countRecordSimilar > 0) {
      res.json({
        status: "error",
        message: "you are already subscribed to this product",
      });
      return;
    }
    var cus = await User.findOne({ _id: id });
    cus = cus.stripe_cus;

    if (cus == "false") {
      const customer = await stripe.customers.create({
        name: name,
        email: email,
        source: token.id,
        address: {
          city: city,
          country: country,
          line1: line1,
          line2: line2,
          state: state,
        },
      });

      var price = await Products.findOne({ _id: product });
      price = price.price * 100;
      var type = price.type;

      const charge = await stripe.charges.create({
        amount: price,
        currency: "usd",
        customer: customer.id,
      });

      var pay_met = new Payments({ pay_method: charge.source });
      pay_met = await pay_met.save();

      const record = new Records({
        user: id,
        local: local,
        product: product,
        section: section,
        charge: price,
        type: type,
        last_pay: new Date().toString(),
        status: true,
      });
      await record.save();

      await User.updateOne(
        { _id: id },
        {
          $push: {
            sections: section,
            products: product,
            subscriptions: record._id,
          },
          stripe_cus: customer.id,
          pay_method: pay_met._id,
        }
      );

      await Sections.updateOne({ _id: section }, { $push: { users: id } });
      await Products.updateOne({ _id: product }, { $push: { users: id } });

      res.json({
        status: "ok",
        message: "Subscription successfully",
      });

      return;
    }

    const card = await stripe.customers.createSource(cus, { source: token.id });

    pay_met = new Payments({ pay_method: card });
    pay_met = await pay_met.save();

    price = await Products.findOne({ _id: product });
    price = price.price * 100;
    type = price.type;

    const record = new Records({
      user: id,
      local: local,
      product: product,
      section: section,
      charge: price,
      type: type,
      last_pay: new Date().toString(),
      status: true,
    });
    await record.save();

    await User.updateOne(
      { _id: id },
      {
        $push: {
          sections: section,
          products: product,
          subscriptions: record._id,
        },
        pay_method: pay_met._id,
      }
    );
    await Sections.updateOne({ _id: section }, { $push: { users: id } });
    await Products.updateOne({ _id: product }, { $push: { users: id } });

    res.json({
      status: "ok",
      message: "Subscription successfully",
    });
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: "Server Error",
    });
  }
};
userController.activeSuscribe = async (req, res) => {
  const { stripe_cus, product, _id } = req.body;

  var price = await Products.findOne({ _id: product });
  price = parseInt(price.price) * 100;

  try {
    await stripe.charges.create({
      amount: price,
      currency: "usd",
      customer: stripe_cus,
    });

    await Records.updateOne(
      { _id: _id },
      { last_pay: new Date().toString(), status: true }
    );
    res.json({
      status: "ok",
      message: "Your subscription has been successfully activated",
    });
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: "Error when charging your card",
    });
  }
};
userController.getTrainers = async (req, res) => {
  try {
    var trainersList = [];
    var trainers = await User.find().select("-password").populate("roles");
    trainers.map((item_, key) => {
      item_.roles.map((item, key) => {
        if (item.name == "employer") {
          trainersList.push(item_);
        }
        return item;
      });
      return item_;
    });
    trainersList = trainersList.map((item, key) => {
      item.avatar = "uploads/avatar/" + item.avatar;
      return item;
    });
    console.log(trainersList);
    res.json({
      status: "ok",
      trainers: trainersList,
    });
  } catch (error) {
    console.log(error);
    res.json({
      status: "error",
      message: "Server Error",
      trainers: [],
    });
  }
};

module.exports = userController;
