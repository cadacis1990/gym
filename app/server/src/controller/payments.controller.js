const paymentsController = {};
import Payments from "../models/payments";
import Users from "../models/user";
import config from '../config.js'
const stripe = require('stripe')(config.stripe_pk);

paymentsController.createPayment = async (req, res) => {
    const user     = req.body.user
    const email    = req.body.email  
    const token    = req.body.stripe.token.id
    const user_info= req.body.data  
try {
   
    const customers = await stripe.customers.list();
    const count = customers.data.filter((item,key)=>{
          return item.email == email
    })

   /*  if (count.length < 1) {
        const customer = await stripe.customers.create({
            name:user_info.firstname + " " + user_info.lastname,
            email:email,
            source:token,
            address:{
                city:user_info.city,
                country:user_info.country,
                line1:user_info.address_line1,
                line2:user_info.address_line2,
                state:user_info.state
            }
        });
        const card = await stripe.customers.createSource(
            getCus,
            {source: token}
        ); 
        const newMethod =  new Payments({status:true,pay_method:card})
        newMethod.save() 
        await Users.updateOne({_id:user},{$push:{pay_method:newMethod._id}})  
        res.json({
            status:"ok",
            message:"Added payment method"
        })
        return
    } */

    var getCus = await Users.findOne({_id:user})
        getCus = getCus.stripe_cus
 
    const card = await stripe.customers.createSource(
        getCus,
        {source: token}
    ); 

    const newMethod =  new Payments({status:true,pay_method:card})
          newMethod.save() 
    await Users.updateOne({_id:user},{$push:{pay_method:newMethod._id}})      
    
    res.json({
        status:"ok",
        message:"Added payment method"
    })

} catch (error) {
   console.log(error);
    res.json({
        status:"error",
        message:"It was impossible to add the payment method"
    })

} 

}

paymentsController.registerPaymentCash = async (req, res) => {

    transaction = new Payments({user_id:req.body.user_id, method:'cash'});
    try {
        transaction.save();
        res.json({
            status:'ok',
            message:'Registered cash payment',
        })
    } catch (error) {
        console.log(error);

    }

 
} 

paymentsController.getPayments = async (req, res) => {
    try {
        var query = {};
        if (req.body.user_id) {
            query.user_id = req.body.user_id
        }
        if (req.body.method) {
            query.method = req.body.method
        }
        var payments = await Payments.find(query);
        res.json({
            status:'ok',
            paymentsLog:payments
        })
    } catch (error) {
        res.json({
            status: 'error',
            message:"No se pudieron obtener los usuarios"
        })
        
        console.log(error);
        
    }
}

paymentsController.getPaymentsById = async (req, res) => {
    try {
        var query = {};
            query.user_id = req.body.user_id
        var payments = await Payments.find(query).sort({createdAt:-1});

        res.json({
            status:'ok',
            paymentsLog:payments
        })
    } catch (error) {
        console.log(error);
        
    }
}

paymentsController.deletePaymentMethod= async (req, res) => {

   const paymethod = JSON.parse(req.query.pay_method)
   const id_paymethod = req.query._id
   const customer = paymethod.customer
   const card = paymethod.id
  try {
   
    const {deleted} = await stripe.customers.deleteSource(
        customer,
        card
    );

    if (!deleted) {
        res.json({
            status:"error",
            message:"Unable to delete this card, contact an administrator"
        })
        return
    }  

    await Payments.deleteOne({_id:id_paymethod})
    await Users.findOneAndUpdate({pay_method:{$elemMatch:{$in:id_paymethod}}},{$pull:{pay_method:id_paymethod}})
    res.json({
        status:"ok",
        message:"Pay Method Deleted"
    })
  } catch (error) {
      res.json({
          status:"error",
          message:"Error deleting your card"
      })
  }
  
 
}
paymentsController.changeCard = async (req, res) => {
     
try {
    
    var {stripe_cus, pay_method, user} = req.body.user
    var {token} = req.body.stripe
  
    const card = await stripe.customers.createSource(
        stripe_cus,
       {source: token.id}
    );

    var cardsave = await Payments.findOne({_id:pay_method})
        cardsave = cardsave.pay_method.id
          
    const deleted = await stripe.customers.deleteSource(
        stripe_cus,
        cardsave
      );

    var pay_met = new Payments({pay_method:card})
        pay_met = await pay_met.save()
    
    await Users.updateOne({_id:user},{pay_method:pay_met._id})
    await Payments.deleteOne({_id:pay_method})

    res.json({
        status:"ok",
        message:"Payment method Changed"
    })

} catch (error) {
    console.log(error);
    res.json({
        status:"ok",
         error:"Server Error"
    })
}
  

    
  return
}

module.exports = paymentsController;