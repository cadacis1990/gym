const rolesController = {};
const Roles  = require('../models/roles');

rolesController.getRoles = async (req, res) => {
   var roles = await Roles.find();
   res.json({
       status:"ok",
       roles:roles
   })
} 

module.exports = rolesController;