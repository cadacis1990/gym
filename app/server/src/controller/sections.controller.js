const sectionsobj = {};
const Locals = require('../models/locals');
const Users = require('../models/user');
const Sections = require('../models/sections');
const ObjectId = require('mongoose').Types.ObjectId;

//crear seccion validado para no crear secciones sobre los mismos horarios
sectionsobj.createSection = async (req, res) => {
    var start = req.body.start_time
    var end = req.body.end_time
    var local = req.body.local
    var max = req.body.max
    var day = req.body.day
     //La secciones pertenecen a un local si no se le pasa el id de el local devuelve error
     if (!req.body.local) {
        res.status(200).json({
            status: 'error',
            message: 'Local or Sections need local_id as parameter'
        })
        return
    }
    var localExist = await Locals.findOne({_id:local})
        if (!localExist) {
            res.status(200).json({
                status: 'error',
                message: 'Local  not Found'
            })
            return
        }
    if (start >= end) {
            res.json({
                status: 'error',
                message: 'El horario final siempre debe ser el mayor'
            })
            return
    }
    
      //Busca el local indicado y devuelve el array de las secciones asociadas a el

      var sectionsUsed = await Locals.findOne({_id:local}).populate("sections")

      sectionsUsed = sectionsUsed.sections;
            var start_ = new Date(start);
                start_ = parseInt(start_.getTime() ) 
            var end_ = new Date(end);
                end_ =  parseInt(end_.getTime())

      for (let i = 0; i < sectionsUsed.length; i++) {
        var st = new Date(sectionsUsed[i].start_time);
            st = parseInt(st.getTime())
        var et = new Date(sectionsUsed[i].end_time);
            et = parseInt(et.getTime())
     
      
        var day_ = sectionsUsed[i].weekDay;

         if (start_ >= st && start_ < et && day == day_) {
             res.json({
                 status: 'error',
                 message: 'Este horario coincide con otro'
             })
             return
         }
         if (end_ > st && end_ <= et && day == day_) {

             res.json({
                 status: 'error',
                 message: 'Este horario coincide con otro'
             })
             return
         } 
     }
   
    
      var newsections = new Sections({ start_time:start, end_time:end, maxUser:max, status: true, weekDay:day });
      await newsections.save();
      sectionsUsed.push(newsections);
      await Locals.updateOne({_id:local},{sections:sectionsUsed});
        res.json({
            status:"ok",
            message:"Seccion agregada con exito"
        })  
      
}
sectionsobj.getSection = async (req, res) => {
   
  
    try {
        if (!req.query.id) {
            res.status(200).json({
                status: 'error',
                message: 'Sections need local_id as parameter'
            })
            return
        }
        if (!ObjectId.isValid(req.query.id)) {
            res.status(200).json({
                status: 'error',
                message: 'Local ID Invalid'
            })
            return
        }
        var localExist = await Locals.findOne({ _id:req.query.id});
        if (!localExist) {
            res.status(200).json({
                status: 'error',
                message: 'Local not Found'
            })
            return
        }
        var local_id = req.query.id;
        var sectionsUsed = await Locals.findOne({ _id:local_id }).populate('sections');
        sectionsUsed = sectionsUsed.sections;
        var arr = [];

        for (let i = 0; i < sectionsUsed.length; i++) {
            var sec = await Sections.findOne({ _id:sectionsUsed[i], weekDay:parseInt(req.query.day)});
            if (sec != null) {
            
                sec.noUser = sec.users.length;
                arr.push(sec); 
            }
        }
        //ordenar array de horarios
        function dynamicSort(property) {
            var sortOrder = 1;
            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a,b) {
                /* next line works with strings and numbers, 
                 * and you may want to customize it to your needs
                 */
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        arr = arr.sort(dynamicSort("start_time"));

        res.json({
            status: 'ok',
            sections: arr
            
        })
    } catch (error) {
        console.log(error);
        res.json({
            status: 'error',
            message: 'Not posible get sections from server'
        })
    }

}
sectionsobj.updateHandleStatus = async (req, res) => {
   try {
        if (!req.body._id) {
           
            res.status(200).json({
                status: 'error',
                message: 'El servidor recivio un parametro incorrecto'
            })
            return
        }
        if (!ObjectId.isValid(req.body._id)) {
            res.status(200).json({
                status: 'error',
                message: 'El servidor recivio un parametro incorrecto'
            })
            return
        }
        var getStatus = await Sections.findOne({_id:req.body._id});
        await Sections.updateOne({_id:req.body._id}, {status:!getStatus.status});
    
        res.status(200).json({
            status: 'ok',
            message: 'La seccion ha sido actualizada'
        })  
   } catch (error) {
        res.status(200).json({
            status: 'error',
            message: 'Error de proceso en el servidor'
        })  
   }
    
}
sectionsobj.updateSection = async (req, res) => {
    if (!req.body.start_time || !req.body.end_time || !req.body.weekDay) {
        res.status(404).json({
            status: 'error',
            message: 'Star time, End time and Day are Required'
        })
        return
    }
    if (!req.body._id) {
        res.status(404).json({
            status: 'error',
            message: 'Section need _id as parameter'
        })
        return
    }

    if (!ObjectId.isValid(req.body._id)) {
        res.status(404).json({
            status: 'error',
            message: 'Section ID Invalid'
        })
        return
    }


    localExist = await Sections.findOne({ _id: req.body._id });
    if (!localExist) {
        res.status(404).json({
            status: 'error',
            message: 'Section not Found'
        })
        return
    }
    var startHour = req.body.start_time;
    var endHour = req.body.end_time;
    var maxUser = req.body.max_user


    //Si se le pasa el id de el local y no existe devuelve error
    localExist = await Sections.findOne({ _id: req.body._id })
    if (!localExist) {
        res.status(404).json({
            status: 'error',
            message: 'Section not Found'
        })
        return
    }

    //Comprueba que el horario inicial siempre sea menor que el horario final
    if (startHour >= endHour) {
        res.json({
            status: 'error',
            message: 'Start date must be less than end date'
        })
        return
    }

    //Busca el local indicado y devuelve el array de las secciones asociadas a el
    sectionsUsed = await Locals.findOne({ _id: req.query.id }).populate('sections');
    sectionsUsed = sectionsUsed.sections;

    //recorre el array de las secciones asociadas a el local indicado y comprueba que no exista una seccion que coincida n horario con la que queremos crear
   /*  for (let i = 0; i < sectionsUsed.length; i++) {

        st = sectionsUsed[i].start_time;
        et = sectionsUsed[i].end_time;
        day = sectionsUsed[i].weekDay;
        star_n = parseInt(startHour);
        end_n = parseInt(endHour);

        if (star_n >= st && star_n < et && day == req.body.weekDay && sectionsUsed[i]._id != req.body._id) {
            res.json({
                status: 'error',
                message: 'There is already a section created in this schedule'
            })
            return
        }
        if (end_n > st && end_n <= et && day == req.body.weekDay && sectionsUsed[i]._id != req.body._id) {

            res.json({
                status: 'error',
                message: 'There is already a section created in this schedule'
            })
            return
        }
    }
 */

    await Sections.updateOne({ _id: req.body._id }, { start_time: startHour, end_time: endHour, weekDay: req.body.weekDay });
    res.json({
        status: 'ok',
        message: 'Sections Update'
    })
}
sectionsobj.removeSection = async (req, res) => {
 
    try {
      
        if (!req.body._id) {
            res.status(200).json({
                status: 'error',
                message: 'ID de Seccion no es Correcto'
            })
            return
        }
        if (!ObjectId.isValid(req.body._id)) {
            res.status(200).json({
                status: 'error',
                message: 'ID de Seccion no es Correcto'
            })
            return
        }
        var sectionExist = await Sections.findOne({ _id:req.body._id });
        var localExist = await Locals.findOne({ _id: req.body.local_id });

        if (!sectionExist || !localExist ) {
            res.status(200).json({
                status: 'error',
                message: 'Seccion no encontrada o no relacionada'
            })
            return
        }

        if (sectionExist.users.length > 0) {
            res.status(200).json({
                status: 'error',
                message: 'Imposible eliminar esta seccion, tiene usuarios relacionados'
            })
            return
        }

        function removeItemFromArr ( arr, item ) {
            var i = arr.indexOf( item );
         
            if ( i != -1 ) {
                arr.splice( i, 1 );
            }
            return arr
        }
        
        var arResult = removeItemFromArr( localExist.sections, req.body._id );
        var resultUpdate = await Locals.updateOne({_id:req.body.local_id},{sections:arResult});
        if (!resultUpdate) {
            res.json({
                status: 'error',
                message: 'Imposible eliminar relacion Seccion-Local'
            })
            return
        }

        var remove = await Sections.deleteOne({ _id: req.body._id });
        res.json({
            status: 'ok',
            message: 'Seccion Eliminada'
        })
    } catch (error) {
        res.status(200).json({
            status: 'error',
            message: 'Error de proceso en el server'
        })
    }
}
sectionsobj.getAviable = async (req, res) => {
    try {
        var sections = await Sections.find();
        var result = { Mon: [], Tue: [], Wed: [], Thu: [], Fri: [], Sat: [], Sun: [] }
        for (let i = 0; i < sections.length; i++) {
            if (sections[i].status == 'enable' && sections[i].maxUser - sections[i].users.length > 0) {
           

                switch (sections[i].weekDay) {
                    case 'Mon':
                        result.Mon.push({ start_time: sections[i].start_time, end_time: sections[i].end_time })
                        break;
                    case 'Tue':
                        result.Tue.push({ start_time: sections[i].start_time, end_time: sections[i].end_time })
                        break;
                    case 'Wed':
                        result.Wed.push({ start_time: sections[i].start_time, end_time: sections[i].end_time })
                        break;
                    case 'Thu':
                        result.Thu.push({ start_time: sections[i].start_time, end_time: sections[i].end_time })
                        break;
                    case 'Fri':
                        result.Fri.push({ start_time: sections[i].start_time, end_time: sections[i].end_time })
                        break;
                    case 'Sat':
                        result.Sat.push({ start_time: sections[i].start_time, end_time: sections[i].end_time })
                        break;
                    case 'Sun':
                        result.Sun.push({ start_time: sections[i].start_time, end_time: sections[i].end_time })
                        break;
                }

            }

        }
        if (result.mon.length == 0 && result.tue.length == 0 && result.wed.length == 0 && result.thu.length == 0 && result.fri.length == 0 && result.sat.length == 0 && result.sun.length == 0) {
            res.status(404).json({
                status: 'error',
                message: 'There are no schedules available'
            })
            return
        }

        res.json({
            status: 'ok',
            aviable: result
        })
        return



    } catch (error) {
        console.log(error);
    }

}
sectionsobj.getByDate = async (req, res) => {
    day = parseInt(req.body.day);
    // hour = parseInt(req.body.hour);
    result = [];
    switch (day) {
        case 0:
            day = 'Sun';
            break;
        case 1:
            day = 'Mon';
            break;
        case 2:
            day = 'Tue';
            break;
        case 3:
            day = 'Wed';
            break;
        case 4:
            day = 'Thu';
            break;
        case 5:
            day = 'Fri';
            break;
        case 6:
            day = 'Sat';
            break;
    }
    try {
        allsections = await Sections.find().select('weekDay').select('start_time').select('end_time').sort({start_time:1});
        for (let i = 0; i < allsections.length; i++) {
    
            if (day == allsections[i].weekDay/*  && allsections[i].start_time <= hour && hour < allsections[i].end_time */) {
                result.push(allsections[i]);       
            }
        }
        res.json({
            status: 'ok',
            sections: result
        })
    } catch (error) {
        console.log(error);

    }

}
sectionsobj.getActive = async (req, res) => {
  var sections = await Sections.find({status:true}).select("start_time end_time").sort({start_time:1})

  if (!sections) {
      res.json({
          status:"error",
          message:"Imposible get hours from server"
      })
      return
  }
    res.json({
        status:"ok",
        sections:sections
    })
  
}

module.exports = sectionsobj;