const {body, validationResult} = require('express-validator');
const Roles = require('../models/roles');
const Subscripions = require('../models/subscriptions');
var validate = {};
validate.singup = [
    
    body('fullname').notEmpty().withMessage('Name Required').isAlpha().withMessage('Alphabet Only'),
    body('email').notEmpty().withMessage('Email Required').isEmail().withMessage('Email Invalid'),
    body('phone').notEmpty().isMobilePhone(),
    body('address').notEmpty(),
    body('avatar'),
    body('pay_method'),
    body('password').notEmpty().withMessage('Password Required').isStrongPassword().withMessage('Easy Password'),
    body('roles'),
    body('subscripions'),
    
]
validate.login = [
    body('email').notEmpty().withMessage('Email Required').isEmail().withMessage('Email Invalid'),
    body('password').notEmpty().withMessage('Password Required').isStrongPassword().withMessage('Easy Password'),
]
validate.recoveryPass = [
    body('email').notEmpty().withMessage('Email Required').isEmail().withMessage('Email Invalid'),
]

module.exports = validate;