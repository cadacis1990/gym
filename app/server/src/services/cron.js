var cron = require('node-cron');
const Records  = require('../models/records');
import config from '../config.js'
const stripe = require('stripe')(config.stripe_pk);



var  chargefunction = async (customer, charge)=>{

    const fetch = await stripe.charges.create({
        amount: parseFloat(charge),
        currency: 'usd',
        customer:customer
      });
   return fetch

}



var task = cron.schedule('0 0 0 * * *',async () =>  {
        var records = await Records.find().populate("user local section product")
        var today_ = new Date()
        var today_day = today_.getDate()
        var today_month = today_.getMonth()
        records.map(async(item,key)=>{

            var create_ = new Date(item.createdAt)
            var create_day = create_.getDate()
            var create_month= create_.getMonth()
            const stripeCustomer =  item.user.stripe_cus
            const chargeCustomer =  item.charge  
         
            if (today_day == create_day && today_month != create_month) {
            
                try {
                    const fetch = await chargefunction(stripeCustomer, chargeCustomer)
                    if (fetch.status != "succeeded") {
                      await Records.updateOne({_id:item._id},{status:false, last_pay:new Date().toString()})
                    }else{
                      await Records.updateOne({_id:item._id},{status:true, last_pay:new Date().toString()})
                    }
                      
                } catch (error) {
                    await Records.updateOne({_id:item._id},{status:false, last_pay:new Date().toString()})
                    
                }
            }  
        })
      

}, {
  scheduled: false
});

export default task