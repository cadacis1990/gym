const nodemailer = require('nodemailer');
const config = require('../config');
var services = {};

var mailConfig = nodemailer.createTransport({
    host: config.email.host,
    port: config.email.port,
    secure: false,
    auth: {
        user: config.email.user,
        pass: config.email.password
    }

})

services.emailConfirm = async (data, req) => {
    try {
        var link = config.url_site;
            link = 'http://' + link + '/checkmail/' + data.confirm;
        var info = await mailConfig.sendMail({
            from: '"' + config.email.from +'"' + '<' + config.email.user + '>',
            to: data.email,
            subject: "Hello - " + data.firstname + " " + data.lastname, // Subject line
            text: "CONFIRMATION LINK:  " + link, // plain text body
            html: "<b>CONFIRMATION LINK:</b>   " + link, // html body
        })
        return info;
    } catch (error) {
        return error;
    }
}

services.emailRecovery= async (data, req) => {
    try {
        var link = config.url_site;
            link = 'http://' + link + '/changepassrecovery/' + data.recovery;
        var info = await mailConfig.sendMail({
            from: '"' + config.email.from +'"' + '<' + config.email.user + '>',
            to: data.email,
            subject: "Hello - " + data.firstname + " " + data.lastname, // Subject line
            text: "RECOVERY MAIL:  " + link, // plain text body
            html: "<b>RECOVERY MAIL:</b>   " + link, // html body
        })
        return info;
    } catch (error) {
        return error;
    }


}

module.exports = services;