import React from 'react';
import Typography from '@mui/material/Typography'
import Login from './login.jsx'
import { Home } from '../index.jsx';
import { Layout } from '../index.jsx';
import { useSelector } from "react-redux";
import {useNavigate} from "react-router-dom"
const Index = () => {
const navigate = useNavigate()
const role = useSelector(state => state.user.roles)


for (let index = 0; index < role.length; index++) {
    if (role[index].name != "client") {
        return (
            <div>
              <Home/>
            </div>
        );
    }           
 }
 return (
    <div>
       <Login/>
    </div>
    )
}

export default Index;
