import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import validator from "validator";
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Snackbar from '@mui/material/Snackbar';
import Stack from '@mui/material/Stack';
import CircularProgress from '@mui/material/CircularProgress';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import {useDispatch, useSelector } from 'react-redux'
import notifyActions from '../../redux/actions/notify.js'
import userActionById from '../../redux/actions/getUserDataById.js'
import axios from 'axios'
import {useParams} from 'react-router-dom'
import {useNavigate} from 'react-router-dom'



export default function SignIn(props) {
    const navigate = useNavigate()
    const {id} = useParams();
    const user     = useSelector(state=>state.userbyid)
    const dispatch = useDispatch();
    const [rpass, setRpass] = React.useState({
        error:false,
        label:"Repita el Password",
        color:"",
        value:""
    });
    const [notify, setNotify] = React.useState({
        type:"success",
        msg:"false",
        status:false
    });
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
        label:"Password",
        color:"",
        error:false

      });

      const handleChange = (prop) => (event) => {
    
            setValues({ ...values, [prop]: event.target.value, error:false, label:"Password", color:""  });
   
        }
      
        const updateRpass = (event)=>{
                setRpass({value:event.target.value}) 
        }
      const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
      };
    
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };
  
    const handleClose = () => {
        setNotify({...notify, status:false});
    }

    const handleSubmit = async () => {
            if (!id) {
              dispatch(notifyActions.active(true, "error", "Invalid Token Id"))
              return
          }
          if (values.password != rpass.value) {
            dispatch(notifyActions.active(true, "error", "Passwords not Match"))
            return
          }  
          if (!validator.isStrongPassword(values.password)) {
            dispatch(notifyActions.active(true, "error", "Passwords is Very Easy"))
            return
          }  
          

          var fetch = await axios.post("/user/changepassrecovery",{
              token:id,
              password:values.password
           })
           
         if (fetch.data.status != "ok") {
          dispatch(notifyActions.active(true, "error", fetch.data.message))
          return
         }  
         //Si la respuesta del server es positiva
         dispatch(notifyActions.active(true, "success",  fetch.data.message))
         dispatch(userActionById(user._id))
         setValues({...values, password:""})
         setRpass({...rpass, value:""})

         setTimeout(() => {
          navigate("/login", { replace: true });
       }, 5000);
      
        };

  return (
   
        <Box
          sx={{
            mt:3,
            marginTop: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
           

          }}
        >
          <Box
           sx={{
            marginTop: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            border:"1px solid #ddbc48",
            p:3,
            borderRadius:3

          }}>
          <Stack sx={{ width: '100%' }} spacing={2}>
            <Snackbar open={notify.status} autoHideDuration={6000} onClose={handleClose}>
                <Alert  severity={notify.type}>
               
                  <AlertTitle>{notify.error}</AlertTitle>
                 
             
                     {notify.msg}
                     <CircularProgress color="success" width={2} />
                  </Alert>
            </Snackbar>
          </Stack>
      
          <Avatar sx={{ m: 1, bgcolor: 'doradoBtn.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" color="#ffffff" variant="h5">
            Change Password
          </Typography>
          <Box sx={{ mt: 1 }}>
         
          <InputLabel style={{color:"#ddbc48"}} htmlFor="outlined-adornment-password">{values.label}</InputLabel>
          <OutlinedInput
            fullWidth 
            size='small'
            sx={{borderBottom:"1px solid #ddbc48",mb:1, input: { color: '#ffffff' }}}
              color="doradoBtn"
            id="outlined-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                  color="white"
                >
                  {values.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label={values.label}
            error={values.error}
          />
            <InputLabel style={{color:"#ddbc48", mt:2 }} htmlFor="outlined-adornment-password">{"Repeat Password"}</InputLabel>
            <TextField
            size='small'
              sx={{borderBottom:"1px solid #ddbc48",mt:-0.1, input: { color: '#ffffff' }}}
              color="doradoBtn"
              error={rpass.error}
              onChange={updateRpass}
              margin="normal"
              required
              fullWidth
              value={rpass.value}
              name="password"
           
              type="password"
              id="passwordtwo"
            />
            <Button
              color="doradoBtn"
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 1, mb: 2 }}
            >
             Cambiar Password
            </Button>
           
          </Box>
     </Box>
        </Box>
    



  
  );
}
