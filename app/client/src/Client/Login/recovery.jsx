import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import {Link} from 'react-router-dom';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Snackbar from '@mui/material/Snackbar';
import { Icon } from '@iconify/react';
import validator from "validator";
import { useNavigate } from "react-router-dom";
import {useDispatch} from 'react-redux'
import notify from '../../redux/actions/notify.js'
import axios from 'axios';

export default function SignIn(props) {
  var navigate = useNavigate();
  var dispatch = useDispatch();

  const [error, setError] = React.useState(false);
 
  const [email, setEmail] = React.useState({
      value:"",
      error:false,
      label:"Email",
      color:""
  });

  const handleEmail = (e) => {
    if (!validator.isEmail(e.target.value) && e.target.value != "") {
      setEmail({value:e.target.value, error:true, label:"Email Invalido", color:"#d32f2f"});
      return
    }  
      setEmail({value:e.target.value, error:false, label:"Email", color:""});
  }

  const handleClose = () => {
    setNotify({...notify, status:false});
  }

  //Accion del submit aqui
  const handleSubmit = async() => {
      if (!validator.isEmail(email.value)) {
         return 
      }

     var fetch = await axios.post("/user/recovery",{
       email:email.value
     })

     if (fetch.data.status != "ok") {
        dispatch(notify.active(true, "error", fetch.data.message))
        return
     }
  

     dispatch(notify.active(true, "success", "Recovery Email Sent"))

      setEmail({...email, value:""})
      setTimeout(() => {
        navigate("/login", { replace: true });
     }, 5000);

  };
  

  return (
   
      <Box display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
      <Container component="main" maxWidth="xs">
        <Card elevation={10} sx={{p:2, borderRadius:5, backgroundColor:"#000000B5", borderRadius:3,  borderStyle:"solid", border:2, borderColor:"#ddbc48"}}>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
   
      
          <Avatar sx={{ m: 1, bgcolor: 'doradoBtn.main' }}>
            <Icon icon="cib:mail-ru" color="black" width="30" height="30" />
          </Avatar>
          <Typography component="h1" variant="h5" sx={{color: 'doradoBtn.main'}} color="doradoBtn">
            Recuperar Password
          </Typography>
          <Box  sx={{ mt: 1 }}>
            <TextField
            
              variant="standard"
              value={email.value}
              error={email.error}
              onChange={handleEmail}
              required
              fullWidth
              id="email"
              label={<Typography variant="body1" color="#ddbc48">Email</Typography>}
              name="email"
              sx={{borderBottom:"1px solid #ddbc48", input: { color: '#ffffff' }}}
              color="doradoBtn"
            />
          
            <Button
              onClick={handleSubmit}
              fullWidth
              color="doradoBtn"
              variant="contained"
              sx={{ mt: 1, mb: 1}}
            >
              Enviar Email
            </Button>
            <Grid container>
              <Grid item xs>
              <Link style={{textDecoration:"none"}} to="/login" >
                  <Typography style={{color:"white"}}  variant="body1" > Acceder</Typography>
              </Link>
              
              </Grid>
            
            </Grid>
          </Box>
        </Box>
        </Card>
      </Container>
      </Box>
  
  );
}
