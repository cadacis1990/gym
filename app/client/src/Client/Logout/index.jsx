import React from 'react';
import { useNavigate } from "react-router-dom";
import Cookies from 'js-cookie'
import { Home } from "../index.jsx";
import {useDispatch, useSelector} from 'react-redux'
import actionUser from "../../redux/actions/getUserData.js";


const Index = () => {
    const navigate = useNavigate()
    const dispatch = useDispatch();
   
    React.useEffect(()=>{
        Cookies.remove('jwt')
        Cookies.remove("userId")
        dispatch(actionUser)
        navigate("/")
    },[])
    return (
        <div>
        
        </div>
       
    );
}

export default Index;
