import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography'
import Services from './Servicios/index.jsx'

const Index = () => {
    return (
        <div>
        <Box>
           <Box  sx={{width:"auto",borderRadius:3,p:3, mr:{xs:1, sm:4}, border:"1px solid #ddbc48"}}>
                <Typography display={"flex"} justifyContent={"center"} textAlign={"center"} sx={{fontSize:"40px"}} variant="h5" color="#ddbc48">
                     Choose One Subscriptions
                </Typography>
                <Typography display={"flex"} justifyContent={"center"} textAlign={"center"} sx={{fontSize:"30px",fontWeight:300, mt:-2}} variant="h6" color="#ffffff">
                      You need to access a subscription
                </Typography>
                <Box sx={{mt:3}}>
                   <Services/>
                </Box>
                 
           </Box>
        </Box>
       
        </div>
    );
}

export default Index;
