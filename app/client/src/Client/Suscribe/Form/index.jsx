import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import {useNavigate} from 'react-router-dom'
import Card from '@mui/material/Card';
import { useDispatch,useSelector } from "react-redux";
import notify from "../../../redux/actions/notify.js";
import checkoutChange from "../../../redux/actions/checkout.js";
import Typography from '@mui/material/Typography'
import StripeForm from './StripeForm/index.jsx'
/* import Images from 'D:/project/gym/build/newuser.png'; */


const styles = {
    paperContainer: {
        backgroundColor:"white",
        backgroundImage: `url(/newuser.png)`,
        backgroundRepeat: "repeat-y",
        backgroundPosition: "center left",
        backgroundSize: "100%"
        
    }
};

const Index = () => {
    return (
        <div>
            <StripeForm/>
        </div>
    );
}


 export default function SignUp(props) {
    const data = useSelector(state=>state.checkout)
    const dispatch = useDispatch()
    const product = useSelector((state)=>state.product)

   
    const handleChangeCountry = (event) => {
      dispatch(checkoutChange({...data, country:event.target.value}))
    }
    const handleChangeAddressOne = (event) => {
      dispatch(checkoutChange({...data, address_line1:event.target.value}))
    }
    const handleChangeAddressTwo = (event) => {
      dispatch(checkoutChange({...data, address_line2:event.target.value}))
    }
    const handleChangeCity = (event) => {
      dispatch(checkoutChange({...data, city:event.target.value}))
    }
    const handleChangeState = (event) => {
      dispatch(checkoutChange({...data, state:event.target.value}))
    }

  

  return (
    <Grid  sx={{ pr:{xs:0, sm:3}, bgcolor:"#000000"}} container spacing={2}>
    
    <Grid   item xs={12} sm={12}>
        
    <Card sx={{backgroundColor:"#000000"}} >
        <Box
        
         sx={{
            border:'8px solid #ddbc48', 
            backgroundColor:"#ffffff",
            borderRadius:3,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
          }}

        >
        
       <Box>
         <Box>
      
       

          <Box borderRadius={15} sx={{p:2}} noValidate >
            <Grid container spacing={1}>
             
           
              <Grid item xs={12}>
              <Grid container spacing={1}>
                <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.country}
                    onChange={handleChangeCountry}
                    required
                    fullWidth
                    id="country"
                    label="Country"
                    name="country"
                    
                    />
                 </Grid>
                 <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.state}
                    onChange={handleChangeState}
                    required
                    fullWidth
                    id="state"
                    label="State"
                    name="state"
                    
                    />
                 </Grid>
              </Grid>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  size="small"
                  color="doradoBtn"
                  value={data.city}
                  onChange={handleChangeCity}
                  required
                  fullWidth
                  id="city"
                  label="City"
                  name="city"

                />
              </Grid>
              <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.address_line1}
                    onChange={handleChangeAddressOne}
        
                    fullWidth
                    id="addressone"
                    label="Address 1"
                    name="addressone"
                    
                    />
                 </Grid>
                 <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.address_line2}
                    onChange={handleChangeAddressTwo}

                    fullWidth
                    id="addresstwo"
                    label="Address 2"
                    name="addressone"
                    
                    />
                 </Grid>


            
              <Grid  item xs={12}>
                      <StripeForm handleClose={props.handleClose} id={props.id} local={props.local}  section={props.section}  product={props.product} />
              </Grid>
          
            </Grid>
            
          </Box>
        </Box>
            </Box>
        </Box>
      
     
 
    </Card>
    </Grid >
 
    </Grid >
  );
}

const  Extract = () => {
    const product = useSelector(state=>state.checkout.product)
    const navigate = useNavigate()
    if (product._id == "") {
        return(
            <div>
               <Box sx={{flexDirection: 'column', alignItems: 'center' }} > 
                <Typography variant="h4" textAlign={"center"} color={"doradoBtn.main"}>
                    Select a Subscription
                </Typography>
                <Typography variant="body1" textAlign={"center"} color={"doradoBtn.main"}>
                    To register it is necessary to select a subscription, please see our products
                </Typography>
                <br/>
                <Box fullWidth textAlign={"center"}>
                <Button onClick={()=>navigate("/services")} variant="text" sx={{border:"1px solid #ddbc48"}} color="doradoBtn">
                    OUR SHOP
                </Button>
                </Box>
              </Box>
            </div> 
        )
    }
    return(
        <div>
            
               <Typography variant="h6" color="gray">Subscription</Typography>
               <Typography variant="h4" color={"doradoBtn.main"}>{product.name}</Typography>
               <br/>
          
               <Typography variant="h6" color="gray">Description</Typography>
               <Typography variant="body2" color="gray">
                  {product.descriptions}
               </Typography>
                <br/>
          
                <Typography variant="h6" color="gray">Price</Typography>
                <Box display="flex">
                  <Typography variant="h4" color={"doradoBtn.main"}>${product.price}</Typography>
                  <Typography variant="body1" color={"doradoBtn.main"}>/{product.type}</Typography>
                </Box>
                <br/>
                <Box fullWidth textAlign={"center"}>
                <Button onClick={()=>navigate("/services")}  sx={{border:"1px solid #ddbc48"}}  variant="text" color="doradoBtn">
                   CHANGE SUBSCRIPTIONS
                </Button>
                </Box>

                <br/>
                  
          
        </div>
    )
} 


