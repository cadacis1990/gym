import * as React from "react";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import validator from "validator";
import Typography from "@mui/material/Typography";
import notify from "../../../../redux/actions/notify.js";
import userUpdate from "../../../../redux/actions/getUserData.js";
import wait from "../../../../redux/actions/wait.js";
import checkoutChange from "../../../../redux/actions/checkout.js";
import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";

const stripePromise = loadStripe(
  "pk_test_51IDcSKBg5CrIbIRKdXcfwFvXP5mUpfpWZVq7iVyWmcK9SfHFcjYhndyUr1KWp3dxAvZNbV7DbBl5OjgoHmN2F1jX00yOggHmjo"
);

export default function SignUp(props) {
  return (
    <div>
      <Elements stripe={stripePromise}>
        <Form {...props} />
      </Elements>
    </div>
  );
}

const Form = (props) => {
  const navigate = useNavigate();
  const stripe = useStripe();
  const elements = useElements();
  const dispatch = useDispatch();
  const checkout = useSelector((state) => state.checkout);
  const user = useSelector((state) => state.user);

  const handleSubmit = async () => {
    if (validator.isEmpty(checkout.country, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid Country"));
      return;
    }
    if (validator.isEmpty(checkout.state, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid State"));
      return;
    }
    if (validator.isEmpty(checkout.city, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid City"));
      return;
    }

    //Aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
    //Aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii

    const fetchStripe = await stripe.createToken(
      elements.getElement(CardElement),
      {
        name: user.name,
        address_line1: checkout.address_line1,
        address_line2: checkout.address_line2,
        address_city: checkout.city,
        address_state: checkout.state,
        address_country: checkout.country,
      }
    );
    dispatch(wait(true));

    const { error } = fetchStripe;
    if (error) {
      dispatch(wait(false));
      dispatch(notify.active(true, "error", error.code));
      return;
    }

    const fetchServer = await axios.put("/user/addproduct", {
      stripe: fetchStripe,
      user: {
        id: props.id,
        local: props.local,
        product: props.product,
        section: props.section,
        email: user.email,
        name: user.name,
        city: checkout.city,
        country: checkout.country,
        line1: checkout.address_line1,
        line2: checkout.address_line2,
        state: checkout.state,
      },
    });

    if (fetchServer.data.status != "ok") {
      dispatch(wait(false));
      dispatch(notify.active(true, "error", fetchServer.data.message));

      return;
    }

    dispatch(wait(false));
    dispatch(userUpdate);
    dispatch(notify.active(true, "success", fetchServer.data.message));
    props.handleClose();
  };
  const cardElementOptions = {
    style: {
      base: {
        color: "#666",
        fontSize: "15px",
        fontWeight: "300",
        padding: "11.4px 12px",
      },
      invalid: {
        color: "#fa755a",
        fontSize: "fa755a",
      },
    },
  };

  return (
    <div>
      <Box
        sx={{
          border: "1px solid grey",
          borderRadius: "5px",
        }}
      >
        <Box sx={{ p: 1 }}>
          <CardElement options={cardElementOptions} />
        </Box>
      </Box>

      <Button
        onClick={handleSubmit}
        fullWidth
        variant="contained"
        sx={{ mt: 3, backgroundColor: "#9e7f10" }}
        disabled={!stripe ? true : false}
      >
        <Typography variant="body1" color="#ffffff">
          Suscribe
        </Typography>
      </Button>
    </div>
  );
};
