import React from 'react';
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Button from '@mui/material/Button'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputAdornment from '@mui/material/InputAdornment';
import { Icon } from '@iconify/react';
import {useSelector, useDispatch} from 'react-redux'
import checkoutAction from '../../../redux/actions/checkout.js'
import notify from '../../../redux/actions/notify.js'
import userAction from '../../../redux/actions/getUserData.js'
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import convertTime from '../../../Component/utils/convertTime.js'
import Dialog from '@mui/material/Dialog';
import StripeForm from '../Form/index.jsx'

const Index =  (props) => {
    const navigate = useNavigate()
    const user = useSelector(state=>state.user._id);
    const locals = useSelector(state=>state.local);
    const checkout = useSelector(state=>state.checkout);
    const [hours, setHours] = React.useState([]);
    const dispatch = useDispatch();
    const [local, setLocal] = React.useState("");
    const [sections, setSections] = React.useState('');
    const [close, setClose] = React.useState(false);

    const handleClose = () => {
      setClose(false);
    };
  
    const handleOpen = (value) => {
      setClose(true);
    };
   
    var statusComponent = false;
        if ( props._id == checkout.product._id) {
           statusComponent = true;
        }

    /* statusComponent = true; */
  
    const handleChange = (event) => {
      setSections(event.target.value);
    };

    const handleChangeLocal = (event) => {
        setLocal(event.target.value);
        var dataSection = locals.filter((item,key)=>{
             return item._id == event.target.value
        })
        setHours(dataSection[0].sections)
      };

  
  const handleSubmit = async (event) => {

    handleOpen()
                    
  };

    return (
        <div>
            <Dialog onClose={handleClose} open={close}>
                <StripeForm handleClose={handleClose} id={user} local={local} product={props._id} section={sections}/>
            </Dialog>
             
            <Box sx={{p:2, borderRadius:"10px", border:statusComponent ? '1px solid #8d8d8d' :'1px solid #ddbc48' }}>
               <Typography variant="h6" color="gray">Subscription</Typography>
              
               <Typography variant="h4" color={statusComponent ? "greyLogin.main":"doradoBtn.main"}>{props.name}</Typography>
          
              {/*  <Typography variant="h6" color="gray">Description</Typography>
               <div className="Container" dangerouslySetInnerHTML={{__html:props.descriptions}}></div> */}
               <Typography variant="body2" color="gray">
                   
               </Typography>
                <br/>
          
                <Typography variant="h6" color="gray">Price</Typography>
                <Box display="flex">
                  <Typography variant="h3" color={statusComponent ? "greyLogin.main":"doradoBtn.main"}>${props.price}</Typography>
                  <Typography variant="body1" color={statusComponent ? "greyLogin.main":"doradoBtn.main"}>/{props.type}</Typography>
                </Box>
                <br/>
                <Box sx={{mb:2}}>
                <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label" color="grey" sx={{mb:2, color:statusComponent ? "greyLogin.main":"#ddbc48"}} >LOCAL</InputLabel>
                            <Select
                                disabled={statusComponent}
                                color="doradoBtn"
                                bgcolor={"#ffffff"}
                                sx={{borderBottom:statusComponent ?"1px solid greyLogin.main":"1px solid #ddbc48", color:statusComponent ? "greyLogin.main":"#ddbc48" }}
                               /*  labelId="demo-simple-select-label" */
                                id="demo-simple-select"
                                value={local}
                                label= {<Typography variant="h3" color="doradoBtn.main">Local</Typography>}
                                
                                endAdornment={
                                  
                                    <InputAdornment position="end">
                                       <Icon icon="bx:bxs-down-arrow" color={statusComponent ? "#8d8d8d":"#ddbc48"} width="20" height="20" />
                                    </InputAdornment>
                                  }
                               
                                onChange={handleChangeLocal}
                                >
                                       
                                       {locals.map((item, key)=>{
                                            return(
                                                <MenuItem key={item._id} value={item._id}>{item.name}</MenuItem>
                                            )
                                       })}
                                 
                            </Select>
                    </FormControl>
                    </Box>
                <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label" color="grey" sx={{ color:statusComponent ? "greyLogin.main":"#ddbc48"}} >SECTIONS</InputLabel>
                            <Select
                                disabled={statusComponent}
                                color="doradoBtn"
                                bgcolor={"#ffffff"}
                                sx={{borderBottom:statusComponent ?"1px solid greyLogin.main":"1px solid #ddbc48", color:statusComponent ? "greyLogin.main":"#ddbc48" }}
                               /*  labelId="demo-simple-select-label" */
                                id="demo-simple-select"
                                value={sections}
                                label= {<Typography variant="h3" color="doradoBtn.main">Sections</Typography>}
                                
                                endAdornment={
                                  
                                    <InputAdornment position="end">
                                       <Icon icon="bx:bxs-down-arrow" color={statusComponent ? "#8d8d8d":"#ddbc48"} width="20" height="20" />
                                    </InputAdornment>
                                  }
                               
                                onChange={handleChange}
                                >
                                       
                                       {hours.map((item, key)=>{
                                            return(
                                                <MenuItem key={item._id} value={item._id}>{convertTime(item.start_time, item.end_time)}</MenuItem>
                                            )
                                       })}
                              
                            </Select>
                    </FormControl>
                   
                  <br/>
                  <br/>

                  <Button onClick={handleSubmit} disabled={statusComponent} variant="contained" color="doradoBtn">
                       SELECT
                  </Button>
             
            </Box>
        </div>
    );
}

export default Index;
