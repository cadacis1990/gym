import React from 'react';
import Card from "./card.jsx";
import {useDispatch, useSelector} from 'react-redux'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box';
const Index = () => {
    const product = useSelector(state=>state.product)
  
    return (
        <div>
         <Box sx={{pr:3}}>
            <Grid container spacing={3}>
                {
                product.map((item, key)=>{
                return (
                <Grid key={key} item xs={12} sm={6}> 
                    <Card key={item._id} _id={item._id} name = {item.name} descriptions={item.descriptions} price={item.price} type={item.type} /> 
                </Grid>
                )
                })
                }
            </Grid>
         </Box>  
           
        </div>
    );
}

export default Index;
