import About      from './Aboutus/index.jsx'
import Calendar   from './Calendar/index.jsx'
import Contact    from './Contactus/index.jsx'
import Home       from './Home/index.jsx'
import Layout     from './Layout/index.jsx'
import Programs   from './Programa/index.jsx'
import SignUp     from './Register/index.jsx'
import Services   from './Servicios/index.jsx'
import Teachers   from './Teacher/index.jsx'
import Logins     from './Login/index.jsx'
import Recovery   from './Login/recovery.jsx'
import ChangePass from './Login/changepass.jsx'
import Logout     from './Logout/index.jsx'
import Account    from './Account/index.jsx'
import AddPayment    from './AddPayment/index.jsx'
import EmailVerify    from './EmailVerify/index.jsx'

export  {Layout}
export  {About};
export  {AddPayment}
export  {Calendar};
export  {Contact};
export  {Home};
export  {Programs};
export  {SignUp};
export  {Services};
export  {Teachers};
export  {Logins};
export  {Recovery};
export  {ChangePass};
export  {Logout};
export  {Account};
export  {EmailVerify};


