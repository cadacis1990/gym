import React from 'react';
import { Box, Typography, Button } from '@mui/material';
import { Icon } from '@iconify/react';
import axios from 'axios'
import {useDispatch} from 'react-redux'
import updateUser from '../../redux/actions/getUserData.js'
import notify from '../../redux/actions/notify.js'
import WaitBlack from '../../redux/actions/wait.js'

const TypeCard = (props)=>{
    if (props.type == "American Express") {
        return(
            <Icon icon="fontisto:american-express" color="#0074ff" width="40" height="40" />
        )
    }

    if (props.type == "Diners Club") {
        return(
            <Icon icon="fa:cc-diners-club" color="#0074ff" width="40" height="40" />
        )
    }
    if (props.type == "Discover") {
        return(
            <Icon icon="logos:discover" color="#0074ff" width="50" height="50" />
        )
    }
 
    if (props.type == "UnionPay") {
        return(
            <Icon icon="logos:unionpay" color="#0074ff" width="50" height="50" />
        )
    }
    if (props.type == "Visa") {
        return(
            <Icon icon="logos:visa" color="#0074ff" width="50" height="50" />
        )
    }
   
        return(
            <Typography variant="h6" color="#000000">
                  CREDIT CARD
             </Typography>
        )

}

const Card = (props) => {
    const dispatch = useDispatch()

    const handleDelete = async(item)=>{
       
        const fetch = await axios.delete("/payments",{
            params:item
        })
        
        dispatch(WaitBlack(true))
        if (fetch.data.status != "ok") {
            dispatch(WaitBlack(false))
            dispatch(notify.active(true, "error", fetch.data.message))
            return
        }
            dispatch(WaitBlack(false))
            dispatch(notify.active(true, "success", fetch.data.message))
            dispatch(updateUser)

       
    }


    return (
        <div>
            <Box sx={{borderRadius:3,p:1,mt:2, background: "rgb(155,155,157)", background: "linear-gradient(174deg, rgba(155,155,157,1) 0%, rgba(255,255,255,1) 32%, rgba(170,170,170,1) 100%)"}}>
               
               <TypeCard type={props.type}/>
                <Typography sx={{mt:2, mb:3, fontWeight:"300"}} variant="h4" color="#6a6a6a">
               
                  {props.number}
                </Typography>
                <Box display={"flex"} justifyContent={"end"}>
                    <Box flexGrow={1}>
                        <Typography sx={{ fontWeight:"300"}} variant="h6" color="#6a6a6a">
               
                             {props.name.toUpperCase()}
                        </Typography>
                    </Box>
                     <Button onClick={()=>handleDelete(props.item)} disableElevation variant="contained" color="error">
                        DELETE
                    </Button>
                </Box>
             
            </Box>
         
          
            
        </div>
    );
}

export default Card;
