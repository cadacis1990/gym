import React from 'react';
import CardCard from './card.jsx'
import {useSelector} from 'react-redux'
import Dialog from '@mui/material/Dialog';
import { Box, Typography, Button } from '@mui/material';
import StripeAddPay from './stripe.jsx'

const Index = () => {
    const cards = useSelector(state=>state.user.pay_method)
    const [open, setOpen] = React.useState(false)

    const handleClose = () => {
      setOpen(false)
    };

    const handleOpen = () => {
        setOpen(true)
    };


    return (
        <div>
            <Dialog onClose={handleClose} open={open}>
                <StripeAddPay handleCloseForm={handleClose}/>
           </Dialog>

            {cards.map((item, key)=>{
                return(
                    <CardCard key = {key}
                              item={item}
                              type={item.pay_method.brand} 
                              status={item.status} 
                              number={"**** **** **** " + item.pay_method.last4} 
                              name = {item.pay_method.name}
                    />
                )

            })}
            <Box sx={{mt:2}} display="flex" justifyContent={"center"} >
               <Button onClick={handleOpen} variant="contained" color="doradoBtn">Add Payment Method</Button>
            </Box>
          
        </div>
    );
}

export default Index;
