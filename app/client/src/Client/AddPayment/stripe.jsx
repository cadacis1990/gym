import * as React from 'react';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import { useDispatch,useSelector } from "react-redux";
import notify from "../../redux/actions/notify.js";
import checkoutChange from "../../redux/actions/checkout.js";
import StripeForm from './StripeForm/index.jsx'



const Index = () => {
    return (
        <div>
            <StripeForm/>
        </div>
    );
}

 Index;

 export default function SignUp(props) {

    const dispatch = useDispatch()
    const product = useSelector((state)=>state.product)

    const [data, setData] = React.useState({
        country:"",
        address_line1:"",
        address_line2:"",
        city:"",
        state:"",
        firstname:"",
        lastname:"",
        email:""
    })

  const handleChangeCountry = (event) => {
    setData({...data, country:event.target.value})
  }

  const handleChangeAddressOne = (event) => {
   setData({...data,  address_line1:event.target.value})
  }
  const handleChangeAddressTwo = (event) => {
    setData({...data,  address_line2:event.target.value})
  }
  const handleChangeCity = (event) => {
    setData({...data, city:event.target.value})
  }
  const handleChangeState = (event) => {
    setData({...data, state:event.target.value})
  }

  const handleFirstName = (event) =>{
    setData({...data, firstname:event.target.value})
  }
  const handleLastName = (event) =>{
    setData({...data, lastname:event.target.value})
  }

  const handleEmail = (event) =>{
    setData({...data, email:event.target.value})
  }

  return (
    <Grid  sx={{  bgcolor:"#000000"}} container spacing={2}>
    
    <Grid   item xs={12} sm={12}>
        
    <Card sx={{backgroundColor:"#000000"}} >
        <Box
        
         sx={{
            border:'8px solid #ddbc48', 
            backgroundColor:"#ffffff",
            borderRadius:3,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
          }}

        >
        


       <Box>
         <Box>
      
       

          <Box borderRadius={15} sx={{p:2}} noValidate >
            <Grid container spacing={1}>
              <Grid item xs={12} sm={6}>
                <TextField
                 size="small"
                  color="doradoBtn"
                  value={data.firstname}
                  onChange={handleFirstName}
                
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                size="small"
                 color="doradoBtn"
                  value={data.lastname}
                  onChange={handleLastName}
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                

                />
              </Grid>
              <Grid item xs={12}>
                <TextField
               size="small"
                 color="doradoBtn"
                  value={data.email}
                  onChange={handleEmail}
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
    
                />
              </Grid>
              
          
              <Grid item xs={12}>
              <Grid container spacing={1}>
                <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.country}
                    onChange={handleChangeCountry}
                    required
                    fullWidth
                    id="country"
                    label="Country"
                    name="country"
                    
                    />
                 </Grid>
                 <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.state}
                    onChange={handleChangeState}
                    required
                    fullWidth
                    id="state"
                    label="State"
                    name="state"
                    
                    />
                 </Grid>
              </Grid>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  size="small"
                  color="doradoBtn"
                  value={data.city}
                  onChange={handleChangeCity}
                  required
                  fullWidth
                  id="city"
                  label="City"
                  name="city"

                />
              </Grid>
              <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.address_line1}
                    onChange={handleChangeAddressOne}
        
                    fullWidth
                    id="addressone"
                    label="Address 1"
                    name="addressone"
                    
                    />
                 </Grid>
                 <Grid item xs={6}>
                    <TextField
                    size="small"
                    color="doradoBtn"
                    value={data.address_line2}
                    onChange={handleChangeAddressTwo}

                    fullWidth
                    id="addresstwo"
                    label="Address 2"
                    name="addressone"
                    
                    />
                 </Grid>


             
              <Grid item xs={12}>
                      <StripeForm handleCloseForm = {props.handleCloseForm} data={data}/>
              </Grid>
          
            </Grid>
            
          </Box>
        </Box>
            </Box>
        </Box>
      
     
 
    </Card>
    </Grid >
   
    </Grid >
  );
}



