import React from "react";
import Typography from "@mui/material/Typography";
import TrainersCard from "./card.jsx";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { orange } from "@mui/material/colors";
import axios from "axios";
import { useDispatch } from "react-redux";
import notify from "../../redux/actions/notify.js";

const Index = () => {
  const dispatch = useDispatch();
  const [trainers, setTrainers] = React.useState([]);
  const en = ["Entrena 1", "Entrena 1", "Entrena 1"];

  const getTrainers = async () => {
    var fetch = await axios.get("/user/trainers");
    if (fetch.data.status != "ok") {
      dispatch(notify.active(true, "error", fetch.data.message));
      return;
    }
    setTrainers(fetch.data.trainers);
  };

  React.useEffect(() => {
    getTrainers();
  }, []);

  if (trainers.length < 1) {
    return (
      <Box
        display={"flex"}
        flexDirection={"column"}
        justifyContent={"center"}
        sx={{
          pl: 1,
          mr: { xs: 0, sm: 4 },
          borderRadius: 2,
          minHeight: "100vh",
          backgroundColor: "#ffffff",
        }}
      >
        <Box display={"flex"} justifyContent={"center"}>
          <Typography
            variant="h2"
            sx={{ fontWeight: 600, fontSize: { xs: "40px", sm: "70px" } }}
            color="doradoBtn.main"
          >
            {"we currently have no coaches".toUpperCase()}
          </Typography>
        </Box>
        <Box
          sx={{ mt: { xs: 0, sm: -2 }, fontSize: { xs: "25px", sm: "70px" } }}
          display={"flex"}
          justifyContent={"center"}
        >
          <Typography variant="h5" color="initial">
            {"we are waiting to add the trainers come back later and they will be available".toUpperCase()}
          </Typography>
        </Box>
      </Box>
    );
  }
  return (
    <div>
      <Box
        display={"flex"}
        flexDirection={"column"}
        justifyContent={"center"}
        sx={{
          mr: { xs: 0, sm: 4 },
          borderRadius: 2,
          minHeight: "100vh",
          backgroundColor: "#ffffff",
        }}
      >
        {/*      <Box
          display={{ xs: "none", sm: "block" }}
          component="img"
          sx={{
            maxHeight: { sm: "100vw" },
            mt: 25,
          }}
          src="/raya.jpg"
        ></Box> */}
        <Box sx={{ mt: 2 }} display={"flex"} justifyContent={"center"}>
          <Typography variant="h4" color="initial">
            {"trainers".toUpperCase()}
          </Typography>
        </Box>
        <Box display={"flex"} sx={{ mt: -2 }} justifyContent={"center"}>
          <Typography
            variant="h2"
            sx={{
              fontWeight: 600,
              fontSize: { xs: "40px", sm: "70px" },
              mb: 5,
            }}
            color="doradoBtn.main"
          >
            {"our trainers".toUpperCase()}
          </Typography>
        </Box>
        <Grid
          container
          spacing={5}
          justifyContent="center"
          alignItems="center"
          alignContent="center"
        >
          {trainers.map((item, key) => {
            return (
              <Grid key={key} item xs={12} sm={3} xl={2}>
                <TrainersCard
                  fb={item.social.fb}
                  tw={item.social.tw}
                  ig={item.social.ig}
                  title={"trainers"}
                  name={item.firstname + " " + item.lastname}
                  avatar={item.avatar}
                />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </div>
  );
};

export default Index;
