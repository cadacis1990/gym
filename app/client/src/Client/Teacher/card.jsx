import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import { Icon } from "@iconify/react";
import Button from "@mui/material/Button";

const Card = (props) => {
  return (
    <div>
      <Box display={"flex"} justifyContent={"center"}>
        <Avatar src={props.avatar} sx={{ width: 200, height: 200 }}></Avatar>
      </Box>
      <Box sx={{ mt: 2 }} display={"flex"} justifyContent={"center"}>
        <Typography variant="h5" color="initial">
          {props.name}
        </Typography>
      </Box>
      <Box sx={{ mt: 1 }} display={"flex"} justifyContent={"center"}>
        <Typography variant="body1" color="initial">
          {props.title}
        </Typography>
      </Box>

      <Box
        sx={{ backgroundColor: "#ffffff", mt: 4 }}
        display={"flex"}
        justifyContent={"center"}
      >
        <Button
          href={props.fb}
          sx={{ mb: 3 }}
          variant="text"
          color="greySocial"
        >
          <Icon icon="brandico:facebook" width="25" height="25" />
        </Button>

        <Button
          href={props.tw}
          sx={{ mb: 3 }}
          variant="text"
          color="greySocial"
        >
          <Icon icon="akar-icons:twitter-fill" width="25" height="25" />
        </Button>

        <Button
          href={props.ig}
          sx={{ mb: 3 }}
          variant="text"
          color="greySocial"
        >
          <Icon icon="ant-design:instagram-outlined" width="25" height="25" />
        </Button>
      </Box>
    </div>
  );
};

export default Card;
