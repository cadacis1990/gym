import React from 'react';
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Button from '@mui/material/Button'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputAdornment from '@mui/material/InputAdornment';
import { Icon } from '@iconify/react';
import {useSelector, useDispatch} from 'react-redux'
import checkoutAction from '../../redux/actions/checkout.js'
import notify from '../../redux/actions/notify.js'
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import convertTime from '../../Component/utils/convertTime.js'

const Index =  (props) => {
    const navigate = useNavigate()
    const checkout = useSelector(state=>state.checkout);
    const hours = useSelector((state)=>state.sections)
    const dispatch = useDispatch();
    const [sections, setSections] = React.useState('');
   
    console.log(hours);
     
    var statusComponent = false;
        if ( props._id == checkout.product._id) {
           statusComponent = true;
        }

    /* statusComponent = true; */
  

    const handleChange = (event) => {
      setSections(event.target.value);
    };

    const handleSubmit = ()=>{
        if (sections == "") {
            dispatch(notify.active(true, "error", "Select a valid section" ))
            return  
        }

        dispatch(checkoutAction({
                                ...checkout,
                                 product:{_id:props._id,
                                          name:props.name,
                                          descriptions:props.descriptions,
                                          price:props.price,  
                                          section_id:sections,
                                          type:props.type
                                         }
                                })) 
         setTimeout(() => {
            navigate("/signup")  
         }, 2000);                       
         
                            }
                        

    
    return (
        <div>
            <Box sx={{p:2, borderRadius:"10px", border:statusComponent ? '1px solid #8d8d8d' :'1px solid #ddbc48' }}>
               <Typography variant="h6" color="gray">Subscription</Typography>
              
               <Typography variant="h4" color={statusComponent ? "greyLogin.main":"doradoBtn.main"}>{props.name}</Typography>
               <br/>
                <Divider light />
               <Typography variant="h6" color="gray">Description</Typography>
               <div className="Container" dangerouslySetInnerHTML={{__html:props.descriptions}}></div>
               <Typography variant="body2" color="gray">
                   
               </Typography>
                <br/>
          
                <Typography variant="h6" color="gray">Price</Typography>
                <Box display="flex">
                  <Typography variant="h3" color={statusComponent ? "greyLogin.main":"doradoBtn.main"}>${props.price}</Typography>
                  <Typography variant="body1" color={statusComponent ? "greyLogin.main":"doradoBtn.main"}>/{props.type}</Typography>
                </Box>
                <br/>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label" color="grey" sx={{color:statusComponent ? "greyLogin.main":"#ddbc48"}} >SECTIONS</InputLabel>
                            <Select
                                disabled={statusComponent}
                                color="doradoBtn"
                                bgcolor={"#ffffff"}
                                sx={{borderBottom:statusComponent ?"1px solid greyLogin.main":"1px solid #ddbc48", color:statusComponent ? "greyLogin.main":"#ddbc48" }}
                               /*  labelId="demo-simple-select-label" */
                                id="demo-simple-select"
                                value={sections}
                                label= {<Typography variant="h3" color="doradoBtn.main">Sections</Typography>}
                                
                                endAdornment={
                                  
                                    <InputAdornment position="end">
                                       <Icon icon="bx:bxs-down-arrow" color={statusComponent ? "#8d8d8d":"#ddbc48"} width="20" height="20" />
                                    </InputAdornment>
                                  }
                               
                                onChange={handleChange}
                                >
                                       
                                       {hours.map((item, key)=>{
                                            return(
                                                <MenuItem key={item._id} value={item._id}>{convertTime(item.start_time, item.end_time)}</MenuItem>
                                            )
                                       })}
                              
                                        
                                  
                                 
                            </Select>
                    </FormControl>
                  <br/>
                  <br/>

                  <Button onClick={handleSubmit} disabled={statusComponent} variant="contained" color="doradoBtn">
                       SELECT
                  </Button>
             
            </Box>
        </div>
    );
}

export default Index;
