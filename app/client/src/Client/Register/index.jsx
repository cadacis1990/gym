import * as React from "react";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { useNavigate } from "react-router-dom";
import Card from "@mui/material/Card";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import axios from "axios";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useDispatch, useSelector } from "react-redux";
import notify from "../../redux/actions/notify.js";
import checkoutChange from "../../redux/actions/checkout.js";
import { grey } from "@mui/material/colors";
import Typography from "@mui/material/Typography";
import StripeForm from "./StripeForm/index.jsx";
import MuiPhoneNumber from "material-ui-phone-number";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import MobileDatePicker from "@mui/lab/MobileDatePicker";
import DesktopDatePicker from "@mui/lab/DesktopDatePicker";
import Stack from "@mui/material/Stack";
import checkout from "../../redux/actions/checkout.js";
import Paper from "@mui/material/Paper";
/* import Images from 'D:/project/gym/build/newuser.png'; */
import Image from "material-ui-image"


const styles = {
  paperContainer: {
    backgroundColor: "white",
    backgroundImage: "url(/newuser.png)",
    backgroundRepeat: "repeat-y",
    backgroundPosition: "center left",
    backgroundSize: "100%",
  },
};

const Index = () => {
  return (
    <div>
      <StripeForm />
    </div>
  );
};

Index;

export default function SignUp(props) {
  const data = useSelector((state) => state.checkout);
  const dispatch = useDispatch();
  const product = useSelector((state) => state.product);

  const handleChangePassword = (event) => {
    dispatch(checkoutChange({ ...data, password: event.target.value }));
  };
  const handleChangeRepeatPassword = (event) => {
    dispatch(checkoutChange({ ...data, repeatPassword: event.target.value }));
  };

  const handleClickShowPassword = () => {
    dispatch(checkoutChange({ ...data, showPassword: !data.showPassword }));
  };
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleChangePhone = (event) => {
    dispatch(checkoutChange({ ...data, phone: event.target.value }));
  };
  const handleChangeCountry = (event) => {
    dispatch(checkoutChange({ ...data, country: event.target.value }));
  };
  const handleChangeAddressOne = (event) => {
    dispatch(checkoutChange({ ...data, address_line1: event.target.value }));
  };
  const handleChangeAddressTwo = (event) => {
    dispatch(checkoutChange({ ...data, address_line2: event.target.value }));
  };
  const handleChangeCity = (event) => {
    dispatch(checkoutChange({ ...data, city: event.target.value }));
  };
  const handleChangeState = (event) => {
    dispatch(checkoutChange({ ...data, state: event.target.value }));
  };

  const handleFirstName = (event) => {
    dispatch(checkoutChange({ ...data, firstname: event.target.value }));
  };
  const handleLastName = (event) => {
    dispatch(checkoutChange({ ...data, lastname: event.target.value }));
  };

  const handleEmail = (event) => {
    dispatch(checkoutChange({ ...data, email: event.target.value }));
  };
  const handleChangeSex = (event) => {
    dispatch(checkoutChange({ ...data, sex: event.target.value }));
  };

  return (
    <Grid
      sx={{ pr: { xs: 0, sm: 3 }, bgcolor: "#000000" }}
      container
      spacing={2}
    >
      <Grid item xs={12} sm={8}>
        <Card sx={{ backgroundColor: "#000000" }}>
          <Box
            sx={{
              border: "8px solid #ddbc48",
              backgroundColor: "#ffffff",
              borderRadius: 3,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Box sx={{ display: { xs: "none", sm: "block" } }}>
              <Paper style={styles.paperContainer}>
                <Box
                  sx={{
                    minWidth: "200px",
                    minHeight: "600px",
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <h1></h1>
                </Box>
              </Paper>
            </Box>

            <Typography
              sx={{
                ml: -10,
                border: "1px solid #d0963d",
                p: 1,
                borderRadius: 1,
                display: { xs: "none", sm: "block" },
              }}
              textAlign={"right"}
              variant="h3"
              color="initial"
            >
              SignUp
            </Typography>

            <Box>
              <Box>
                <Box borderRadius={15} sx={{ p: 2 }} noValidate>
                  <Grid container spacing={1}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.firstname}
                        onChange={handleFirstName}
                        name="firstName"
                        required
                        fullWidth
                        id="firstName"
                        label="First Name"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.lastname}
                        onChange={handleLastName}
                        required
                        fullWidth
                        id="lastName"
                        label="Last Name"
                        name="lastName"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.email}
                        onChange={handleEmail}
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.phone}
                        onChange={handleChangePhone}
                        required
                        fullWidth
                        id="phone"
                        label="Phone"
                        name="phone"
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DatePicker
                          label="Birthday"
                          value={data.birthday}
                          onChange={(newValue) => {
                            dispatch(
                              checkoutChange({ ...data, birthday: newValue })
                            );
                          }}
                          renderInput={(params) => (
                            <TextField
                              fullWidth
                              color="doradoBtn"
                              size="small"
                              {...params}
                            />
                          )}
                        />
                      </LocalizationProvider>
                    </Grid>
                    <Grid item xs={6}>
                      <FormControl fullWidth>
                        <InputLabel
                          color="doradoBtn"
                          id="demo-simple-select-label"
                        >
                          Sex
                        </InputLabel>
                        <Select
                          fullWidth
                          size="small"
                          color="doradoBtn"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={data.sex}
                          label="Sex"
                          onChange={handleChangeSex}
                        >
                          <MenuItem value={"Male"}>Male</MenuItem>
                          <MenuItem value={"Female"}>Female</MenuItem>
                          <MenuItem value={"Other"}>Other</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                      <Grid container spacing={1}>
                        <Grid item xs={6}>
                          <TextField
                            size="small"
                            color="doradoBtn"
                            value={data.country}
                            onChange={handleChangeCountry}
                            required
                            fullWidth
                            id="country"
                            label="Country"
                            name="country"
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <TextField
                            size="small"
                            color="doradoBtn"
                            value={data.state}
                            onChange={handleChangeState}
                            required
                            fullWidth
                            id="state"
                            label="State"
                            name="state"
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.city}
                        onChange={handleChangeCity}
                        required
                        fullWidth
                        id="city"
                        label="City"
                        name="city"
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.address_line1}
                        onChange={handleChangeAddressOne}
                        fullWidth
                        id="addressone"
                        label="Address 1"
                        name="addressone"
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.address_line2}
                        onChange={handleChangeAddressTwo}
                        fullWidth
                        id="addresstwo"
                        label="Address 2"
                        name="addressone"
                      />
                    </Grid>

                    <Grid item xs={12}>
                      <TextField
                        size="small"
                        fullWidth
                        color="doradoBtn"
                        id="password"
                        type={data.showPassword ? "text" : "password"}
                        value={data.password}
                        onChange={handleChangePassword}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                              >
                                {data.showPassword ? (
                                  <VisibilityOff />
                                ) : (
                                  <Visibility />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        label={
                          <Typography variant="body1">Password</Typography>
                        }
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        size="small"
                        color="doradoBtn"
                        value={data.repeatPassword}
                        onChange={handleChangeRepeatPassword}
                        required
                        fullWidth
                        name="repeat-password"
                        label="Repeat Password"
                        type="password"
                        id="repeat-password"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <StripeForm />
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            </Box>
          </Box>
        </Card>
      </Grid>
      <Grid item xs={12} sm={4}>
        <Card sx={{ backgroundColor: "#000000" }}>
          <Box
            sx={{
              border: "8px solid #ddbc48",
              p: 1,
              pt: 1,
              minHeight: "615px",
              borderRadius: 3,

              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Extract />
          </Box>
        </Card>
      </Grid>
    </Grid>
  );
}

const Extract = () => {
  const product = useSelector((state) => state.checkout.product);
  const navigate = useNavigate();
  if (product._id == "") {
    return (
      <div>
        <Box sx={{ flexDirection: "column", alignItems: "center" }}>
          <Typography
            variant="h4"
            textAlign={"center"}
            color={"doradoBtn.main"}
          >
            Select a Subscription
          </Typography>
          <Typography
            variant="body1"
            textAlign={"center"}
            color={"doradoBtn.main"}
          >
            To register it is necessary to select a subscription, please see our
            products
          </Typography>
          <br />
          <Box fullWidth textAlign={"center"}>
            <Button
              onClick={() => navigate("/services")}
              variant="text"
              sx={{ border: "1px solid #ddbc48" }}
              color="doradoBtn"
            >
              OUR SHOP
            </Button>
          </Box>
        </Box>
      </div>
    );
  }
  return (
    <div>
      <Typography variant="h6" color="gray">
        Subscription
      </Typography>
      <Typography variant="h4" color={"doradoBtn.main"}>
        {product.name}
      </Typography>
      <br />

      <Typography variant="h6" color="gray">
        Description
      </Typography>
      <Typography variant="body2" color="gray">
        {product.descriptions}
      </Typography>
      <br />

      <Typography variant="h6" color="gray">
        Price
      </Typography>
      <Box display="flex">
        <Typography variant="h4" color={"doradoBtn.main"}>
          ${product.price}
        </Typography>
        <Typography variant="body1" color={"doradoBtn.main"}>
          /{product.type}
        </Typography>
      </Box>
      <br />
      <Box fullWidth textAlign={"center"}>
        <Button
          onClick={() => navigate("/services")}
          sx={{ border: "1px solid #ddbc48" }}
          variant="text"
          color="doradoBtn"
        >
          CHANGE SUBSCRIPTIONS
        </Button>
      </Box>

      <br />
    </div>
  );
};
