import * as React from "react";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import validator from "validator";
import Typography from "@mui/material/Typography";
import notify from "../../../redux/actions/notify.js";
import wait from "../../../redux/actions/wait.js";

import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";

const stripePromise = loadStripe(
  "pk_test_51IDcSKBg5CrIbIRKdXcfwFvXP5mUpfpWZVq7iVyWmcK9SfHFcjYhndyUr1KWp3dxAvZNbV7DbBl5OjgoHmN2F1jX00yOggHmjo"
);

export default function SignUp(props) {
  return (
    <div>
      <Elements stripe={stripePromise}>
        <Form />
      </Elements>
    </div>
  );
}

const Form = () => {
  const navigate = useNavigate();
  const stripe = useStripe();
  const elements = useElements();
  const dispatch = useDispatch();
  const checkout = useSelector((state) => state.checkout);

  const handleSubmit = async () => {
    if (validator.isEmpty(checkout.firstname, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid First Name"));
      return;
    }
    if (validator.isEmpty(checkout.lastname, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid Last Name"));
      return;
    }

    if (!validator.isEmail(checkout.email)) {
      dispatch(notify.active(true, "error", "Invalid Email"));
      return;
    }
    if (!validator.isMobilePhone(checkout.phone)) {
      dispatch(notify.active(true, "error", "Invalid Phone"));
      return;
    }
    if (validator.isEmpty(checkout.country, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid Country"));
      return;
    }
    if (validator.isEmpty(checkout.state, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid State"));
      return;
    }
    if (validator.isEmpty(checkout.city, { ignore_whitespace: true })) {
      dispatch(notify.active(true, "error", "Invalid City"));
      return;
    }
    if (!validator.isStrongPassword(checkout.password)) {
      dispatch(
        notify.active(
          true,
          "error",
          "The password needs a minimum of 8 characters, uppercase, lowercase and symbols"
        )
      );
      return;
    }
    if (checkout.password != checkout.repeatPassword) {
      dispatch(notify.active(true, "error", "The passwords do not match"));
      return;
    }
    if (checkout.product._id == "") {
      dispatch(notify.active(true, "error", "Choose a Subscription"));
      return;
    }
    //Aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
    //Aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii

    const fetchStripe = await stripe.createToken(
      elements.getElement(CardElement),
      {
        name: checkout.firstname + " " + checkout.lastname,
        address_line1: checkout.address_line1,
        address_line2: checkout.address_line2,
        address_city: checkout.city,
        address_state: checkout.state,
        address_country: checkout.country,
      }
    );
    dispatch(wait(true));

    const { error } = fetchStripe;
    if (error) {
      dispatch(wait(false));
      dispatch(notify.active(true, "error", error.code));
      return;
    }
    const fetchServer = await axios.post("/user", {
      stripe: fetchStripe,
      user: checkout,
    });
    if (fetchServer.data.status != "ok") {
      dispatch(wait(false));
      dispatch(notify.active(true, "error", fetchServer.data.message));
      return;
    }
    dispatch(wait(false));
    dispatch(notify.active(true, "success", fetchServer.data.message));

    navigate("/login");
  };
  const cardElementOptions = {
    style: {
      base: {
        color: "#666",
        fontSize: "15px",
        fontWeight: "300",
        padding: "11.4px 12px",
      },
      invalid: {
        color: "#fa755a",
        fontSize: "fa755a",
      },
    },
  };

  return (
    <div>
      <Box
        sx={{
          border: "1px solid grey",
          borderRadius: "5px",
        }}
      >
        <Box sx={{ p: 2 }}>
          <CardElement options={cardElementOptions} />
        </Box>
      </Box>

      <Button
        onClick={handleSubmit}
        fullWidth
        sx={{ mt: 2 }}
        variant="contained"
        color="doradoBtn"
        disabled={!stripe ? true : false}
      >
        <Typography variant="body1" color="#ffffff">
          Suscribe
        </Typography>
      </Button>
    </div>
  );
};
