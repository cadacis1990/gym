import React from 'react';
import MenuCustomer from './customer.jsx'
import MenuRegister from './register.jsx'
import {useSelector} from 'react-redux'

const Index = (props) => {
    const role = useSelector(state => state.user.roles)
    for (let index = 0; index < role.length; index++) {
        if (role[index].name != "client") {
            return (
                <div>
                 
                   <MenuRegister handleClose = {props.doClose}/>
               </div>
           );
         }
                    
     }
    return (
         <div>
                 <MenuCustomer handleClose = {props.doClose}/>
         
        </div>
    );
}

export default Index;
