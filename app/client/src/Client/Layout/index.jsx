import React from 'react';
import Customer from './customer.jsx'
import Register from './register.jsx'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box';
import Footer from './footer/index.jsx'
const Layout = (props) => {
    return (
        <div>
           
            <Customer>{props.children}</Customer>
        
        </div>
    );
}

export default Layout;
