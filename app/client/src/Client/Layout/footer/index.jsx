import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid'
import CardFooter from './card.jsx'
import Typography from '@mui/material/Typography'
import { Icon } from '@iconify/react';
import Button from '@mui/material/Button'

const Index = () => {
    return (
        <div>
              <Box sx={{backgroundColor:"#000000", pt:5}}>
                 <Box sx={{backgroundColor:"#000000"}}>
                  <Box display={"flex"} justifyContent={"center"} >
                      <Typography variant="h4" color="#ddbc48">
                          CONTACTOS
                      </Typography>
                  </Box>
                  <Box display={"flex"} justifyContent={"center"} >
                      <Typography variant="h2" color="#ffffff" sx={{fontWeight:600, fontSize:{xs:"30px",sm:"70px"}}}>
                          Contacta con Nosotros
                      </Typography>
                  </Box>
                  <Box display={"flex"} justifyContent={"center"} >
                      <Typography variant="body1" color="#ffffff"  sx={{fontWeight:300}} >
                        
                      </Typography>
                  </Box>
                </Box>
                <Box   sx={{pt:10, borderTop:"1px solid #ddbc48"}}>
                  <Grid container spacing={2}>
                      <Grid display={"flex"} justifyContent={"center"} xs={12} sm={4} item>
                          <Box>
                          <CardFooter 
                             icon={<Icon color="white" icon="mdi-light:map-marker" width="40" height="40" />}
                             header= "dirección"
                             link=""
                             description="Lorem describe"
                             />
                         
                         <CardFooter 
                             icon={<Icon color="white" icon="mdi-light:phone" width="40" height="40" />}
                             header= "teléfono"
                             link=""
                             description="Lorem describe"
                             />
                          </Box>
                      </Grid>
                      <Grid display={"flex"} justifyContent={"center"}  xs={12} sm={4} item>
                          <Box>
                          <CardFooter 
                             icon={<Icon color="white" icon="mdi-light:email" width="40" height="40" />}
                             header= "email"
                             link=""
                             description="Lorem describe"
                             />
                         
                         <CardFooter 
                             icon={<Icon color="white" icon="clarity:world-line" width="40" height="40" />}
                             header= "sitio web"
                             link=""
                             description="Lorem describe"
                             />
                          </Box>
                      </Grid>
                      <Grid display={"flex"} justifyContent={"center"}  xs={12} sm={4} item>
                   
                       <Box display={"flex"} flexDirection={"column"} justifyContent={"center"} sx={{mb:{xs:4, sm:0}}} >
                          <Box>
                              <Button variant="text" color="primary">
                              <Icon  icon="eva:facebook-fill"         color="#ddbc48" width="50" height="50" />
                              </Button>
                              <Button variant="text" color="primary">
                              <Icon  icon="akar-icons:twitter-fill"   color="#ddbc48" width="50" height="50" />
                              </Button>
                              <Button variant="text" color="primary">
                              <Icon  icon="akar-icons:instagram-fill" color="#ddbc48" width="50" height="50" />
                              </Button>
                         
                          </Box>

                       </Box>
                      </Grid>
                  </Grid>
                 </Box>
              </Box>
              <Box display={"flex"} justifyContent={"center"} sx={{backgroundColor:"#ddbc48"}}>
                  <Typography variant="body1" color="initial">
                      COPYRIGTH
                  </Typography>
              </Box>
        </div>
    );
}

export default Index;
