import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import { Icon } from '@iconify/react';
import {useNavigate} from 'react-router-dom'
import Button from '@mui/material/Button'
import {Link} from "react-router-dom"

const Card = (props) => {

    return (
        <div>
            <a style={{textDecoration:"none"}} href={props.link}>
          
            <Button variant="text" color="primary">
                <Box  display={"flex"} sx={{mb:4}}>
                <Box sx={{mr:1}} display={"flex"} flexDirection={"column"} justifyContent={"center"}>
                    {props.icon}
                   {/*  {<Icon icon="clarity:map-marker-line" color="white" width="50" height="50" />} */}
                </Box>
                
                <Box sx={{borderLeft:"1px solid #ddbc48", pl:2}} >
                    <Typography variant="h6" color="#ddbc48">

                    {props.header.toUpperCase()}  
                    
                    </Typography>
                    <Typography sx={{mt:1}} variant="body1" color="#ffffff">
                       {props.description}
                    </Typography>
                </Box>
            </Box>
            </Button>
         </a>
          
           
        </div>
    );
}

export default Card;
