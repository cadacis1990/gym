import React from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";

const styles = {
  paperContainer: {
    minHeight: "100vh",
    backgroundImage: `url(${"figuraAboutUs.jpg"})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
  },
};

const Index = () => {
  return (
    <div style={styles.paperContainer}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={5}></Grid>
        <Grid
          sx={{
            border: "1px solid #d29b41",
            mr: { xs: 0, sm: 6 },
            ml: { xs: 1, sm: 0 },
          }}
          item
          xs={12}
          sm={5}
        >
          <Box>
            <Box sx={{ ml: { xs: 0, sm: -5 } }}>
              <Typography
                variant="body1"
                sx={{ fontSize: "60px" }}
                color="#ffffff"
              >
                {"sobre".toUpperCase()}
              </Typography>
              <Typography
                variant="body1"
                sx={{ mt: -4, fontSize: "45px", fontWeight: 600 }}
                color="doradoBtn.main"
              >
                {"Nosotros"}
              </Typography>
            </Box>
            <Typography
              variant="body1"
              sx={{ fontSize: "16px", pr: 1 }}
              color="#ffffff"
              textAlign={"justify"}
            >
              Nosotros NosotrosNosotros Nosot rosN oso trosNo sotrosN osotros
              Nosotr os Nosotrossdf f gsfd gsf gdfsg dsf gdfs gsf gsf gdfg sfdg
              as gsfd gsfd gsf gsf gdfs gsf gfdsNosotros NosotrosNosotros Nosot
              rosN oso trosNo sotrosN osotros Nosotr os Nosotrossdf f gsfd gsf
              gdfsg dsf gdfs gsf gsf gdfg sfdg as gsfd gsfd gsf gsf gdfs gsf
              gfdsNosotros NosotrosNosotros Nosot rosN oso trosNo sotrosN
              osotros Nosotr os Nosotrossdf f gsfd gsf gdfsg dsf gdfs gsf gsf
              gdfg sfdg as gsfd gsfd gsf gsf gdfs gsf gfdsNosotros
              NosotrosNosotros Nosot rosN oso trosNo sotrosN osotros Nosotr os
              Nosotrossdf f gsfd gsf gdfsg dsf gdfs gsf gsf gdfg sfdg as gsfd
              gsfd gsf gsf gdfs gsf gfdsNosotros NosotrosNosotros Nosot rosN oso
              trosNo sotrosN osotros Nosotr os Nosotrossdf f gsfd gsf gdfsg dsf
              gdfs gsf gsf gdfg sfdg as gsfd gsfd gsf gsf gdfs gsf gfdsNosotros
              NosotrosNosotros Nosot rosN oso trosNo sotrosN osotros Nosotr os
              Nosotrossdf f gsfd gsf gdfsg dsf gdfs gsf gsf gdfg sfdg as gsfd
              gsfd gsf gsf gdfs gsf gfdsNosotrosNosotros Nosot rosN oso trosNo
              sotrosN osotros Nosotr os Nosotrossdf f gsfd gsf gdfsg dsf gdfs
              gsf gsf gdfg sfdg as gsfd gsfd gsf gsf gdfs gsf
              gfdsNosotrosNosotros Nosot rosN oso trosNo sotrosN osotros Nosotr
              os Nosotrossdf f gsfd gsf gdfsg dsf gdfs gsf gsf gdfg sfdg as gsfd
              gsfd gsf gsf gdfs gsf gfdsNosotrosNosotros Nosot rosN oso trosNo
              sotrosN osotros Nosotr os Nosotrossdf f gsfd gsf gdfsg dsf gdfs
              gsf gsf gdfg sfdg as gsfd gsfd gsf gsf gdfs gsf gfds
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default Index;
