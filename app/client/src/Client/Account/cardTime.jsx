import React from "react";
import Card from "@mui/material/Card";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
const Cardtime = ({ temp, date, day }) => {
  var opacity = 0;
  var border = "1px solid #ffffff";
  var colorDate = "#000000";
  var backColor = "#ffffff";

  if (temp == 2) {
    border = "1px solid #ffffff";
    opacity = 1;
    colorDate = "#000000";
    backColor = "#ffffff";
  }
  if (temp == 1) {
    border = "1px solid #ffffff";
    opacity = 1;
    colorDate = "#ffffff";
    backColor = "#000000";
  }
  if (temp == 0) {
    border = "1px solid #ffffff";
    opacity = "0.5";
    colorDate = "#ffffff";
    backColor = "#000000";
  }

  return (
    <div>
      <Card
        sx={{
          opacity: opacity,
          backgroundColor: backColor,
          border: border,
          p: 1,
          mb: 2,
        }}
      >
        <Box display={"flex"}>
          <Typography variant="h5" fontWeight={"600"} color="#ddbc48">
            {day}
          </Typography>
          <Box
            display={"flex"}
            flexGrow={1}
            flexDirection={"column"}
            justifyContent={"center"}
          >
            <Typography
              textAlign={"end"}
              fontWeight={"600"}
              variant="h6"
              color={colorDate}
            >
              {date}
            </Typography>
          </Box>
        </Box>
      </Card>
    </div>
  );
};

export default Cardtime;
