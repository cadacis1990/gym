import React from "react";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const Gymname = () => {
  const local = useSelector((state) => state.user.subscriptions[0].local.name);
  const suscriptions = useSelector((state) => state.user.subscriptions);

  if (suscriptions < 1) {
    return (
      <Card sx={{ pr: 2, pl: 2, pt: 3, pb: 3, mt: 2 }}>
        <Typography
          sx={{ fontWeight: 600, fontSize: { xs: "25px", sm: "40px" } }}
          variant="h4"
          color="#ddbc48"
        >
          Gimnacio
        </Typography>
        <Typography
          variant="h1"
          sx={{ fontSize: { xs: "25px", sm: "30px" } }}
          color="#ddbc48"
        >
          {""}
        </Typography>
      </Card>
    );
  }

  return (
    <div>
      <Card sx={{ pr: 2, pl: 2, pt: 3, pb: 3, mt: 2 }}>
        <Typography
          sx={{ fontWeight: 600, fontSize: { xs: "25px", sm: "40px" } }}
          variant="h4"
          color="#ddbc48"
        >
          Gimnacio
        </Typography>
        <Typography
          variant="h1"
          sx={{ fontSize: { xs: "25px", sm: "30px" } }}
          color="#ddbc48"
        >
          {local}
        </Typography>
      </Card>
    </div>
  );
};

export default Gymname;
