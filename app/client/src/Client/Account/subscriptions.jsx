import React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import userAction from "../../redux/actions/getUserData.js";
import userActionById from "../../redux/actions/getUserDataById.js";
import notify from "../../redux/actions/notify.js";
import axios from "axios";
import Dialog from "@mui/material/Dialog";
import FormStripe from "./ChangeCard/Form/index.jsx";
import wait from "../../redux/actions/wait.js";
import userUpdate from "../../redux/actions/getUserData.js";

const Subscriptions = ({
  status,
  last,
  next_pay,
  price,
  local,
  product,
  section,
  user,
  _id,
}) => {
  user = useSelector((state) => state.user);

  var dispatch = useDispatch();
  var [close, setClose] = React.useState(false);

  const handleClose = () => {
    setClose(false);
  };

  const handleOpen = () => {
    setClose(true);
  };
  const delSuscribe = async () => {
    var fetch = await axios.delete("/user/suscriptions", {
      params: {
        local: local,
        user: user._id,
        section: section,
        product: product,
        id: _id,
      },
    });

    if (fetch.data.status != "ok") {
      dispatch(notify.active(true, "error", fetch.data.message));
      return;
    }
    dispatch(notify.active(true, "success", fetch.data.message));
    dispatch(userActionById(_id));
    dispatch(userAction);
  };
  const handleSubmit = async () => {
    dispatch(wait(true));

    const fetchServer = await axios.post("/user/activesuscribe", {
      _id,
      stripe_cus: user.stripe_cus,
      user: user._id,
      product,
      section,
      local,
    });

    if (fetchServer.data.status != "ok") {
      dispatch(wait(false));
      dispatch(notify.active(true, "error", fetchServer.data.message));

      return;
    }

    dispatch(wait(false));
    dispatch(userUpdate);
    dispatch(notify.active(true, "success", fetchServer.data.message));
  };

  return (
    <div>
      <Dialog onClose={handleClose} open={close}>
        <FormStripe handleClose={handleClose} />
      </Dialog>

      <Box sx={{ p: 3, mt: 3, backgroundColor: "#e8e7e2", borderRadius: 1 }}>
        <Grid container spacing={0}>
          <Grid
            xs={12}
            sm={4}
            flexGrow={1 / 3}
            sx={{
              borderRight: { xs: "1px solid #e8e7e2", sm: "1px solid #6a6a6a" },
            }}
            display={"flex"}
            justifyContent={"center"}
            item
          >
            <Box>
              <Typography
                display={"flex"}
                justifyContent={"center"}
                variant="body1"
                sx={{ fontSize: "12px", fontWeight: 600 }}
                color={status ? "#3c9500" : "#ac0000"}
              >
                ULTIMO PAGO: {status ? "Confirmado" : "Rechazado"}
              </Typography>
              <Typography
                display={"flex"}
                justifyContent={"center"}
                variant="h4"
                sx={{ fontWeight: 600 }}
                color={!status ? "error" : "initial"}
              >
                {last}
              </Typography>

              <Typography
                display={"flex"}
                justifyContent={"center"}
                variant="body1"
                sx={{ mt: 2, fontSize: "12px", fontWeight: 600 }}
                color="initial"
              >
                PROXIMO PAGO
              </Typography>
              <Typography
                display={"flex"}
                justifyContent={"center"}
                variant="h4"
                sx={{ fontWeight: 600 }}
                color="initial"
              >
                {next_pay}
              </Typography>
            </Box>
          </Grid>
          <Grid
            xs={12}
            sm={4}
            flexGrow={1 / 3}
            display={"flex"}
            justifyContent={"center"}
            item
          >
            <Box
              sx={{ mt: { xs: 2, sm: 0 } }}
              display={"flex"}
              justifyContent={"center"}
              flexDirection={"column"}
            >
              <Typography
                display={"flex"}
                justifyContent={"center"}
                variant="body1"
                sx={{ mt: 2, fontSize: "12px", fontWeight: 600 }}
                color="initial"
              >
                PRECIO
              </Typography>
              <Typography
                display={"flex"}
                justifyContent={"center"}
                variant="h4"
                sx={{ fontWeight: 600 }}
                color="initial"
              >
                ${price}/Mes
              </Typography>
              <Typography
                display={"flex"}
                justifyContent={"center"}
                variant="body1"
                sx={{ fontWeight: 300 }}
                color="initial"
              ></Typography>
            </Box>
          </Grid>
          <Grid
            xs={12}
            sm={4}
            flexGrow={1 / 3}
            display={"flex"}
            justifyContent={"center"}
            item
          >
            <Box
              sx={{ mt: { xs: 2, sm: 0 } }}
              display={"flex"}
              justifyContent={"center"}
              flexDirection={"column"}
            >
              {!status ? (
                <Button
                  sx={{ mb: 2 }}
                  onClick={handleSubmit}
                  variant="contained"
                  color="primary"
                >
                  ACTIVAR
                </Button>
              ) : (
                ""
              )}
              {user.pay_method[0].pay_method.last4 == "Cash" ? (
                <Button
                  disabled={true}
                  onClick={handleOpen}
                  variant="contained"
                  color="doradoBtn"
                >
                  CASH
                </Button>
              ) : (
                <Button
                  onClick={handleOpen}
                  variant="contained"
                  color="doradoBtn"
                >
                  CAMBIAR TARJETA
                </Button>
              )}

              <Button
                sx={{ mt: 2 }}
                onClick={delSuscribe}
                variant="contained"
                color="error"
              >
                CANCELAR
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

export default Subscriptions;
