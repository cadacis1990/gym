import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import axios from "axios";
import { styled } from "@mui/material/styles";
import { useDispatch, useSelector } from "react-redux";
import userAction from "../../redux/actions/getUserData.js";
import userActionById from "../../redux/actions/getUserDataById.js";
import notify from "../../redux/actions/notify.js";

const Input = styled("input")({
  display: "none",
});

const Nameavatar = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const handleUpload = async (e) => {
    var files = e.target.files;
    var data = new FormData();
    data.append("avatar", files[0]);
    data.append("id", user._id);

    var fetch = await axios.post("/user/upload", data);

    if (fetch.data.status != "ok") {
      dispatch(notify.active(true, "error", fetch.data.message));
      return;
    }

    dispatch(notify.active(true, "success", "Avatar Updated"));
    dispatch(userAction);
    dispatch(userActionById(user._id));
  };
  return (
    <div>
      <label htmlFor="contained-button-file">
        <Input
          accept="image/*"
          id="contained-button-file"
          type="file"
          value={""}
          onChange={handleUpload}
        />
        <Box display={"flex"} justifyContent={"center"}>
          <Button
            displ
            color="doradoBtn"
            sx={{ mt: 3, mb: 1, "&:hover": { backgroundColor: "transparent" } }}
            style={{ "&:hover": { backgroundColor: "#000000B5" } }}
            variant="text"
            component="span"
          >
            <Avatar
              sx={{ width: 100, height: 100 }}
              alt={user.firstname}
              src={user.avatar}
            />
          </Button>
        </Box>
        <Box display={"flex"} justifyContent={"center"}>
          <Typography color="#ffffff" variant="h6">
            {user.firstname + " " + user.lastname}{" "}
          </Typography>
        </Box>
      </label>
    </div>
  );
};

export default Nameavatar;
