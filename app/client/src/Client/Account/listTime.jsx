import React from "react";
import CardTime from "./cardTime.jsx";
import { useSelector } from "react-redux";
import convertTime from "../../Component/utils/convertTime.js";
import Box from "@mui/material/Box";
const Listtime = () => {
  var sections = useSelector((state) => state.user.subscriptions[0].section);

  if (sections.length < 1) {
    return <div></div>;
  }

  var time = convertTime(sections.start_time, sections.end_time);
  var nowDay = new Date().getDay();
  var day = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes"];

  return (
    <div>
      <Box sx={{ pt: 1.5 }}>
        {day.map((item, key) => {
          var temp = 0;
          if (key + 1 < nowDay) {
            temp = 0;
          }
          if (key + 1 == nowDay) {
            temp = 1;
          }
          if (key + 1 > nowDay) {
            temp = 2;
          }

          return <CardTime key={key} day={item} date={time} temp={temp} />;
        })}
      </Box>
    </div>
  );
};

export default Listtime;
