import React from 'react';
import Card from '@mui/material/Card'
import Box from '@mui/material/Box';
import { Icon } from '@iconify/react';
import Typography from '@mui/material/Typography'
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button'
import ButtonGroup from '@mui/material/ButtonGroup';
import Container from '@mui/material/Container'
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid'
import ChangePass from '../../Component/Forms/changePassProfileClient.jsx'
import axios from 'axios';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { styled } from '@mui/material/styles';
import { useParams } from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import UpdateUserAdmin from '../../Component/Forms/updateUserAdmin.jsx'
import convertTime from '../../Component/utils/convertTime.js'
import Wait from "../../Component/utils/wait.jsx";
import AddSuscribe from "../../Component/Forms/addSuscriptionsToUser.jsx";
import AddSections from "../../Component/Forms/addSectionsToUser.jsx";
import { useDispatch, useSelector } from "react-redux";
import userAction from '../../redux/actions/getUserData.js'
import userActionById from '../../redux/actions/getUserDataById.js'
import notify from '../../redux/actions/notify.js';
import AddPayment from '../AddPayment/index.jsx'
import Paper from '@mui/material/Paper';

const styles = {
  paperContainer: {
      backgroundColor:"black",
     backgroundImage: `url(/training.jpg)`, 
      backgroundRepeat: "no-repeat",
      backgroundPosition: "bottom right",
  }
};

const Suscriptions = (props)=>{
    const {id} = useParams()
    const dispatch = useDispatch();
    const user   = useSelector(state=>state.user);
    
    const delSuscribe = async (value)=>{
         
        var fetch = await axios.delete("/user/suscriptions",{
         params:{
           local:value.local._id,
           user:id,
           section:value.section._id,
           product:value.product._id, 
           id:value._id
         }
        })

        if (fetch.data.status != "ok") {
          dispatch(notify.active(true, "error", fetch.data.message))
          return
        }
          dispatch(notify.active(true, "success", fetch.data.message))
          dispatch(userActionById(id))
          dispatch(userAction)
      }
    
       
    return(
        <div>
    
       
     
              {
                  user.subscriptions.map((item, key)=>{
  
                      return(
                        <div key={Math.random() * 10000}>
                      <Card  key = {Math.random() * 10000} sx={{backgroundColor:"#000000", mb:2, p:2, borderBottom:"1px solid #ddbc48"}}>
                       <Box key = {Math.random() * 10000} sx={{display:"flex"}}>
                        <Box key = {Math.random() * 10000} display={"flex"} flexDirection={"column"} justifyContent={"center"} sx={{mr:2,ml:1, pt:-2}}>
                           <Icon icon="bi:cash-coin" color="white" width="26" height="26" />
                        </Box>
                        <Box  key = {Math.random() * 10000} flexGrow={1} sx={{ display:"flex", justifyContent:"center"}} >
                         <Box key = {Math.random() * 10000}>
                             <Typography key = {Math.random() * 10000} style={{fontSize:16}} variant="h6" color=" white">
                               <strong>Local: </strong>{item.local.name}
                             </Typography>

                             <Typography  key = {Math.random() * 10000} style={{fontSize:16}} variant="h6" color=" white">
                              <strong> Product: </strong> {item.product.name} - <strong>${item.product.price}</strong>
                             </Typography>
                     
                             <Typography key = {Math.random() * 10000} style={{fontSize:16}} variant="h6" color=" white">
                             <strong>Time:</strong> {convertTime(item.section.start_time, item.section.end_time)}
                             </Typography>
                          </Box>
                        </Box>
                        <ButtonGroup  key = {Math.random() * 10000} variant="contained" aria-label="outlined primary button group">
                        <Button key = {Math.random() * 10000} value={item._id} onClick={()=>delSuscribe(item)} color="error"><Icon icon="clarity:close-line" color="white" width="18" height="18" /></Button>
                        </ButtonGroup>
                      </Box>
                      </Card>
           
                          </div>
                      )
                  })
                   
               }
         
    </div>

    )
}

const Input = styled('input')({
    display: 'none',
  });

const Index = () => {

const dispatch = useDispatch()
const user = useSelector(state=>state.user)
const [open, setOpen] = React.useState(false);
const [openSuscribe, setOpenSuscribe] = React.useState(false)


const handleClickOpen = () => {
    setOpen(true);
  };

 const handleUpload = async(e)=>{
   
         var files = e.target.files
         var data = new FormData();
             data.append('avatar', files[0])
             data.append('id', user._id)
       
        var fetch = await axios.post("/user/upload",data)  

        if (fetch.data.status != "ok") {
            dispatch(notify.active(true, "error", fetch.data.message))
              return
        } 

        dispatch(notify.active(true, "success", "Avatar Updated"))
        dispatch(userAction)
        dispatch(userActionById(user._id))
  
    }  
 const handleOpenSuscribe= ()=>{
        setOpenSuscribe(true)
}
 const handleCloseForm = (value) => {
        setOpen(false);
    };
const handleCloseSuscribe= ()=>{
    setOpenSuscribe(false)
 }


if (!user.hasOwnProperty('_id')) {
  return <h1>Wait for User</h1>  
}
    return (
        <div>
                 <Paper style={styles.paperContainer}>
                <Dialog onClose={handleCloseForm} open={open}>
                    <UpdateUserAdmin handleCloseForm={handleCloseForm}/>
                </Dialog>
                <Dialog onClose={handleCloseSuscribe} open={openSuscribe}>
                <AddSuscribe handleClose={handleCloseSuscribe}/>
                </Dialog>
              <Grid container spacing={2}>
                
              <Grid item  xs={12} sm={4} xl={4}>
                 <Card color='#ffffff' elevation={15} sx={{backgroundColor:"#000000", alignItems:"center" ,p:3, borderRadius:8, border:"1px solid #ddbc48"}}>
        
                           <Box style={{ display:'flex', justifyContent:'center' }}>
                               <Avatar   sx={{width:250, height:250}} alt="Remy Sharp" src={user.avatar} />
                           </Box>
                           <Box style={{ display:'flex', justifyContent:'center' }}>
                           
                            <Stack direction="row" alignItems="center" spacing={2}>
                                 <label htmlFor="contained-button-file">
                                     <Input accept="image/*" id="contained-button-file"  type="file" value={""} onChange={handleUpload} />
                                     <Button color="doradoBtn" sx={{mt:3, mb:3}} variant="contained" component="span">
                                        Subir
                                     </Button>
                                 </label>
                             </Stack>
                          
                        </Box>
                            <Divider sx={{mt:2}} />
                   
                        <Box style={{ display:'flex', justifyContent:'center' }}> 
                          <Typography color="#ffffff" variant="h4" >{user.firstname+" "+user.lastname }   </Typography>
                        </Box>
                        
                         <Divider color="#ddbc48" sx={{mt:2, mb:2}}  />

                        <Box style={{ display:'flex' }}> 
                            <Box sx={{pt:0.5}}>
                            <Icon  icon="carbon:email" color="white" width="26" height="26" />&nbsp;&nbsp;
                            </Box>
                            <Typography color="#ffffff" variant="h6">{user.email}</Typography>
                        </Box>
                        <Box style={{ display:'flex', }}> 
                            <Box sx={{pt:0.5}}>
                            <Icon icon="bi:phone-vibrate" color="white" width="26" height="26" />&nbsp;&nbsp;
                            </Box>
                            <Typography variant="h6" color="#ffffff">{user.phone}</Typography>
                        </Box>
                        <Box style={{ display:'flex' }}> 
                            <Box sx={{pt:1}}>
                            <Icon icon="clarity:map-marker-line" color="white" width="26" height="26" />
                            </Box>
                            <Typography variant="h6" color="#ffffff">{user.address_line1 + "," +  user.address_line2}</Typography>
                        </Box>
                        <Box style={{ display:'flex', justifyContent:'center' }}>
                            <Button color="doradoBtn"  onClick={handleClickOpen}  sx={{mt:1}} variant="contained">
                               Editar
                            </Button>
                            </Box>
                       
                   
                   </Card>
               </Grid>
               <Grid item  xs={12} sm={4} xl={4}>
                   <Grid container spacing={2}>
                      <Grid item  xs={12} sm={12} xl={12}>
                            <Card  elevation={15} sx={{backgroundColor:"#000000", alignItems:"center" ,p:3, borderRadius:8, border:"1px solid #ddbc48"}}>
                            <Box style={{ display:'flex', justifyContent:'center' }}> 
                                    <Typography color="#ddbc48" variant="h4" >{"Subscriptions" }   </Typography>
                                    </Box>
                            <Suscriptions/>
                            <Box style={{ display:'flex', justifyContent:'center' }}>
                                            <Button color="doradoBtn"  onClick={handleOpenSuscribe}  sx={{mt:2}} variant="contained">
                                                Añadir Subscriptions
                                            </Button>
                                </Box>
                            </Card>
                      </Grid>
                      <Grid item  xs={12} sm={12} xl={12}>
                            <Card  elevation={15} sx={{backgroundColor:"#000000", alignItems:"center" ,p:3, borderRadius:8, border:"1px solid #ddbc48"}}>
                                 
                                <Box style={{ display:'flex', justifyContent:'center' }}> 
                                   <Typography color="#ddbc48" variant="h4" >{"Change Password" }   </Typography>
                                </Box>
                                <Box sx={{minHeight:"50%"}}>
                           
                                <ChangePass/>
                          
                                 </Box>
                            </Card>
                      </Grid>      
                   </Grid>
              
               </Grid>
               <Grid item sx={{pr:3}}  xs={12} sm={4} xl={4}>
               <Card sx={{mr:4}}  elevation={15} sx={{backgroundColor:"#000000", alignItems:"center" ,p:3, borderRadius:8, border:"1px solid #ddbc48"}}>
               <AddPayment/>
               </Card>
              </Grid>
             </Grid>  

             </Paper>
        </div>
          );
}

export default Index;