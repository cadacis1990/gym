import React from "react";

import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Dialog from "@mui/material/Dialog";
import UpdateUserAdmin from "../../Component/Forms/updateUserAdmin.jsx";
import AddSuscribe from "../../Component/Forms/addSuscriptionsToUser.jsx";
import { useDispatch, useSelector } from "react-redux";
import Paper from "@mui/material/Paper";
import AvatarName from "./nameAvatar.jsx";
import GymName from "./gymName.jsx";
import ListTime from "./listTime.jsx";
import Subscriptions from "./listSuscribe.jsx";
import Suscribe from "../Suscribe/index.jsx";

const styles = {
  paperContainer: {
    backgroundColor: "black",
    backgroundImage: "url(training.jpg)",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "bottom right",
  },
};

const Index = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [open, setOpen] = React.useState(false);
  const [openSuscribe, setOpenSuscribe] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleOpenSuscribe = () => {
    setOpenSuscribe(true);
  };
  const handleCloseForm = (value) => {
    setOpen(false);
  };
  const handleCloseSuscribe = () => {
    setOpenSuscribe(false);
  };

  if (!user.hasOwnProperty("_id")) {
    return <h1>Wait for User</h1>;
  }
  if (user.subscriptions.length < 1) {
    return <Suscribe />;
  }

  return (
    <div>
      <Paper style={styles.paperContainer}>
        <Dialog onClose={handleCloseForm} open={open}>
          <UpdateUserAdmin handleCloseForm={handleCloseForm} />
        </Dialog>
        <Dialog onClose={handleCloseSuscribe} open={openSuscribe}>
          <AddSuscribe handleClose={handleCloseSuscribe} />
        </Dialog>
        <Box
          sx={{ p: 3, m: { xs: 0, sm: 5 }, border: "1px solid #ddbc48" }}
          minHeight={"95vh"}
        >
          <Grid container spacing={6}>
            <Grid xs={12} sm={6} xl={4} item>
              <AvatarName />
              <GymName />
            </Grid>
            <Grid sx={{ pt: 1 }} item xs={12} sm={6} xl={4}>
              <ListTime />
            </Grid>
          </Grid>
          <Grid container spacing={6}>
            <Grid sm={12} xl={8} item>
              <Box>
                <Subscriptions />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    </div>
  );
};

export default Index;
