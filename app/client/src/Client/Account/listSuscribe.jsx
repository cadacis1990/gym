import React from "react";
import Subscriptions from "./subscriptions.jsx";
import { useSelector } from "react-redux";
const Listsuscribe = () => {
  const user = useSelector((state) => state.user);
  if (user.subscriptions.length < 1) {
    return <div></div>;
  }

  return (
    <div>
      {user.subscriptions.map((item, key) => {
        var last_pay = new Date(item.last_pay);
        var next_pay = "";
        if (last_pay.getMonth() == 11) {
          next_pay =
            last_pay.getDate() + "/" + 1 + "/" + last_pay.getFullYear() + 1;
        } else {
          next_pay =
            last_pay.getDate() +
            "/" +
            (parseInt(last_pay.getMonth()) + 2) +
            "/" +
            last_pay.getFullYear();
        }
        last_pay =
          last_pay.getDate() +
          "/" +
          (parseInt(last_pay.getMonth()) + 1) +
          "/" +
          last_pay.getFullYear();
        return (
          <Subscriptions
            key={key}
            _id={item._id}
            status={item.status}
            last={last_pay}
            next_pay={next_pay}
            price={item.charge / 100}
            local={item.local._id}
            product={item.product._id}
            section={item.section._id}
            user={user._id}
          />
        );
      })}
    </div>
  );
};

export default Listsuscribe;
