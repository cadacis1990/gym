import React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import axios from 'axios'
import {useParams} from 'react-router-dom'
import {useNavigate} from 'react-router-dom'

const Index = () => {
   const {id} = useParams()
   const navigate = useNavigate()

   const [data, setData] = React.useState({
       status:true,
       message:"Waiting to confirm email",
   })

  const fetch = async()=>{
      const fetch = await axios.get("/user/confirm",{
          params:{
              token:id
          }
      })
      if (fetch.data.status != "ok") {
  
          setData({status:false, message:fetch.data.message})
          return
      }
          setData({status:false, message:fetch.data.message})
   
  }


  React.useEffect(()=>{
      fetch()  
  },[])

    return (
        <div>
          
 <Box sx={{minHeigth:"95vh", display:"flex", justifyContent:"center",mt:15}}>
     <Box sx={{minHeigth:"95vh",  display:"flex",flexDirection:"column", justifyContent:"center"}}>
         <Card  elevation={15} sx={{backgroundColor:"#000000", alignItems:"center" ,p:3, borderRadius:8, border:"1px solid #ddbc48", minWidth:"300px"}}>
         <Box sx={{minHeigth:"95vh", display:"flex", justifyContent:"center"}}>
             <Typography variant="body1" color="#ffffff">
                   {data.message}
             </Typography>
         </Box>
          
          <Box sx={{minHeigth:"95vh", display:"flex", justifyContent:"center", mt:2}}>
        
                <Button disabled={data.status} onClick={()=>navigate("/login")} style={{ "&:disabled": {backgroundColor:'red'}}} variant="contained" color="doradoBtn">
                    Go Login
                </Button>
       
          </Box>  
         </Card>   
     </Box>
 </Box>
        </div>
    );
}

export default Index;
