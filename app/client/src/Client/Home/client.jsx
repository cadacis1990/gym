import React from 'react';
import Typography from '@mui/material/Typography'
import Image from 'material-ui-image'
import Box from '@mui/material/Box';
import Cartel from './unete.jsx' 
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper';
/* import Images from 'D:/project/gym/build/training.jpg' */; // Import using relative path

const styles = {
    paperContainer: {
        backgroundColor:"black",
        backgroundImage: `url(/training.jpg)`,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "bottom right",
        
        
    }
}; 

const IndexHight = () => {
    return (
        <div>
             <Paper style={styles.paperContainer}>
               <Box >
               <Grid container spacing={1}>
                
               <Grid item xs={12} sm={6} > 
             <Box    
                minHeight={"90vh"}
                display={"flex"}
                flexDirection={"column"}
                justifyContent="center"
             >
                 
             <Cartel/>
                 
              
               </Box>
            </Grid>

            <Grid  item xs={12} sm={6} > 

            </Grid>


            </Grid>
            </Box>
            </Paper>
            
         
        </div>
    );
}
const IndexSmall = () => {
    return (
        <div>
            
            <Box >
            <Grid container spacing={1}>
                
            <Grid item xs={12} sm={6} > 
           
                 <Image src={"/training.jpg"}/>
            </Grid>

            <Grid  item xs={12} sm={6} >     
                 <Cartel/>
            </Grid>

            
            </Grid>
            </Box>
       
        </div>
    );
}

const Index = () => {
    return (
        <div>
            <Box sx={{display:{xs:"none", sm:"block"}}} >
               <IndexHight/>
            </Box>
            <Box sx={{display:{xs:"block", sm:"none"}}} >
              <IndexSmall/>
            </Box>
            
        </div>
    );
}


export default Index;
