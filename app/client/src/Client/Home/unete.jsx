import React from 'react';
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button'
import {useNavigate} from 'react-router-dom'
const Header = () => {
    
    
    return (
        <div>
            {/*escritorio*/}
            <Box sx={{display:{xs:"none", sm:"block"}}}>
            <Typography variant="h3" style={{fontWeight:300, fontSize:50}} color="#ddbc48">CONSIGUE UNA</Typography>
            <Typography variant="h3" sx={{mt:-1}} style={{fontWeight:400, fontSize:42}}  color="#ffffff">FIGURA CORPORAL</Typography>
            <Typography variant="h3" sx={{mt:-2}} style={{fontWeight:800, fontSize:78}}  color="#ddbc48">PERFECTA</Typography>
            </Box>
             {/*Movil*/}
            <Box  sx={{display:{xs:"block", sm:"none"}}}>
            <Typography variant="h3" style={{ fontWeight:300, fontSize:32}} color="#ddbc48">CONSIGUE UNA</Typography>
            <Typography variant="h3"  sx ={{mt:-1}} style={{fontWeight:400, fontSize:26}}  color="#ffffff">FIGURA CORPORAL</Typography>
            <Typography variant="h3"  sx ={{mt:-1}} style={{fontWeight:800, fontSize:48}}  color="#ddbc48">PERFECTA</Typography>
            </Box>
        </div>
    );
}
const Descriptions = () => {
    return (
        <div>
            <Typography variant="body1"  color="#ffffff">
               Lorem ipsum dolor. Nunc eget mi ac massa mus et viverra molestie. Donec molestie egestas magna. Etiam purus justo, sodales mattis arcu sed, gravida cursus augue.
            </Typography>
        </div>
    );
}
const Unete = () => {
    const navigate = useNavigate()
    return (
        <div>
            <Box sx={{ml:{ sm:13}, p:2, width:{xs:"100%", sm:350}, borderRadius:3,  borderStyle:"solid", border:2, borderColor:"#ddbc48", backgroundColor:"#000000B5"}}>
            <Box sx={{ml:{ sm:-13}}}>
            <Header/>
            </Box>
            <Descriptions/>
            </Box>
    
            <Button onClick={()=>navigate("/signup")} sx={{ml:{ sm:13}, mt:3, fontWeight:100}} variant="contained" color="doradoBtn" >
                <Typography variant="body1" color="black">UNETE A NOSOTROS  </Typography>

                </Button>
  
              
            
        </div>
    );
}

export default Unete;
