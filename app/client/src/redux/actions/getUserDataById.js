import axios from 'axios'


const get = (id) => async(dispatch, getSate)=>{
    var fetch = await axios.get("/user/profile/",{
        params:{
            _id:id
        }
    });
    dispatch({type:"userbyid@get", payload:fetch.data.users});
} 

export default get
