import axios from 'axios'


const get = (id) => async(dispatch, getSate)=>{
    var fetch = await axios.get("/products/byid",{
        params:{
            _id:id
        }
    }) 

    dispatch({type:"productbyid@get", payload:fetch.data.products});
} 

export default get
