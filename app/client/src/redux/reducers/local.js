const defaultState = [];

function reducer(state=defaultState, action){
    switch (action.type) {
        case "local@get":
            return action.payload
            break;
        default:
            return state
    }
}

export default reducer;