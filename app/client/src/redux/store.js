import {createStore, combineReducers, applyMiddleware} from 'redux'
import user from './reducers/user.js';
import userbyid from './reducers/userById.js';
import product from './reducers/product.js'
import productById from './reducers/productById.js'
import local from './reducers/local.js'
import localById from './reducers/localById.js'
import dataDashboard from './reducers/dataDashboard.js'
import notify from './reducers/notify.js'
import wait from './reducers/wait.js'
import checkout from './reducers/checkout.js'
import sections from './reducers/sections.js'
import thunk from "redux-thunk";
import {composeWithDevTools} from 'redux-devtools-extension'

const reducer = combineReducers({
    user,
    product,
    productById,
    local,
    localById,
    dataDashboard,
    userbyid,
    notify,
    checkout,
    sections,
    wait
});

const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(thunk))
  );

export default store;