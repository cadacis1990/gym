import React from 'react';
import CardHours from "../hours/card.jsx";
import AddHours from "../../Lists/addHours.jsx";
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Box from '@mui/material/Box';
import createTime from "../../utils/convertTime.js"
import {useDispatch, useSelector} from 'react-redux'
import {useParams} from 'react-router-dom'

const Index = (props) => {
    const {id} = useParams();
    const data = useSelector((state)=>state.local);
    var   filter = data.filter((value)=>{
        if (value._id == id) {
            return value
        }
    })   
          filter = filter[0]
    //const dispatch = useDispatch();
 
    const [day, setDay] = React.useState(1);

        const handleDayLun = ()=>{
            setDay(1)
        }
        const handleDayMar = ()=>{
            setDay(2)
        }
        const handleDayMie = ()=>{
            setDay(3)
            
        }
        const handleDayJue = ()=>{
            setDay(4)
          
        }
        const handleDayVie = ()=>{
            setDay(5)
         
        }
        const handleDaySab = ()=>{
            setDay(6)
          
        }
        const handleDayDom = ()=>{
            setDay(7)
        }

    React.useEffect(()=>{
  
    },[day]) 


    var createRender = [];

    for (let index = 0; index < filter.sections.length; index++) {
        
        if(filter.sections[index].weekDay == day){

            var timeString = createTime(filter.sections[index].start_time, filter.sections[index].end_time);
            createRender.push( 
              <CardHours key={index} 
                          handleUpdate={props.handleUpdate}
                          handleNotify={props.handleNotify}
                          id={filter.sections[index]._id} 
                          hour={timeString} 
                          maxUser={filter.sections[index].maxUser}
                          totalUser={filter.sections[index].users.length}
                          status={filter.sections[index].status}
                          localId={props.localId}
              /> 
              
       );
        }
        
    }
    return (

        <div>
          {/*   <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                '& > *': {
                m: 1,
                },
               
            }}
            >
            <ButtonGroup variant="contained" aria-label="outlined button group">
                <Button onClick={handleDayLun} color={day == 1 ? "error":"primary" }>Lun</Button>
                <Button onClick={handleDayMar} color={day == 2 ? "error":"primary"} >Mar</Button>
                <Button onClick={handleDayMie} color={day == 3 ? "error":"primary"} >Mie</Button>
                <Button onClick={handleDayJue} color={day == 4 ? "error":"primary" }>Jue</Button>
                <Button onClick={handleDayVie} color={day == 5 ? "error":"primary" }>Vie</Button>
                <Button onClick={handleDaySab} color={day == 6 ? "error":"primary" }>Sab</Button>
                <Button onClick={handleDayDom} color={day == 7 ? "error":"primary" }>Dom</Button>
            </ButtonGroup>
            </Box> */}
             <AddHours handleNotify={props.handleNotify} handleUpdate={props.handleUpdate} day={day} localId={props.localId}/>
             {createRender}
        </div>

    );
}

export default Index;
