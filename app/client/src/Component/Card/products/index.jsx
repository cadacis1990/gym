import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ButtonGroup from '@mui/material/ButtonGroup';
import { Icon } from '@iconify/react';
import { Link } from "react-router-dom";
import axios from "axios"

export default function OutlinedCard(props) {
  var fetchDelete = async ()=>{
    if (props.users>0) {
       props.handleNotify({
         status:true,
         color:"error",
         msg:"No se puede eliminar este paquete tiene clientes suscritos"
       })
       return
    }
    var fetch = await axios.delete("/products/" + props.id,{
     })
     if (fetch.data.status != "ok") {
         props.handleNotify({
           status:true,
           color:"error",
           msg:fetch.data.message
         })
         return
     }
     props.handleNotify({
      status:true,
      color:"success",
      msg:fetch.data.message
    }) 
    props.handleSuccess()
   }
 
  return (
    <Box sx={{ minWidth: 275 }}>
      <Card variant="outlined">
            <React.Fragment>
            <CardContent>
            <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
               Paquete:
            </Typography>
            <Typography variant="h4" component="div">
              {props.name}
            </Typography>
            <Typography sx={{fontSize:16, mt: 1.5 }}  variant="body2" >
            <strong>Precio:</strong> {props.price}
            </Typography>
            <Typography sx={{fontSize:16, mt: 1.5 }}  variant="body2" >
            <strong>Tipo:</strong>{props.type}
            </Typography>
            <strong>Descripcion:</strong> 
            <div className="Container" dangerouslySetInnerHTML={{__html:props.descriptions}}></div>
            <Typography sx={{ fontSize: 16 }} variant="body2">
           
              
            </Typography>
            <Typography sx={{ fontSize: 16 }} variant="body2">
            <strong>Suscriptores:</strong> {props.users}
            </Typography>
            </CardContent>
            <CardActions>
                <ButtonGroup variant="contained" aria-label="outlined primary button group" disableElevation>
                <Link style={{textDecoration:"none"}} to={"/dashboard/product/"+props.id}> <Button color="primary">Editar</Button></Link>
                <Button sx={{ml:2}} onClick={fetchDelete} color="error">Eliminar</Button>
                </ButtonGroup>
            </CardActions>
        </React.Fragment>
      </Card>
    </Box>
  );
}