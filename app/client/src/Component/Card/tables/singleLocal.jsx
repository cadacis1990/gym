import React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import  Actions  from "../../Lists/actionList.jsx";
import AvatarTable from "./avatarTable.jsx";
import createHour from "../../utils/convertTime.js"
import AddUserAdmin from "../../Lists/addUserAdmin.jsx";
import {useDispatch, useSelector} from 'react-redux'
import {useParams} from 'react-router-dom'



const Singellocal = (props) => {
            const {id} = useParams()
            const fetch  = useSelector(state=>state.local)
            var   filter = fetch.filter((value)=>{
                 if (value._id == id) {
                    return value
                 }
            })
                  filter = filter[0]
              
            var dataNew = []
       
           filter.sections.map((value, key)=>{
        
                var st = value.start_time;
                var et = value.end_time;
                var day = value.weekDay
                
                value.users.map((value, key)=>{
     
                    if (value.products.length <= 0) {
                        var product=0
                    }else{
                        var product = value.products[0].name + " - $" + value.products[0].price
                    }
                    if (!value.pay_status) {
                        var pay_status = "Pendiente"
                    }else{
                        var pay_status = "Pagado"
                    }
                    const weekDay = (value)=>{
                        var day = "";
                        switch (value) {
                            case 1:
                              day = "Lunes"  
                                break;
                            case 2:
                              day = "Martes"   
                                break;  
                            case 3:
                              day = "Miercoles"    
                                break;
                            case 4:
                                day = "Jueves"   
                                break;
                            case 5:
                                day = "Viernes"         
                                break;  
                            case 6:
                                day = "Sabado"         
                                break;
                            case 7:
                                day = "Domingo"  
                                break;                        
                            default:
                                break;
                        }
                        return day
                    }
                    var pay_string = []
                    value.pay_method.map((value)=>{
                        
                            pay_string.push("(**** **** **** " + value.pay_method.last4+") ")
                    
                    })
                    
                    dataNew.push({  id:value._id, 
                                    avatar:value.avatar,
                                    fullname:value.firstname + " " + value.lastname, 
                                    email:value.email, 
                                    time:createHour(st,et),
                                    paquete:product,
                                    pay_status:pay_status,
                                    pay_method: pay_string,
                                    status:value.client_status,
                                    
                                    createdAt:value.createdAt 
                                    }
                                    )
                 
                })
            
             })
            
             //ordenar
     
             dataNew = dataNew.sort(function (a, b) {
                if (a.createdAt > b.createdAt) {
                  return -1;
                }
                if (a.createdAt < b.createdAt) {
                  return 1;
                }
                // a must be equal to b
                return 0;
              });
          
  

          
         const columns = [
         {
           field: 'accion',
           headerName: 'Accion',
           width: 100,
           renderCell: (params) => {
             return(

               <Box   sx={{display: 'flex',  justifyContent: 'center'}}>
                   <Actions 
                            handleNotify={props.handleNotify} 
                            handleUpdate={props.handleUpdate}
                            data={{id:params.row.id, status:params.row.status, pay_status:params.row.pay_status}}
                    />
               </Box>
       
               )
           }

         },
         {
            field: 'present',
            headerName: '',
            width: 30,
            editable: true,
            renderCell: (params) => {
             
                    return(
                        <AvatarTable avatar={params.row.avatar} status={params.row.status} name={params.row.firstname} />
                    )
              },
          },
         {
           field: 'fullname',
           headerName: 'Nombre Completo',
           width: 200,
       
         },
         {
           field: 'email',
           headerName: 'Email',
           width: 200,
         
         },
         {
           field: 'time',
           headerName: 'Horario',
           width: 100,
         
         },
    
         {
          field: 'paquete',
          headerName: 'Paquete',
          width: 250,
          
        },
  
        {
            field: 'pay_method',
            headerName: 'Metodo de Pago',
            width:150,
            
        },
        {
          field: 'pay_status',
          headerName: 'Pago',
          width: 100,
        },
       
         
       ];

 

    return (
        <div>
             <AddUserAdmin handleUpdate={props.handleUpdate}   handleNotify={props.handleNotify} localId={props.id}/>
                    <DataGrid
                            pagination
                            columns={columns}
                            rows={dataNew}
                            autoHeight={true}
                            rowsPerPageOptions={[5, 10, 20, 100]}
                            localeText={{
                            toolbarDensity: 'Size',
                            toolbarDensityLabel: 'Size',
                            toolbarDensityCompact: 'Small',
                            toolbarDensityStandard: 'Medium',
                            toolbarDensityComfortable: 'Large',
                            }}
                            components={{
                            Toolbar: GridToolbar,
                            }}
                        />
        </div>
          );
}


export default Singellocal;
