import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import CardHeader from "./CardHeader.jsx";
import CardIcon from "./CardIcon.jsx";
import CardFooter from "./CardFooter.jsx";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../assets/jss/material-dashboard-react/views/dashboardStyle.js"

const useStyles = makeStyles(styles);

const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
  </Box>
);

export default function BasicCard(props) {
  const children = props.children;
  const classes = useStyles();
  return (
    <div>
    <Card  sx={{pt:4, borderRadius:5}} elevation={15}>
            <CardHeader color="warning" stats icon>
              <CardIcon color={props.iconboxcolor}  sx={{ width: 300 }}>
              
              {props.iconsview} 
             
              </CardIcon>
               <p className={classes.cardCategory}>{props.tag}</p>
               <Box sx={{display:"flex", flexDirection: 'row', marginRight:"0",  justifyContent: 'flex-end' }}>
               <Typography  variant={props.variantValues} color="initial">{props.value}</Typography>
               <Typography variant="h6" color="gray">{props.prefix}</Typography>
               </Box>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
               
              </div>
            </CardFooter>
          
              {props.children}
           
          </Card>
        
    </div>
  );
}