import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ButtonGroup from '@mui/material/ButtonGroup';
const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);

export default function BasicCard(props) {
  return (
    <Card sx={{ minWidth: 275 }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          Producto
        </Typography>
        <Typography variant="h3" component="div">
          {'$' + props.precio}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          {props.time}
        </Typography>
        <Typography variant="body2">
          {props.descripcion}
         
        </Typography>
      </CardContent>
       <ButtonGroup disableElevation sx={{p:3}} variant="contained" aria-label="outlined primary button group">
          <Button color="secondary">Editar</Button>
          <Button color="error">Eliminar</Button>
       </ButtonGroup>
    </Card>
  );
}