import * as React from 'react';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import Box from '@mui/material/Box';

import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

const columns = [
  { field: 'id', headerName: 'No', width: 90 },
  {
    field: 'firstName',
    headerName: 'Nombre(s)',
    width: 150,
    editable: true,
  },
  {
    field: 'lastName',
    headerName: 'Apellidos',
    width: 150,
    editable: true,
  },
  {
    field: 'category',
    headerName: 'Categoria',
    width: 100,
    editable: true,
  },
  {
    field: 'local',
    headerName: 'Local',
    width: 100,
    editable: true,
  },
  {
    field: 'email',
    headerName: 'Email',
    width: 200,
    editable: true,
  },
  {
    field: 'pay',
    headerName: 'Metodo de Pago',
    width: 200,
    editable: true,
  },
];

function getData(){
    ///consultar la api aqui
    var rows = [];
    for (let index = 0; index < 10; index++) {
      rows.push({id:index,firstName:" faker.name.firstName()", lastName: "faker.name.lastName()",category:"categoria", local:"faker.name.findName()", email:"faker.internet.email()", pay:"CARD"})
    }
    return rows;
}

export default function DataGridDemo() {

  const [data, setData] = React.useState(getData());
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
 
  const updateData = ()=>{
    setData(getData());
  }
  const handleClickError = () => {
    setError(true);
  };
  const handleClickSuccess = () => {
    setSuccess(true);
  };

  const handleClose = () => {
    setError(false);
    setSuccess(false);
  };
  return (
  
    <div style={{ height: 400, width: '100%' }}>
            <Stack spacing={2} sx={{ width: '100%' }}>
              
              <Snackbar open={success} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                  This is a success message!
                </Alert>
              </Snackbar>
              <Snackbar open={error} autoHideDuration={3000} onClose={handleClose}>
                  <Alert severity="error">This is an error message!</Alert>
              </Snackbar>
          </Stack>
    
        <Box borderRadius={5}  sx={{p:2, backgroundColor:"#f2f2ff"}}> 
       
        <Box borderRadius={5} sx={{backgroundColor:"white", p:2}}>
        <DataGrid
                          pagination
                          rowsPerPageOptions={[5, 10, 20, 100]}
                          columns={columns}
                          rows={data}
                          autoHeight={true}
                          localeText={{
                          toolbarDensity: 'Size',
                          toolbarDensityLabel: 'Size',
                          toolbarDensityCompact: 'Small',
                          toolbarDensityStandard: 'Medium',
                          toolbarDensityComfortable: 'Large',
                          }}
                          components={{
                          Toolbar: GridToolbar,
                          }}
                      /> 
      </Box>
      </Box>
    </div>
  );
}