import React from "react";
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';


 const Test = (props)=>{
    function update (){
        props.updateRole(); 
     }
    return(

        <IconButton  color="menuBtnMovil"  aria-label="open drawer"   edge="start"  onClick={update}>
              <Button variant="contained">{props.roles}</Button>
        </IconButton>  

    )
  }

  export default Test;