
import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import EditIcon from '@mui/icons-material/Edit';
import Divider from '@mui/material/Divider';
import ArchiveIcon from '@mui/icons-material/Archive';
import Box from '@mui/material/Box';
import { Icon } from '@iconify/react';
import { useNavigate } from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import productUpdate from '../../redux/actions/getProduct.js'
import notify from '../../redux/actions/notify.js'
import axios from "axios";

const StyledMenu = styled((props) => (
  
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity,
        ),
      },
    },
  },
}));

export default function CustomizedMenus(props) {
  const dispatch = useDispatch()
  var navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {

    setAnchorEl(null);
  };

  const delSuscribe = async ()=>{
    var fetch = await axios.delete("/user/suscriptions",{
     params:{
      user_id:props.data.id,
      suscriptions_id:props.data.suscriptions_id
     }
    })
    if (fetch.data.status != "ok") {
    dispatch(notify.active(true, "error", fetch.data.message))
     return
    }
    dispatch(notify.active(true, "success", fetch.data.message))
    dispatch(productUpdate)
    handleClose();
  }

  const handlePay = async ()=>{
 
      var fetch = await axios.get("/user/statuspay",{
         params:{
            _id:props.data.id
         }
      })
      if (fetch.data.status != "ok") {
          dispatch(notify.active(true, "error", fetch.data.message))
         return
      }
      dispatch(notify.active(true, "success", fetch.data.message))
      dispatch(productUpdate)
      handleClose();


  }

  const goEdit = () => {
      navigate("/dashboard/users/" + props.data.id + "/edit", { replace: true });
  };

  const getLedPay = () => {
    if (props.data.pay_status == "Pendiente") {
      return(
        <MenuItem onClick={handlePay}  disableRipple>
        <Icon icon="akar-icons:circle-check-fill" color="green" width="20" height="20" inline={true} />&nbsp;&nbsp;Marcar Pago
        </MenuItem>
       )
    }
      return(
        <MenuItem  onClick={handlePay}  disableRipple>
        <Icon icon="akar-icons:circle-check-fill" color="red" width="20" height="20" inline={true} />&nbsp;&nbsp;Marcar no Pago
        </MenuItem>
      )
  }
  return (
    <div>
      <Box > 
      <Button variant="text">
      <Icon icon="akar-icons:more-vertical-fill"
            width="24" 
            height="24" 
            onClick={handleClick}            
      />
      </Button>
      </Box>

      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >  
        <Box
      sx={{
        display: 'flex',
        '& > *': {
          m: 1,
        },
      }}
    >
      
    </Box>
        <MenuItem  onClick={goEdit} disableRipple>
          <EditIcon />
         Ver Usuario
        </MenuItem>
        <Divider sx={{ my: 0.5 }} />
           {getLedPay()}

        <MenuItem onClick={delSuscribe} disableRipple>
        <ArchiveIcon />
           Cancelar Suscripcion
        </MenuItem>
      </StyledMenu>
    </div>
  );
}