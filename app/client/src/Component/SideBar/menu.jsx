import React from 'react';
import {Link} from 'react-router-dom';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Icon } from '@iconify/react';
import { useNavigate } from "react-router-dom";
import Button from '@mui/material/Button'
import {useDispatch, useSelector} from 'react-redux'
import actionUser from '../../redux/actions/getUserData.js'
import Cookies from 'js-cookie'

const Menu = (props) => {
    
    const navigate = useNavigate();
   
    const itemc = ( texto, link, icon ) =>{
        const dispatch = useDispatch() 
        const breakUser = ()=>{
           Cookies.remove('jwt')
           Cookies.remove("userId")
           dispatch(actionUser)
   
         }
        if (texto == "Salir") {
            return( 
                 
                     <ListItem button>
                           {icon}
                           <ListItemText onClick={breakUser} primary={ <Typography variant="h6" style={{color:"white"}} sx={{ fontWeight:500, fontSize:18, ml:2}} >{texto}</Typography>} />
                     </ListItem>
        
                ) 
        }

        return( 
            <Link to={link} style={{ textDecoration: 'none', color:"white"}}>     
                    <ListItem button onClick={props.doClose}>
                        {icon}
                        <ListItemText primary={ <Typography variant="h6" sx={{fontWeight:500, fontSize:18, ml:2}} >{texto}</Typography>} />
                    </ListItem>
            </Link>
       )
     }


    return (
        <div>
        <Toolbar />
        <Typography variant="h7">
        <Divider />
        </Typography>
            <Box >
            <List >
                {itemc("Dashboard","/dashboard",<Icon icon="mdi-light:view-dashboard" width="28" height="28" />)}
                {itemc("Locales","/dashboard/locals", <Icon icon="mdi-light:home" width="28" height="28" />)}
                {/*      {itemc("Paquetes","/dashboard/products",<Icon icon="mdi-light:cart" width="28" height="28" />)} */}
                
                {/* {itemc("Suscripciones","/dashboard/suscripciones",<Icon icon="mdi-light:credit-card" width="28" height="28" />)} */}
                 {/*  {itemc("Reportes","/dashboard/reportes",<Icon icon="mdi-light:clipboard-check" width="28" height="28" />)} */}
                {itemc("Salir","/dashboard/exit", <Icon icon="mdi-light:logout" width="28" height="28" color="white" />)}
               
            
            </List>
            </Box>
      </div>
    );
}

export default Menu;


