import React, { PureComponent } from 'react';
import ReactApexChart from 'react-apexcharts'

export default class ApexChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    
      series: [{
        name: 'VIP',
        data: [31, 40, 28, 51, 42, 109, 100]
      }, {
        name: 'Standar',
        data: [11, 32, 45, 32, 4]
      }],
      options: {
        chart: {
          height: 350,
          type: 'area'
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'smooth'
        },
        xaxis: {
          type: 'string',
          categories: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio"]
        },

      },
    
    
    };
  }
  render() {
    return (
        <div id="chart">
        <ReactApexChart options={this.state.options} series={this.state.series} type="area" height={360} />
        </div>
    )}

}
