import React from 'react';
import Card from '@mui/material/Card'
import Box from '@mui/material/Box';
import { Icon } from '@iconify/react';
import Typography from '@mui/material/Typography'
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button'
import ButtonGroup from '@mui/material/ButtonGroup';
import Container from '@mui/material/Container'
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid'
import ChangePass from '../Forms/changePassProfile.jsx'
import axios from 'axios';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { styled } from '@mui/material/styles';
import { useParams } from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import UpdateUserAdmin from '../Forms/updateUserAdmin.jsx'
import convertTime from '../utils/convertTime.js'
import Wait from "../utils/wait.jsx";
import AddSuscribe from "../Forms/addSuscriptionsToUser.jsx";
import AddSections from "../Forms/addSectionsToUser.jsx";
import AddPayment from "../Forms/addPayMethodToUser.jsx";
import { useDispatch, useSelector } from "react-redux";
import userAction from '../../redux/actions/getUserData.js'
import notifyAction from '../../redux/actions/notify.js'
import userActionById from '../../redux/actions/getUserDataById.js'



const Suscriptions = (props)=>{
    const {id} = useParams()
    const dispatch = useDispatch();
    const user   = useSelector(state=>state.userbyid);
    
    const delSuscribe = async (value)=>{
         
        var fetch = await axios.delete("/user/suscriptions",{
         params:{
           local:value.local._id,
           user:id,
           section:value.section._id,
           product:value.product._id, 
           id:value._id
         }
        })

        if (fetch.data.status != "ok") {
          dispatch(notifyAction.active(true, "error", fetch.data.message))
          return
        }
          dispatch(notifyAction.active(true, "success", fetch.data.message))
          dispatch(userActionById(id))
      }
    
       
    return(
        <div>
      
       
     
              {
                  user.subscriptions.map((item, key)=>{
  
                      return(
                        <div key={Math.random() * 10000}>
                      <Card key = {Math.random() * 10000} sx={{mb:2}}>
                       <Box key = {Math.random() * 10000} sx={{display:"flex"}}>
                        <Box key = {Math.random() * 10000} display={"flex"} flexDirection={"column"} justifyContent={"center"} sx={{mr:2,ml:1, pt:-2}}>
                           <Icon icon="bi:cash-coin" color="black" width="26" height="26" />
                        </Box>
                        <Box  key = {Math.random() * 10000} flexGrow={1} sx={{ display:"flex", justifyContent:"center"}} >
                         <Box key = {Math.random() * 10000}>
                             <Typography key = {Math.random() * 10000} style={{fontSize:16}} variant="h6" color=" greyIcon">
                               <strong>Local: </strong>{item.local.name}
                             </Typography>

                             <Typography  key = {Math.random() * 10000} style={{fontSize:16}} variant="h6" color=" greyIcon">
                              <strong> Product: </strong> {item.product.name} - <strong>${item.product.price}</strong>
                             </Typography>
                     
                             <Typography key = {Math.random() * 10000} style={{fontSize:16}} variant="h6" color=" greyIcon">
                             <strong>Time:</strong> {convertTime(item.section.start_time, item.section.end_time)}
                             </Typography>
                          </Box>
                        </Box>
                        <ButtonGroup  key = {Math.random() * 10000} variant="contained" aria-label="outlined primary button group">
                        <Button key = {Math.random() * 10000} value={item._id} onClick={()=>delSuscribe(item)} color="error"><Icon icon="clarity:close-line" color="white" width="18" height="18" /></Button>
                        </ButtonGroup>
                      </Box>
                      </Card>
                          </div>
                      )
                  })
                   
               }
          
            
   
     
    </div>

    )
}

const Payment = (props)=>{
    const dispatch = useDispatch();
    const user   = useSelector(state=>state.userbyid);

    const delPayment = async ()=>{
    
        var fetch = await axios.delete("/user/deletepayment",{
    
         params:{
          user_id:props.data.user_id,
          _id:props.data._id} 
         
        })

        if (fetch.data.status != "ok") {
          dispatch(notifyAction.active(true, "error", fetch.data.message))
          return
        }
          dispatch(notifyAction.active(true, "success", fetch.data.message))
          dispatch(userActionById(user._id))
      }
    return(
      <Card>
        <Box sx={{display:"flex"}}>
            <Box sx={{mr:2, pt:-2}}>
               <Icon icon="bi:credit-card" color="black" width="26" height="26" />
            </Box>
            <Box flexGrow={1} sx={{display:"flex", justifyContent:"center"}}>
               <Typography style={{fontSize:16}} variant="h6" color=" greyIcon">{props.data.payment}</Typography>
            </Box>
            <ButtonGroup variant="contained" aria-label="outlined primary button group">
            <Button onClick={delPayment} color="error"><Icon icon="clarity:close-line" color="white" width="18" height="18" /></Button>
            </ButtonGroup>
        </Box>
      </Card>
    )
}


 
const Singeluser = (props) => {
    const dispatch = useDispatch();
    const user   = useSelector(state=>state.userbyid);

    const { id } = useParams();
    const [open, setOpen] = React.useState(false);

    const [openSuscribe, setOpenSuscribe] = React.useState(false)
    const handleOpenSuscribe= ()=>{
        setOpenSuscribe(true)
    }
    const handleCloseSuscribe= ()=>{
        setOpenSuscribe(false)
    }
    const [openSections, setOpenSections] = React.useState(false)
    const handleOpenSections= ()=>{
       setOpenSections(true)
    }
    const handleCloseSections= ()=>{
        setOpenSections(false)
    }
    const [openPayments, setOpenPayments] = React.useState(false)
    const handleOpenPayments= ()=>{
       setOpenPayments(true)
    }
    const handleClosePayments= ()=>{
        setOpenPayments(false)
    }

  
    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleCloseForm = (value) => {
      setOpen(false);
     
    };
    const handleNotify = (data)=>{
       setNotify(data)
    }


    const [notify, setNotify] =  React.useState({
        status:false,
        title:"OK",
        msg:"No hay Mensajes",
        color:"success"
    });

    const handleClose = () => {
        setNotify({...notify,status:false})
      }

    const [data, setData] = React.useState()
    const Input = styled('input')({
        display: 'none',
      });


    const handleUpload = async(e)=>{
   
         var files = e.target.files
         var data = new FormData();
             data.append('avatar', files[0])
             data.append('id', id)
       
        var fetch = await axios.post("/user/upload",data)  

        if (fetch.data.status != "ok") {
              setNotify({
                  status:true,
                  color:"error",
                  msg:fetch.data.message
              })
              return
        } 
        setNotify({
            status:true,
            color:"success",
            msg:fetch.data.message
        })
        dispatch(userAction)
        dispatch(userActionById(id))
  
    }  
    const handleUpdate = async()=>{
    dispatch(userActionById(id))

        var fetch = await axios.get("/user/profile/",{
            params:{
                _id:id
            }
        });
      
      setData({
        products:fetch.data.users.products,
        sections:fetch.data.users.sections,
        pay_method:fetch.data.users.pay_method,
        avatarUrl:fetch.data.users.avatar,
        subscriptions:fetch.data.users.subscriptions,
        role:"Cliente",
        name:fetch.data.users.fullname,
        email:fetch.data.users.email,
        phone:fetch.data.users.phone,
        address:fetch.data.users.address,
        hour:"",
        suscriptions:""
      })
   
    }
    
    React.useEffect(async ()=>{
      handleUpdate()
       },[])

       if (!data) {
           return <Wait/>
       }
       
    return (
        <div>
             <Stack spacing={2} sx={{ width: '100%' }}>
              <Snackbar open={notify.status} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={notify.color} sx={{ width: '100%' }}>
                  {notify.msg}
                </Alert>
              </Snackbar>
            </Stack>
            <Dialog onClose={handleCloseForm} open={open}>
                <UpdateUserAdmin handleCloseForm={handleCloseForm} handleUpdate={handleUpdate}/>
            </Dialog>
            <Dialog onClose={handleCloseSuscribe} open={openSuscribe}>
               <AddSuscribe handleClose={handleCloseSuscribe}/>
             </Dialog>
             <Dialog onClose={handleClosePayments} open={openPayments}>
               <AddPayment handleClose={handleClosePayments}/>
            </Dialog>
             <Dialog onClose={handleCloseSections} open={openSections}>
              <AddSections handleClose={handleCloseSections}/>
            </Dialog>
              <Box display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
                  <Container  component="main" maxWidth="xl">
                    <Grid container  spacing={4}>
                       <Grid item  xs={12} sm={6} xl={6}>
                         <Card  elevation={15} sx={{alignItems:"center" ,p:3, borderRadius:8}}>
                           
                              <Box style={{ display:'flex', justifyContent:'center' }}>
                                  <Avatar   sx={{width:250, height:250}} alt="Remy Sharp" src={user.avatar} />
                              </Box>
                           <Box style={{ display:'flex', justifyContent:'center' }}>
                              
                               <Stack direction="row" alignItems="center" spacing={2}>
                                    <label htmlFor="contained-button-file">
                                        <Input accept="image/*" id="contained-button-file"  type="file" value={""} onChange={handleUpload} />
                                        <Button sx={{mt:3, mb:3}} variant="contained" component="span">
                                           Subir
                                        </Button>
                                    </label>
                                </Stack>
                             
                           </Box>
                               <Divider sx={{mt:2}} />
                           <Box sx={{mt:2}} style={{ display:'flex', justifyContent:'center' }}> 
                           <Typography variant="h6" color="initial">{   user.roles[0].name}</Typography>
                           </Box>
                           <Box style={{ display:'flex', justifyContent:'center' }}> 
                           <Typography variant="h4" color="initial">{user.fullname}</Typography>
                           </Box>
                           
                            <Divider sx={{mt:2, mb:2}}  />
   
                           <Box style={{ display:'flex' }}> 
                               <Box sx={{pt:0.5}}>
                               <Icon  icon="carbon:email" color="black" width="26" height="26" />&nbsp;&nbsp;
                               </Box>
                               <Typography variant="h6" color=" greyIcon">{user.email}</Typography>
                           </Box>
                           <Box style={{ display:'flex', }}> 
                               <Box sx={{pt:0.5}}>
                               <Icon icon="bi:phone-vibrate" color="black" width="26" height="26" />&nbsp;&nbsp;
                               </Box>
                               <Typography variant="h6" color=" greyIcon">{user.phone}</Typography>
                           </Box>
                           <Box style={{ display:'flex' }}> 
                               <Box sx={{pt:1}}>
                               <Icon icon="clarity:map-marker-line" color="black" width="26" height="26" />
                               </Box>
                               <Typography variant="h6" color="greyIcon">{user.address}</Typography>
                           </Box>
                           <Box style={{ display:'flex', justifyContent:'center' }}>
                               <Button  onClick={handleClickOpen}  sx={{mt:1}} variant="contained" color="primary">
                                  Editar
                               </Button>
                               </Box>
                          
                      
                  </Card>
                       </Grid>
                        <Grid item sx={{minHeight:"100%"}} xs={12} sm={6} xl={6}>
                        <Box sx={{minHeight:"50%"}}>
                        <Card  elevation={15}  sx={{alignItems:"center" ,p:3, borderRadius:8, minHeight:"100%"}}>
                          
                            <Box sx={{mb:2}} style={{ display:'flex', justifyContent:'center' }}> 
                                <Typography variant="h6" color="greyIcon">Suscripciones</Typography>
                            </Box>
                           
                                 <Suscriptions 
                                       handleNotify={handleNotify} 
                                       handleUpdate={handleUpdate}
                                  />
                                   
                           <Box style={{ display:'flex', justifyContent:'center' }}>
                                <Button  onClick={handleOpenSuscribe}  sx={{mt:2}} variant="contained" color="primary">
                                    Añadir Subscriptions
                                </Button>
                             </Box>
                              
                             
                             
                              <Divider sx={{mt:2, mb:2}}  />
                            <Box sx={{mb:2}} style={{ display:'flex', justifyContent:'center' }}> 
                                <Typography variant="h6" color="greyIcon">Metodos de Pago</Typography>
                            </Box>
                            {
                                    user.pay_method.map((value, key)=>{
                                            return(<Payment handleNotify={handleNotify} 
                                                            handleUpdate={handleUpdate} 
                                                            key={key} 
                                                            data={{_id:value._id,user_id:id,payment:"**** **** **** "+value.pay_method.last4}}
                                                    />
                                                           
                                                          
                                                  )
                                    })
                            }
                             
                              <Box style={{ display:'flex', justifyContent:'center' }}>
                                <Button  onClick={handleOpenPayments} sx={{mt:2}} variant="contained" color="primary">
                                    Añadir Metodo
                                </Button>
                              </Box>
                            </Card>
                            </Box>
                            <Box sx={{minHeight:"50%"}}>
                            <Card  elevation={15}  sx={{mt:2,mb:3, alignItems:"center" ,p:3, borderRadius:8, minHeight:"100%"}}>
                                <ChangePass/>
                           </Card> 
                           </Box>
                       </Grid>
                    </Grid>  
                   
               </Container>
               </Box>
     
        </div>
    );
}

export default Singeluser;
