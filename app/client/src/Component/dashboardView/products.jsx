import * as React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid'
import AddProduct from "../Lists/addProduct.jsx";
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import CardProduct from '../Card/products/index.jsx'
import {useParams} from 'react-router-dom'   
import axios from "axios"
import {useDispatch, useSelector} from 'react-redux'
import getLocal from '../../redux/actions/getLocalData.js' 

export default function DataGridDemo() {
  const {id} =useParams()
  const dispatch = useDispatch();
  const locals = useSelector((state) => state)
  const [success, setSuccess] = React.useState(false);
  const [notify, setNotify] = React.useState({
    status:false,
    color:"success",
    msg:""
  });

  var product= useSelector(state=>state.local)
  var allProduct = [];
      product.map((item, key)=>{
        item.products.map((item, key)=>{ 
          allProduct.push(item)
        })  
      })

  var productByLocal = []
      product.map((item, key)=>{
  
           if (item._id == id) {
              item.products.map((item, key)=>{
                productByLocal.push(item) 
              })
           }
        
   })


  if (!id) {
     var data = allProduct
  } else{
     var data = productByLocal
     console.log(productByLocal);
  }

  

  const handleNotify = (data) => {
    setNotify(data)
  };

  const handleClickSuccess = () => {
      dispatch(getLocal)
  };

  const handleClose = () => {
    setNotify({
      status:false,
      color:"success",
      msg:""
    })
  };

  if (product.length < 1) {
     return (
       <h1>Wait</h1>
     )
  }


  var viewProduct = [];

  for (let i = 0; i < data.length; i++) {
    viewProduct.push(  
    <Grid key={i} item xs={12} sm={6} md={6} xl={3}>
       <CardProduct id={data[i]._id} 
         handleSuccess={handleClickSuccess}
         handleNotify={handleNotify} name={data[i].name} 
         type={data[i].type} teacher={data[i].teacher} 
         users={!data[i].users ? 0 : data[i].users.length } 
         descriptions={data[i].descriptions} 
         price={data[i].price} 
         sesiones={data[i].sesiones}/>  
    </Grid>
     );
  }

  return (
    <div style={{ width: '100%' }}>
            <Stack spacing={2} sx={{ width: '100%' }}>
       
              <Snackbar open={notify.status} autoHideDuration={3000} onClose={handleClose}>
                  <Alert severity={notify.color}>{notify.msg}</Alert>
              </Snackbar>
          </Stack>
            
     <Box borderRadius={5}   sx={{p:2}}> 
      <Box  borderRadius={5} sx={{backgroundColor:"white"}}>

        <Box display={"flex"} justifyContent={"end"} zIndex={100}  sx={{width:"100%" }}>
          {
            !id ? "": <AddProduct handleNotify={handleNotify} handleSuccess={handleClickSuccess} />
          }
         
        </Box>  

        <Box display="flex" justifyContent="flex-center" sx={{mb:1}}>
         <Grid container spacing={2}>
           {viewProduct}
         </Grid>
        </Box>  
      </Box>
      </Box>
    </div>
  );
}