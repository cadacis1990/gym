import * as React from 'react';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import CardBoard from '../Card/customMyCard.jsx'
import Grid from '@mui/material/Grid'
import { Icon } from '@iconify/react';


import Actions  from "../Lists/actionListSuscription.jsx";
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import Led from "../Lists/led.jsx";

const columns = [
 /*  { field: 'id', headerName: 'No', width: 90 }, */
  {
    field: 'accion',
    headerName: 'Accion',
    width: 80,
    renderCell: (params) => {
      return(
        <Box   sx={{display: 'flex',  justifyContent: 'center'}}>
             <Actions data={{id:params.row.id, status:params.row.status}}/>
        </Box>

        )
    },
    editable: true,
  },
  {
    field: 'statuspay',
    headerName: 'Mensualidad',
    width: 120,
    editable: true,
    renderCell: (params) => {
            return(
                <Led status={params.row.status}/>
            )
      },
  },
  
  {
    field: 'firstName',
    headerName: 'Nombre(s)',
    width: 150,
    editable: true,
  },
  {
    field: 'lastName',
    headerName: 'Apellidos',
    width: 150,
    editable: true,
  },
  {
    field: 'category',
    headerName: 'Categoria',
    width: 100,
    editable: true,
  },
  {
    field: 'local',
    headerName: 'Local',
    width: 100,
    editable: true,
  },
  {
    field: 'email',
    headerName: 'Email',
    width: 200,
    editable: true,
  },
  {
    field: 'status',
    headerName: 'Estado',
    width: 80,
    editable: true,
  },
  {
    field: 'pay_method',
    headerName: 'Tipo de Pago',
    width: 100,
    editable: true,
  }

];

function getData(){
    ///consultar la api aqui
   ///consultar la api aqui
   var rows = [];
   for (let index = 0; index < 10; index++) {
     rows.push({id:index,firstName:" faker.name.firstName()", lastName: "faker.name.lastName()",category:"categoria", local:"faker.name.findName()", email:"faker.internet.email()", status:"faker.random.boolean()", pay_method:"CARD"})
   }
   return rows;
}


export default function DataGridDemo() {

  const [data, setData] = React.useState([]);
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
 
  const updateData = ()=>{
    setData(getData());
  }
  const handleClickError = () => {
    setError(true);
  };
  const handleClickSuccess = () => {
    setSuccess(true);
  };

  const handleClose = () => {
    setError(false);
    setSuccess(false);
  };
  
React.useEffect(()=>{
updateData();
},[])

  return (
  


    <div style={{ height: 400, width: '100%' }}>
            <Stack spacing={2} sx={{ width: '100%' }}>
              
              <Snackbar open={success} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                  This is a success message!
                </Alert>
              </Snackbar>
              <Snackbar open={error} autoHideDuration={3000} onClose={handleClose}>
                  <Alert severity="error">This is an error message!</Alert>
              </Snackbar>
          </Stack>
    
        <Box borderRadius={5}  sx={{p:2}}> 
       
    
        <Box display="flex" sx={{mb:6}}>
            <Grid container spacing={2} sx={{ pb:2, pr:1, borderRadius: 4}}>
                <Grid item xs={12} sm={6} md={4} xl={4} >   
                <CardBoard tag="Cantidad de Suscriptores" value="251" prefix="/Total" variantValues="h3" iconboxcolor="primary" iconsview={<Icon icon="mdi-light:account" />}/>  
                </Grid>
                <Grid item xs={12} sm={6} md={4} xl={4} >   
                <CardBoard tag="Ganancias" value="1546" prefix="/Mes" variantValues="h3" iconboxcolor="warning" iconsview={<Icon icon="carbon:currency-dollar" />}/>  
                </Grid>
                <Grid item xs={12} sm={6} md={4} xl={4} >   
                <CardBoard tag="Pagos Pendientes" value="10" prefix="/Mes" variantValues="h3" iconboxcolor="danger"  iconsview={<Icon icon="ic:outline-watch-later" />}/>  
                </Grid>
            </Grid>
        </Box> 
      <Box borderRadius={5} sx={{backgroundColor:"white"}}>
        <Box display="flex" justifyContent="flex-end" sx={{mb:1}}>
         
        {/*   <TextField id="outlined-search" label="Buscar" type="search"   size="small" sx={{ alignContent:"right", mr:10 }} /> */}
          {/* <AddUser handleError={handleClickError} handleSuccess={handleClickSuccess} updateData={updateData}/> */}
          
        </Box>  
        <DataGrid
                          
                          columns={columns}
                          rows={data}
                          autoHeight={true}
                          rowsPerPageOptions={[10]}
                          localeText={{
                          toolbarDensity: 'Size',
                          toolbarDensityLabel: 'Size',
                          toolbarDensityCompact: 'Small',
                          toolbarDensityStandard: 'Medium',
                          toolbarDensityComfortable: 'Large',
                          }}
                          components={{
                          Toolbar: GridToolbar,
                          }}
                      /> 
      </Box>
      </Box>
    </div>
  );
}