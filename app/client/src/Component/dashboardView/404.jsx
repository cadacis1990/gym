import React from 'react';
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button'
import Card from '@mui/material/Card';
import { useNavigate } from "react-router-dom";
import IconButton from '@mui/material/IconButton'
import {Link} from "react-router-dom";

const NotFound = () => {
    const navigate = useNavigate();  
  
    return (
        <div>
           <Card sx={{ minWidth: 275 }}>
            <Box  display="flex"  justifyContent="center" alignItems="center"  minHeight="100vh" >
               <Box>
                    <Typography variant="h2" color="initial">
                    ERROR 404
                    </Typography>
                    <Typography variant="h6" color="initial">
                    ESTA PAGINA NO SE ENCUENTRA
            </Typography>
                        <Box style={{ display:'flex', justifyContent:'center' }}>
                           
                                <Button  onClick={() => {navigate("/", { replace: true });}} sx={{mt:4}} variant="contained" color="primary">
                                   INICIO
                                </Button>
                        
                            
                        </Box>
               </Box>
            </Box>
        </Card>
        </div>
    );
}

export default NotFound;
