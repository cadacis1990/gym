import React from 'react';
import Grid from '@mui/material/Grid'
import CardBoard from '../Card/customMyCard.jsx'
import ChartLastSuscriptions from '../charts/charLastSuscriptions.jsx' 
import ClientChart from '../charts/clientGeneral.jsx'
import AttendenceWeek from '../charts/attendance.jsx'
import dashIcons from '../Icons/dashboard.jsx'
import { Icon } from '@iconify/react';
import LastUser from "../Card/lastUser.jsx";
import {useSelector} from 'react-redux'
import Wait from "../utils/wait.jsx";
/* import jwtMiddleware from "../../middleware/jwtSend.js" */



const Dashboard = () => {
    const data = useSelector(state=>state.local)
    var totales = 0
    var cash = 0
    var activos = 0
    var users = []

    data.map((value)=>{
      value.sections.map((value)=>{
          totales = totales + value.users.length  
             value.users.map((value)=>{
               users.push(value)
               if (value.client_status) {
                  activos = activos +1
               }
                value.pay_method.map((value)=>{
                   if (value.pay_method == "Cash") {
                       cash = cash + 1
                   }
                })
             })
      })
    })


    var fullname = "There are no Users"
    if (users.length > 0 ) {
       fullname = users[0].firstname + " " + users[0].lastname
       console.log(users);
    } 
 
    console.log(users.length);
    
    var sortUser = users.sort(function (a, b) {
      if (a.createdAt > b.createdAt) {
        return -1;
      }
      if (a.createdAt < b.createdAt) {
        return 1;
      }
      // a must be equal to b
      return 0;
    });

    
    if (data.length == 0) {
       return <Wait/>
    }

    const icons = dashIcons;
    return (
        <div>
     <Grid container spacing={2} sx={{backgroundColor:"white", pb:2, pr:1, borderRadius: 4}}>
        <Grid item xs={12} sm={6} md={4} xl={3} >
          <Grid container spacing={2} sx={{ pb:2, borderRadius: 4}}>
            <Grid item  xs={12} sm={12} md={12} xl={12}>
            <CardBoard tag="Suscripciones Totales " value={totales} prefix="/Total" variantValues="h3" iconboxcolor="danger" iconsview={<Icon icon="bi:credit-card-2-front" color="white" />}/>
            </Grid>
            <Grid item  xs={12} sm={12} md={12} xl={12}>
            <CardBoard tag="Usuarios al Cash" value={cash} prefix="/Total" variantValues="h3" iconboxcolor="primary" iconsview={<Icon icon="bi:cash-coin" color="white" />} />
            </Grid>
            <Grid item  xs={12} sm={12} md={12} xl={12}>
            <CardBoard tag="Clientes en Activo" value={activos} prefix="/Total" variantValues="h3" iconboxcolor="info" iconsview={<Icon icon="healthicons:gym-outline" />}/>
            </Grid>
          </Grid>
        </Grid>
            <Grid item xs={12} sm={6} md={8} xl={9} >
            <CardBoard tag="Ultima suscripcion" value={fullname } variantValues="h6" iconboxcolor="warning" iconsview={<Icon icon="mdi-light:clipboard-check" />}>
            <ChartLastSuscriptions />
            </CardBoard>
            </Grid>
            <Grid item  xs={12} sm={6} md={8} xl={9}>
            <CardBoard tag="Historial de Users" value="Ultimo Año" variantValues="h6" iconboxcolor="success" iconsview={<Icon icon="mdi-light:clipboard-text" />}>
            <ClientChart/>
            </CardBoard>
            </Grid>
            <Grid item  xs={12} sm={6} md={4} xl={3}>
            <CardBoard tag="Asistencias Semanal" value="Ultima Semana" variantValues="h6" iconboxcolor="info"  iconsview={<Icon icon="mdi-light:check-bold" />}>
            <AttendenceWeek />
            </CardBoard>
            </Grid>
           
            
         </Grid>
        </div>
    );
}


export default Dashboard;
