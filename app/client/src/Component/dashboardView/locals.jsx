import * as React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid'
import AddLocal from "../Lists/addLocal.jsx";
import CardLocals from "../Card/locals/index.jsx";
import {useDispatch, useSelector} from 'react-redux'


export default function DataGridDemo() {
  const locals = useSelector((state) => state.local)
  var countUser = 0;
  var maxUserLocal = 0;
  var viewLocals = [];  

  locals.map((value,key)=>{
      value.sections.map((value)=>{

        countUser = parseInt(countUser) + parseInt(value.users.length)
        maxUserLocal = parseInt(maxUserLocal) + parseInt(value.maxUser)
      })
      viewLocals.push(
        <Grid key={key} item xs={12} sm={6} md={4} xl={3}>
           <CardLocals   
             id={value._id} 
             countUser={countUser} 
             maxUserLocal={maxUserLocal} 
             name={value.name} 
             address={value.address} />  
         </Grid>  
     )
        countUser = 0;
        maxUserLocal = 0;
  })

  return (
  


    <div style={{ height: 400, width: '100%' }}>

          <Box borderRadius={5}   sx={{p:2}}> 
          <Box  borderRadius={5} sx={{backgroundColor:"white", p:2}}>
          <Box zIndex={100} sx={{mb:1, position:"fixed", bottom:16, right:16 }}>
             <AddLocal />
          </Box>  
          <Box display="flex" justifyContent="flex-center" sx={{mb:1}}>
         <Grid container spacing={2}>
          {viewLocals} 
         </Grid>
        </Box>  
      </Box>
      </Box>
    </div>
  );
}