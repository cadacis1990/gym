import * as React from 'react';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import CardBoard from '../Card/customMyCard.jsx'
import Grid from '@mui/material/Grid'
import { Icon } from '@iconify/react';
import  Actions  from "../Lists/actionListUser.jsx";
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';


const columns = [

 /*  { field: 'id', headerName: 'No', width: 90 }, */
  {
    field: 'accion',
    headerName: 'Accion',
    width: 80,
    renderCell: (params) => {
      return(
        <Box   sx={{display: 'flex',  justifyContent: 'center'}}>
             <Actions />
        </Box>

        )
    },
    editable: true,
  },
  {
    field: 'firstName',
    headerName: 'Nombre(s)',
    width: 150,
    editable: true,
  },
  {
    field: 'lastName',
    headerName: 'Apellidos',
    width: 150,
    editable: true,
  },
  {
    field: 'email',
    headerName: 'Email',
    width: 200,
    editable: true,
  },
  {
    field: 'local',
    headerName: 'Local',
    width: 100,
    editable: true,
  },
  {
    field: 'time',
    headerName: 'Horario',
    width: 100,
    editable: true,
  },
  {
    field: 'category',
    headerName: 'Categoria',
    width: 100,
    editable: true,
  },
  {
    field: 'status',
    headerName: 'Estado',
    width: 80,
    editable: true,
  },
  {
    field: 'pay_method',
    headerName: 'Tipo de Pago',
    width: 100,
    editable: true,
  }
  
];


export default function DataGridDemo() {

  const [data, setData] = React.useState([]);
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
 
  const handleClickError = () => {
    setError(true);
  };
  const handleClickSuccess = () => {
    setSuccess(true);
  };

  const handleClose = () => {
    setError(false);
    setSuccess(false);
  };

 
  return (
  


    <div style={{ height: 400, width: '100%' }}>
            <Stack spacing={2} sx={{ width: '100%' }}>
              
              <Snackbar open={success} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                  This is a success message!
                </Alert>
              </Snackbar>
              <Snackbar open={error} autoHideDuration={3000} onClose={handleClose}>
                  <Alert severity="error">This is an error message!</Alert>
              </Snackbar>
          </Stack>
    
        <Box borderRadius={5}  sx={{p:2}}> 
       
    
        <Box display="flex" sx={{mb:6}}>
            <Grid container spacing={2} sx={{ pb:2, pr:1, borderRadius: 4}}>
                <Grid item xs={12} sm={6} md={4} xl={4} >   
                <CardBoard tag="Cantidad de Usuarios" value="460" prefix="/Total" variantValues="h3" iconboxcolor="primary" iconsview={<Icon icon="mdi-light:account" />}/>  
                </Grid>
                <Grid item xs={12} sm={6} md={4} xl={4} >   
                <CardBoard tag="Usuarios Suscritos" value="251" prefix="/Mes" variantValues="h3" iconboxcolor="warning" iconsview={<Icon icon="mdi-light:check-bold" />}/>  
                </Grid>
                <Grid item xs={12} sm={6} md={4} xl={4} >   
                <CardBoard tag="Usuarios Canceladas" value="10" prefix="/Total" variantValues="h3" iconboxcolor="danger"  iconsview={<Icon icon="mdi-light:alert" />}/>  
                </Grid>
            </Grid>
        </Box> 
      <Box borderRadius={5} sx={{backgroundColor:"white"}}>
        <Box display="flex" justifyContent="flex-end" sx={{mb:1}}>
         

          
        </Box> 
        <DataGrid
                          
                          columns={columns}
                          rows={data}
                          autoHeight={true}
                          rowsPerPageOptions={[10]}
                          localeText={{
                          toolbarDensity: 'Size',
                          toolbarDensityLabel: 'Size',
                          toolbarDensityCompact: 'Small',
                          toolbarDensityStandard: 'Medium',
                          toolbarDensityComfortable: 'Large',
                          }}
                          components={{
                          Toolbar: GridToolbar,
                          }}
                      /> 
      {/* <DataGrid 
        checkboxSelection = {false}
        autoHeight={true}
        rows={data}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[8]}
      /> */}
      </Box>
      </Box>
    </div>
  );
}