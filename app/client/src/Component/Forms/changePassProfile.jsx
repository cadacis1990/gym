import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import validator from "validator";
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Snackbar from '@mui/material/Snackbar';
import Stack from '@mui/material/Stack';
import CircularProgress from '@mui/material/CircularProgress';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import {useDispatch, useSelector } from 'react-redux'
import notifyActions from '../../redux/actions/notify.js'
import userActionById from '../../redux/actions/getUserDataById.js'
import axios from 'axios'



export default function SignIn(props) {

    const user     = useSelector(state=>state.userbyid)
    const dispatch = useDispatch();
    const [rpass, setRpass] = React.useState({
        error:false,
        label:"Repita el Password",
        color:"",
        value:""
    });
    const [notify, setNotify] = React.useState({
        type:"success",
        msg:"false",
        status:false
    });
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
        label:"Password",
        color:"",
        error:false

      });

      const handleChange = (prop) => (event) => {

             
        if (!validator.isStrongPassword(event.target.value) && event.target.value != "") {
            setValues({ ...values, [prop]: event.target.value, error:true, label:"Min 8 caracteres, mayusculas, minusculas, simbolos", color:"#d32f2f" });
            return
        }
        if (rpass.value != event.target.value && event.target.value != "") {
            setValues({ ...values, [prop]: event.target.value, error:true, label:"Password no Coinciden", color:"#d32f2f" });
            return
        }

            setValues({ ...values, [prop]: event.target.value, error:false, label:"Password", color:""  });
            setRpass({ ...values, error:false, label:"Repetir Password", color:""  });
        }
      

        const updateRpass = (event)=>{
                if (values.password != event.target.value) {
                    setRpass({value:event.target.value, error:true, label:"Password no Coinciden", color:"#d32f2f"})
                    return
                }
                setValues({ ...values, error:false, label:"Password", color:""  });
                setRpass({value:event.target.value, error:false, label:"Repetir Password", color:""}) 
        }
      const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
      };
    
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };
  
    const handleClose = () => {
        setNotify({...notify, status:false});
    }

    const handleSubmit = async () => {
          console.log("haciendo submit");
          if (values.password != rpass.value || !validator.isStrongPassword(values.password)) {
            return
          }  

          var fetch = await axios.put("/user",{
              id:user._id,
              password:values.password
           })
         if (fetch.data.status != "ok") {
          dispatch(notifyActions.active(true, "error", fetch.data.message))
          return
         }  
         //Si la respuesta del server es positiva
         dispatch(notifyActions.active(true, "success",  fetch.data.message))
         dispatch(userActionById(user._id))
         setValues({...values, password:""})
         setRpass({...rpass, value:""})
         return
        };

  return (
   
        <Box
          sx={{
            marginTop: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Stack sx={{ width: '100%' }} spacing={2}>
            <Snackbar open={notify.status} autoHideDuration={6000} onClose={handleClose}>
                <Alert  severity={notify.type}>
               
                  <AlertTitle>{notify.error}</AlertTitle>
                 
             
                     {notify.msg}
                     <CircularProgress color="success" width={2} />
                  </Alert>
            </Snackbar>
          </Stack>
      
     
       
          <Box sx={{ mt: 1 }}>
         
          <InputLabel style={{color:values.color}} htmlFor="outlined-adornment-password">{values.label}</InputLabel>
          <OutlinedInput
            fullWidth 
            id="outlined-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label={values.label}
            error={values.error}
          />
            <TextField
              error={rpass.error}
              onChange={updateRpass}
              margin="normal"
              required
              fullWidth
              value={rpass.value}
              name="password"
              label={rpass.label}
              type="password"
              id="passwordtwo"
            />
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 1, mb: 2 }}
            >
             Cambiar Password
            </Button>
           
          </Box>
        </Box>
    



  
  );
}
