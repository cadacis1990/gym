import React from 'react';
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import {PaymentElement, CardElement} from '@stripe/react-stripe-js';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';


const stripePromise = loadStripe('pk_test_51IDcSKBg5CrIbIRKdXcfwFvXP5mUpfpWZVq7iVyWmcK9SfHFcjYhndyUr1KWp3dxAvZNbV7DbBl5OjgoHmN2F1jX00yOggHmjo');

const CheckoutForm = () => {
  return (
    <form>
       <TextField
          fullWidth
          required
          id="outlined-required"
          label="Email"
          defaultValue="joe@joe.ex"
        />
        
    </form>
  );
};


export default function(){
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm/>
    </Elements>
  );
};