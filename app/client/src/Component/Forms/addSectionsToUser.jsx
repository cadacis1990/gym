import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { createTheme } from '@mui/material/styles';
import Card from '@mui/material/Card';

import axios from "axios"
import "date-fns"


const theme = createTheme();

export default function SignUp(props) {
    
  return (
    <Card>
   
        <Box  sx={{ m:3,  display: 'flex', flexDirection: 'column',   alignItems: 'center',  }}   >          
         
                <TextField
                       
                            required
                            fullWidth
                            id="max"
                            label="Maximo de Usuarios"
                            name="max"
                       
                          />
            <Button
        
              fullWidth
              variant="contained"
              sx={{ mt: 3}}
            >
              Agregar
            </Button>
   
        </Box>
      
  
  
 
    </Card>
  )
}