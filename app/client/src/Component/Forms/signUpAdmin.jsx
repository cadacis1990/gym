import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Card from '@mui/material/Card';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import axios from "axios"
import { makeStyles } from "@material-ui/core/styles";
import createTime from '../utils/convertTime.js'
import { useDispatch,useSelector } from "react-redux";
import updateLocal from "../../redux/actions/getLocalData.js";
import updateLocalById from "../../redux/actions/getLocalById.js";
import notify from "../../redux/actions/notify.js";
import {useParams} from 'react-router-dom'

export default function SignUp(props) {
  const {id} = useParams()
  const dispatch = useDispatch()
  var   product = useSelector((state)=>state.local)
  var   [roles, setRoles] = React.useState([])

  product = product.filter((item, key)=>{
    return item._id == id
  })
  var productData  = product[0].products
  var sectionsData = product[0].sections

  var createRender = [];
  for (let index = 0; index <  sectionsData.length; index++) {
       var _id =   sectionsData[index]._id;
       createRender.push( {value:_id, label:createTime(sectionsData[index].start_time, sectionsData[index].end_time)})
  }


  const handleUpdate = ()=>{
    dispatch(updateLocal)
    dispatch(updateLocalById(props.localId))
  }
  const [data, setData] =React.useState({
     user_id:id,
     role:"",
     firstname:"",
     lastname:"",
     email:"",
     pay_method:"",
     product:"",
     day:1,
     getSections:[],
     section:"",
     password:"",
     local_id:props.localId
  })

  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handleFirstName = (e) =>{
     setData({...data,firstname:e.target.value})
  }
  const handleLastName = (e) =>{
    setData({...data,lastname:e.target.value})
  }
  const handleEmail = (e) =>{
    setData({...data,email:e.target.value})
  }
  const handlePay = (e) =>{
    setData({...data,pay_method:e.target.value})
  }
  const handleProduct = (e) =>{
    setData({...data,product:e.target.value})
  }
 /*  const handleDay = async (e) =>{
    var fetch = await axios.get("/sections/",{
      params:{
          id:props.localId,
          day:1
      }
  })
  
    setData({...data, getSections:createRender, day:e.target.value})
  } */
  
  const handleSection = (e) =>{
    setData({...data,section:e.target.value})
  }
  const handleRoles = (e) =>{
    setData({...data,role:e.target.value})
  }
  const handlePassword = (e) =>{
    setData({...data,password:e.target.value})
  }
  const handleSubmit = async () => {
    console.log(data);
    var fetch = await axios.post("/user/userbyadmin",data);
    if (fetch.data.status != "ok") {
       dispatch(notify.active(true, "error", fetch.data.message))
        return
    }
       dispatch(notify.active(true, "success", fetch.data.message))
       handleUpdate()
       props.doClose()
  };
  const loadRoles = async()=>{
    var fetch = await axios.get("/roles")
    if (fetch.data.status != "ok") {
     dispatch(notify.active(true, "error", "Error to get roles"))
     return
    }
    setRoles(fetch.data.roles)

  }

  React.useEffect(()=>{
      loadRoles()
  },[])
  
  if (roles.length < 1) {
    return <h1>wait</h1>
  }
  return (
    <Card >
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            borderRadius:15,
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Box borderRadius={15} noValidate sx={{ mt: 1 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  value={data.firstname}
                  onChange={handleFirstName}
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  value={data.lastname}
                  onChange={handleLastName}
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={data.email}
                  onChange={handleEmail}
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
              <FormControl fullWidth >
              <InputLabel id="demo-simple-select-disabled-label">Pago</InputLabel>
              <Select
                  labelId="demo-simple-select-disabled-label"
                  id="demo-simple-select-disabled"
                  value={data.pay_method}
                  label="Cash"
                  onChange={handlePay}
                  fullWidth
                >
                 
                  <MenuItem value={"cash"}>Cash</MenuItem>
               
              </Select>
              <FormHelperText>Disabled</FormHelperText>
            </FormControl>
              </Grid>
              <Grid item xs={12}>
              <FormControl fullWidth >
              <InputLabel id="demo-simple-select-disabled-label">Paquetes</InputLabel>
              <Select
                  labelId="demo-simple-select-disabled-label"
                  id="demo-simple-select-disabled"
                  value={data.product}
                  label="Product"
                  onChange={handleProduct}
                  fullWidth
              >
                 
                 {productData.map((option, key) => (
                      <MenuItem value={option._id} key={key}>
                       {option.name} ---- <strong>Precio:{option.price}</strong>
                      </MenuItem>
                    ))}
               
              </Select>
              <FormHelperText>Disabled</FormHelperText>
            </FormControl>
              </Grid>
              <Grid item xs={12}>
              <FormControl fullWidth >
              <InputLabel id="demo-simple-select-disabled-label">Roles</InputLabel>
              <Select
                  labelId="demo-simple-select-disabled-label"
                  id="roles"
                  value={data.role}
                  label="Roles"
                  onChange={handleRoles}
                  fullWidth
              >
                 
                 {roles.map((option, key) => (
                

                   option.name != "client" ?
                      <MenuItem value={option._id} key={key}>
                       {option.name}
                      </MenuItem> : ""
                    ))}
               
              </Select>
              <FormHelperText>Disabled</FormHelperText>
            </FormControl>
              </Grid>
           
              <Grid item xs={12}>
              <form autoComplete="off">
              <FormControl fullWidth >
              <InputLabel id="demo-simple-select-disabled-label">Horario</InputLabel>
                  <Select
                    value={data.section}
                    name="country"
                    onChange={handleSection}
                    inputProps={{
                      id: "open-select"
                    }}
                  >
                    {createRender.map((option, key) => (
                      <MenuItem value={option.value} key={key}>
                       {option.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </form>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={data.password}
                  onChange={handlePassword}
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                />
              </Grid>
            </Grid>
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
                  Registrar
            </Button>
       
          </Box>
        </Box>
      
      </Container>
    </Card>
  );
}