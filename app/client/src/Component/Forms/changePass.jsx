import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import validator from "validator";
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Snackbar from '@mui/material/Snackbar';
import Stack from '@mui/material/Stack';
import CircularProgress from '@mui/material/CircularProgress';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { useNavigate } from "react-router-dom";


export default function SignIn(props) {
    var navigate = useNavigate();
    const [rpass, setRpass] = React.useState({
        error:false,
        label:"Repita el Password",
        color:"",
        value:""
    });
    const [notify, setNotify] = React.useState({
        type:"success",
        msg:"false",
        status:false
    });
    const [values, setValues] = React.useState({
        amount: '',
        password: '',
        weight: '',
        weightRange: '',
        showPassword: false,
        label:"Password",
        color:"",
        error:false

      });

      const handleChange = (prop) => (event) => {

             
        if (!validator.isStrongPassword(event.target.value) && event.target.value != "") {
            setValues({ ...values, [prop]: event.target.value, error:true, label:"Min 8 caracteres, mayusculas, minusculas, simbolos", color:"#d32f2f" });
            return
        }
        if (rpass.value != event.target.value && event.target.value != "") {
            setValues({ ...values, [prop]: event.target.value, error:true, label:"Password no Coinciden", color:"#d32f2f" });
            return
        }

            setValues({ ...values, [prop]: event.target.value, error:false, label:"Password", color:""  });
            setRpass({ ...values, error:false, label:"Repetir Password", color:""  });
            console.log(event.target.value)
        }
      

        const updateRpass = (event)=>{
                if (values.password != event.target.value) {
                    setRpass({value:event.target.value, error:true, label:"Password no Coinciden", color:"#d32f2f"})
                    return
                }
                setValues({ ...values, error:false, label:"Password", color:""  });
                setRpass({value:event.target.value, error:false, label:"Repetir Password", color:""})
                console.log(event.target.value); 
        }
      const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
      };
    
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };
  
    const handleClose = () => {
        setNotify({...notify, status:false});
    }

    const handleSubmit = () => {
        
         if (values.password != rpass.value || !validator.isStrongPassword(values.password)) {
          return
         }  
         //consultar la api a partir de aqui
         //Si la respuesta del server es negativa
         setNotify({type:"error", title:"Error", msg:"Respuesta de el server", status:true})

         //Si la respuesta del server es positiva
         setNotify({title:"ok", type:"success", msg:"Su password ha sido cambiado ahora puede acceder estamos redireccionando a la pagina de acceso", status:true})
         setTimeout(() => {
            navigate("/recovery", { replace: true });
         }, 5000);
        
         return
        };

  return (
   
        <Box
          sx={{
            marginTop: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Stack sx={{ width: '100%' }} spacing={2}>
            <Snackbar open={notify.status} autoHideDuration={6000} onClose={handleClose}>
                <Alert  severity={notify.type}>
               
                  <AlertTitle>{notify.error}</AlertTitle>
                 
             
                     {notify.msg}
                     <CircularProgress color="success" width={2} />
                  </Alert>
            </Snackbar>
          </Stack>
      
          <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Cambiar Password
          </Typography>
          <Box sx={{ mt: 1 }}>
         
          <InputLabel style={{color:values.color}} htmlFor="outlined-adornment-password">{values.label}</InputLabel>
          <OutlinedInput
            fullWidth 
            id="outlined-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label={values.label}
            error={values.error}
          />
            <TextField
              error={rpass.error}
              onChange={updateRpass}
              margin="normal"
              required
              fullWidth
              name="password"
              label={rpass.label}
              type="password"
              id="passwordtwo"
            />
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 1, mb: 2 }}
            >
             Cambiar Password
            </Button>
           
          </Box>
        </Box>
    



  
  );
}


