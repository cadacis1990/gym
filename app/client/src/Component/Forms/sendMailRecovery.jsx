import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import {Link} from 'react-router-dom';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Snackbar from '@mui/material/Snackbar';
import { Icon } from '@iconify/react';
import validator from "validator";
import { useNavigate } from "react-router-dom";

export default function SignIn(props) {
  var navigate = useNavigate();
  const [error, setError] = React.useState(false);
  const [notify, setNotify] = React.useState({
    type:"success",
    status:false,
    msg:"",
    title:""
  });
  const [email, setEmail] = React.useState({
      value:"",
      error:false,
      label:"Email",
      color:""
  });

  const handleEmail = (e) => {
    if (!validator.isEmail(e.target.value) && e.target.value != "") {
      setEmail({value:e.target.value, error:true, label:"Email Invalido", color:"#d32f2f"});
      return
    }  
      setEmail({value:e.target.value, error:false, label:"Email", color:""});
  }

  const handleClose = () => {
    setNotify({...notify, status:false});
  }

  //Accion del submit aqui
  const handleSubmit = () => {
      if (!validator.isEmail(email.value)) {
         return //no hacer nada si el email no es valido
      }
      //consultar api aqui y esperar respuesta
     
     ///Si la respuesta de el server es negativa
     /* setNotify({status:true, msg:"Respuesta de el Servidor", type:"error", title:"Error" }) */

     ///Si la respuesta de el server es positiva hacer esto.
      setNotify({status:true, msg:"Se le ha enviado un Email a " + email.value +  " para recuperar su password", type:"success", title:"OK" })
      setEmail({...email, value:""})
      setTimeout(() => {
        navigate("/", { replace: true });
     }, 5000);

  };
  

  return (
   
      <Box display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
      <Container component="main" maxWidth="xs">
        <Card elevation={10} sx={{p:2, borderRadius:5}} >
        <CssBaseline />
        <Box
          sx={{
            marginTop: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Stack sx={{ width: '100%' }} spacing={2}>
            <Snackbar open={notify.status} autoHideDuration={6000} onClose={handleClose}>
                <Alert  severity={notify.type}>
                  <AlertTitle>{notify.title}</AlertTitle>
                  {notify.msg}
                </Alert>
            </Snackbar>
          </Stack>
      
          <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
            <Icon icon="cib:mail-ru" color="white" width="30" height="30" />
          </Avatar>
          <Typography component="h1" variant="h5">
            Recuperar Password
          </Typography>
          <Box  sx={{ mt: 1 }}>
            <TextField
              value={email.value}
              error={email.error}
              onChange={handleEmail}
              margin="normal"
              required
              fullWidth
              id="email"
              label={email.label}
              name="email"
              autoComplete="email"
              autoFocus
            />
          
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 1, mb: 1 }}
            >
              Enviar Email
            </Button>
            <Grid container>
              <Grid item xs>
              <Link style={{textDecoration:"none"}} to="/" >
                  <Typography style={{color:"grey"}}  variant="body1" > Acceder</Typography>
              </Link>
              
              </Grid>
            
            </Grid>
          </Box>
        </Box>
        </Card>
      </Container>
      </Box>
  
  );
}
