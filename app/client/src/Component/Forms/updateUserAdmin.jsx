import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import axios from 'axios';
import Card from '@mui/material/Card';
import { useParams } from "react-router-dom";
import MuiPhoneNumber from 'material-ui-phone-number';
import { useDispatch, useSelector } from "react-redux";
import notify from '../../redux/actions/notify.js'
import userAction from '../../redux/actions/getUserData.js'
import userActionById from '../../redux/actions/getUserDataById.js'

export default function SignUp(props) {
  var {id} = useParams();
  const dispatch = useDispatch();
  const user     = useSelector(state=>state.userbyid);
  const client     = useSelector(state=>state.user);
  const [open, setOpen] = React.useState(false);
  const [data, setData] =React.useState({
    
     firstname:"",
     lastname:"",
     email:"",
     phone:"",
     address:"",
     pay_method:"",
     product:"",
     day:"",
     getSections:[],
     getProducts:[],
     section:"",
     password:"",

  })
 

  const handleUpdate= async() => {
    var fetch = await axios.get("/user/profile/",{
       params:{
         _id:id
       }
    })
    var users = fetch.data.users
    setData({...data,firstname:users.firstname,lastname:users.lastname, email:users.email, address:users.address, phone:users.phone})

  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handlePhone = (event) =>{
    setData({...data,phone:event})
 }
  const handleFirstName = (e) =>{
     setData({...data,firstname:e.target.value})
  }
  const handleLastName = (e) =>{
    setData({...data,lastname:e.target.value})
  }
  const handleEmail = (e) =>{
    setData({...data,email:e.target.value})
  }
  const handleAddress = (e) =>{
    setData({...data,address:e.target.value})
  }

  const handleSubmit = async () => {
  
      var fetch = await axios.put("/user",{
          id: id || client._id,
          email:data.email,
          address:data.address,
          phone:data.phone,
          firstname:data.firstname,
          lastname:data.lastname,
          
      })
     
      if (fetch.data.status != "ok") {
         dispatch(notify.active(true,"error", fetch.data.message))
      } 
      dispatch(notify.active(true,"success", fetch.data.message))
      dispatch(userActionById(id))
      dispatch(userAction)
      props.handleCloseForm()
  };


  return (
    <Card >
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            borderRadius:15,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            mt:3
          }}
        >
          <Box borderRadius={15} noValidate >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  value={data.fullname}
                  onChange={handleFirstName}
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  value={data.lastname}
                  onChange={handleLastName}
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={data.email}
                  onChange={handleEmail}
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={data.address}
                  onChange={handleAddress}
                  required
                  fullWidth
                  id="address"
                  label="Direccion"
                  name="address"           
                />
              </Grid>
              <Grid item xs={12}>
                 <MuiPhoneNumber fullWidth value={data.phone} defaultCountry={'us'} onChange={handlePhone}/>
              </Grid>

            </Grid>
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
                  Actualizar
            </Button>
       
          </Box>
        </Box>
      
      </Container>
    </Card>
  );
}