import * as React from 'react';
import {Link} from 'react-router-dom';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';

import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Snackbar from '@mui/material/Snackbar';
import validator from "validator";
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import axios from 'axios';
import Cookies from 'js-cookie'
import {useDispatch, useSelector} from 'react-redux'
import actionUser from "../../redux/actions/getUserData.js";

export default function SignIn(props) {
  const dispatch = useDispatch()

  const [notify, setNotify] = React.useState({
    status:false,
    title:"",
    type:"error",
    msg:""
  })
  const [error, setError] = React.useState(false);
  const [email, setEmail] = React.useState({
     value:'',
     error:false,
     label:"Email",
     color:""
  });

  const [values, setValues] = React.useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
    error:false
  });

  //validando email field onchange
  const handleEmail = (e) => {
    if (!validator.isEmail(e.target.value) && e.target.value != "") {
        setEmail({value:e.target.value ,error:true, label:"Email Invalido", color:"#d32f2f"})
       
        return
    }
      setEmail({value:e.target.value ,error:false, label:"Email", color:""})
  }

  const handleChange = (prop) => (event) => {
      setValues({ ...values, [prop]: event.target.value, error:true });
  };

  const handleClose = () => {
    setNotify({...notify, status:false});
  }

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };


  //Accion para submit
  const handleSubmit = async () => {
      var emails = email.value;
      var password = values.password;
      if (!validator.isEmail(emails) ) {
           return //no hacer nada si los campos no cumplen los requisitos
      } 
        
        var data = await axios.post("/user/login", {
           email:emails,
           password:password
        });
        if (data.data.status == "ok" && data.data.jwt) {
          Cookies.set('jwt', data.data.jwt)
          Cookies.set("userId",data.data.userId)
          dispatch(actionUser);
          return
        }

        setNotify({...notify, status:true, title:data.data.status, msg:data.data.message})

    //Si la respuesta del server es negativa
    /* setNotify({status:true, title:"Error", type:"error", msg:"Mensaje del server"})*/

    // Si la respuesta de el server es positiva  
  }

  return (
   
      <Box display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
      <Container component="main" maxWidth="xs">
        <Card elevation={10} sx={{p:2, borderRadius:5}} >
        <CssBaseline />
        <Box
          sx={{
            marginTop: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Stack sx={{ width: '100%' }} spacing={2}>
            <Snackbar open={notify.status} autoHideDuration={6000} onClose={handleClose}>
                <Alert  severity={notify.type}>
                  <AlertTitle>{notify.title}</AlertTitle>
                  {notify.msg}
                </Alert>
            </Snackbar>
          </Stack>
      
          <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Acceder
          </Typography>
          <Box noValidate sx={{ mt: 1 }}>
          <InputLabel style={{color:email.color}} htmlFor="outlined-adornment-password">{email.label}</InputLabel>
          <OutlinedInput
            fullWidth 
            id="email"
            value={email.value}
            onChange={handleEmail}
            label={email.label}
            error={email.error}           
          />
           
          <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
          <OutlinedInput
            fullWidth 

            id="password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="Password"
          
          />
          
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Acceder
            </Button>
            <Grid container>
              <Grid item xs>
                <Link color="playBtnHrs" style={{textDecoration:"none", textDecorationColor:"none"}} to="/recovery" >
                  <Typography style={{color:"grey"}} variant="body1" >Recuperar Password</Typography>
                </Link>
              </Grid>
            
            </Grid>
          </Box>
        </Box>
        </Card>
      </Container>
      </Box>
  
  );
}
