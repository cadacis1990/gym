import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { createTheme } from '@mui/material/styles';
import Card from '@mui/material/Card';
import axios from "axios"
import "date-fns"
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { useSelector, useDispatch } from 'react-redux';
import converTime from '../utils/convertTime.js'
import {useParams} from 'react-router-dom'
import getLocal from '../../redux/actions/getLocalData.js' 
import notify from '../../redux/actions/notify.js'
import userAction from '../../redux/actions/getUserData.js' 
import userActionById from '../../redux/actions/getUserDataById.js'
const theme = createTheme();

export default function SignUp(props) {
  const {id} = useParams();
  const dispatch = useDispatch()
  const locals = useSelector(state=>state.local)
  const client = useSelector(state=>state.user)
      
  const [local, setLocal] = React.useState('');
  const [product, setProduct] = React.useState('');
  const [products, setProducts] = React.useState([])
  const [section, setSection] = React.useState('');
  const [sections, setSections] = React.useState([]);



  const handleChangeLocal = async (event) => {

    locals.map((item, key)=>{
       if (item._id == event.target.value ) {
          setProducts(item.products)
          setSections(item.sections)
          return
       }
    })
     setLocal(event.target.value);
 };

 const handleChangeProduct = (event) => {
  setProduct(event.target.value);
};
const handleChangeSection = (event) => {
  setSection(event.target.value);
};

const handleSubmit = async (event) => {
  var fetch = await axios.put("/user/addproduct",{
    id:id || client._id ,
    local,
    product,
    section
  })
  if (fetch.data.status != "ok") {
      dispatch(notify.active(true, "error", fetch.data.message))
      return
  }
  dispatch(userAction)
  dispatch(userActionById(id))
  dispatch(notify.active(true, "success", fetch.data.message))
  props.handleClose()

};


  return (
    <Card>
     <Box sx={{p:3,  minWidth: 300 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Local</InputLabel>
        <Select
          labelId="local"
          id="local"
          value={local}
          label="Local"
          onChange={handleChangeLocal}
        >
          {
          locals.map((item, key)=>{
             return(
              <MenuItem key={item._id} value={item._id}>{item.name}</MenuItem>
             )
          })
          }
      
        </Select>
      </FormControl>
      <FormControl sx = {{mt:1}} fullWidth>
        <InputLabel id="demo-simple-select-label">Sections</InputLabel>
        <Select
          labelId="sections"
          id="sections"
          value={section}
          label="Sections"
          onChange={handleChangeSection}
        >
          {
          sections.map((item, key)=>{
             return(
              <MenuItem key={item._id} value={item._id}>{converTime(item.start_time, item.end_time)}</MenuItem>
             )
          })
          }
      
        </Select>
      </FormControl>
      <FormControl sx={{mt:1}} fullWidth>
        <InputLabel id="demo-simple-select-label">Products</InputLabel>
        <Select
          labelId="product"
          id="product"
          value={product}
          label="Product"
          onChange={handleChangeProduct}
        >
          {
        
          products.map((item, key)=>{
              return(
                <MenuItem key={item._id} value={item._id}>{item.name} --- ${item.price}</MenuItem>
               )
          })
          }
      
        </Select>
      </FormControl>
      <Button
           onClick={handleSubmit}
           fullWidth
           variant="contained"
           sx={{ mt: 2 }}
         >
           Agregar
         </Button>
    </Box>
      
           

    </Card>
  )
}