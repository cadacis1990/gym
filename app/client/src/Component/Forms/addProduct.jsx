import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Card from '@mui/material/Card';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import axios from "axios"
import { useDispatch, useSelector } from "react-redux";
import notify from '../../redux/actions/notify.js'
import { useParams } from "react-router-dom";
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';


const theme = createTheme();

export default function SignUp(props) {
  const [editor, setEditor] = React.useState( () => EditorState.createEmpty())
  const dispatch = useDispatch()
  const {id} = useParams()
  const [data, setData] = React.useState({
    local_id:id,
    name:"",
    descriptions:"",
    type:"",
    price:""
  });

  const handleName = (e) => {
     setData({...data, name:e.target.value})
  }

  const handleDescriptions = (e) => {
    setData({...data, descriptions:e.target.value})
  }

  const handleType = (e) => {
    setData({...data, type:e.target.value})
  }

  const handlePrice = (e) => {
    setData({...data, price:e.target.value})
  }

  const handleChangeEditor = (e)=>{
    console.log(draftToHtml(convertToRaw(e.getCurrentContent())));
    setEditor(e)
   
  }

  const handleSubmit = async () => {
    
    // consultar la api aqui
    var fetch = await axios.post("/products",{...data, descriptions:draftToHtml(convertToRaw(editor.getCurrentContent()))})
    if (fetch.data.status != "ok") {
      dispatch(notify.active(true, "error", fetch.status.message))
       return
    }

    // handleClickError();
    props.handleSuccess();
    props.doClose();
  };
  
 

  return (
    <Card>
    <ThemeProvider theme={theme}>
      
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 2,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >   
        
          <Box  noValidate  sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  onChange={handleName}
                  value={data.name}
                  autoComplete="given-name"
                  name="name"
                  required
                  fullWidth
                  id="name"
                  label="Nombre del Producto"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <Box
                sx={{border:"1px solid #cdcdcd", borderRadius:1}}>
              <Editor
                editorState={editor}
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor"
                onEditorStateChange={handleChangeEditor}
              />
              </Box>
                {/* <TextField
                   multiline
                  onChange={handleDescriptions}
                  value={data.descriptions}
                  autoComplete="given-name"
                  name="descriptions"
                  fullWidth
                  id="name"
                  label="Descripcion"
                  autoFocus
                /> */}
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  onChange={handlePrice}
                  value={data.price}
                  required
                  fullWidth
                  id="price"
                  label="Precio"
                  name="price"
             
                />
              </Grid>
              <Grid item xs={12}>
                  <InputLabel id="demo-simple-select-label">Categoria</InputLabel>
                    <Select
                          onChange={handleType}
                          value={data.type}
                          fullWidth
                          label="Category"
                          
                          >
                          <MenuItem value={"Semanal"}>Semanal</MenuItem>
                          <MenuItem value={"Mensual"}>Mensual</MenuItem>
                    </Select>
              </Grid>
             
            </Grid>
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb:3 }}
            >
              Crear Paquete
            </Button>
           
        
          </Box>
         
        </Box>
      
      </Container>
  
    </ThemeProvider>
    </Card>
  );
}