import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import notifyAction from "../../redux/actions/notify.js";
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

const Notify = () => {
const notify = useSelector(state=>state.notify);  
const dispatch = useDispatch();  

const off = (event, reason) =>{
    dispatch(notifyAction.off(false, "success", ""))
}

    return (
        <div>
           <Stack spacing={2} sx={{ width: '100%' }}>
              <Snackbar open={notify.status} autoHideDuration={6000} onClose={off}>
                <Alert onClose={off} severity={notify.color} sx={{ width: '100%' }}>
                  {notify.message}
                </Alert>
              </Snackbar>
             </Stack>
        </div>
    );
}

export default Notify;
