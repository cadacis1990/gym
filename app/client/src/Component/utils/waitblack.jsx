import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography'

export default function CircularIndeterminate() {
  return (
    <Box sx={{backgroundColor:"#000000", p:2,borderRadius:2, border:"1px solid #ddbc48"}}>
      <CircularProgress sx={{color:"#ddbc48"}} />
    </Box>
     
  );
}