const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');



const config = {
  devtool: "source-map",  
  entry:{
     app:path.resolve(__dirname, 'app/client/public/index.js'),

  }, 
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].bundled.js',
    publicPath: '/'
  },
  module: {
    rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use:{
            loader:'babel-loader',
            options:{
              presets:["@babel/preset-env", "@babel/preset-react"]
            }
          } 

        },
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader' ]
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: 'file-loader'
        },
        {
          test: /\.svg$/,
          exclude: /node_modules/,
          loader: 'svg-react-loader'
        }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: './app/client/public/index.html',
      favicon: './app/client/public/favicon.ico'
    })
    

  ],
  
  devServer: {
    //publicPath: "/",
    //contentBase: "/app",
    hot: true,
    //overlay: true,
    port: 4000,
    //inline: true,
    //progress:true,
    open: 'http://localhost:4000'
  },
}

module.exports = config;
