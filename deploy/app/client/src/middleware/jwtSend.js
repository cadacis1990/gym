import axios from 'axios';
import { Service } from 'axios-middleware';
import Cookies from 'js-cookie'


const service = new Service(axios);


service.register({
  onRequest(config) {
     config.headers.jwt = Cookies.get('jwt'); 
     /* config.url = "/" + config.url; */
     config.url = "http://localhost:8000/api" + config.url;
     return config; 
  },
 /*  onSync(promise) {
    console.log('onSync');
    return promise;
  },
  onResponse(response) {
    console.log('onResponse' + reponse);
    return response;
  } */
});

export default service