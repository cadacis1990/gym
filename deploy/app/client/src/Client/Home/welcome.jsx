import React from 'react';
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button'
import {useNavigate} from 'react-router-dom'
import {useSelector} from 'react-redux'
const Header = () => {
    const user = useSelector(state=>state.user)
    const Price = ()=>{
        return(
            <div>
            <Typography variant="h6" style={{fontWeight:300, fontSize:15}} color="#ddbc48">PRICE</Typography>
            <Box display={"flex"}>
            <Typography variant="h3" sx={{mt:-0.5}} style={{fontWeight:400, fontSize:30}}  color="#ffffff">{user.products[0].price}</Typography>
            <Typography variant="h3" sx={{mt:-0.5}} style={{fontWeight:400, fontSize:15}}  color="#ffffff">/{user.products[0].type}</Typography>
            </Box>
            </div>
        )

    }
    return (
        <div>
            {/*escritorio*/}
            <Box sx={{display:{xs:"none", sm:"block"}}}>
                <Typography variant="h6" style={{fontWeight:300, fontSize:20}} color="#ddbc48">WELCOME</Typography>
                <Typography variant="h3" sx={{mt:-0.5}} style={{fontWeight:400, fontSize:30}}  color="#ffffff">{user.firstname + " " + user.lastname}</Typography>
                <br/>

                <Typography variant="h6" style={{fontWeight:300, fontSize:15}} color="#ddbc48">SUBSCRIPTIONS</Typography>
                <Typography variant="h3" sx={{mt:-0.5}} style={{fontWeight:400, fontSize:20}}  color="#ffffff">{user.products.length > 0 ? user.products[0].name : "Visit our Subscriptions"}</Typography>
                <br/>
                {user.products.length > 0 ? <Price/> : ""}
                
            </Box>
             {/*Movil*/}
          
        </div>
    );
}
/* const Descriptions = () => {
    return (
        <div>
            <Typography variant="body1"  color="#ffffff">
               Lorem ipsum dolor. Nunc eget mi ac massa mus \cet viverra molestie. Donec molestie egestas magna. Etiam purus justo, sodales mattis arcu sed, gravida cursus augue.
            </Typography>
        </div>
    );
} */
const Unete = () => {
    const navigate = useNavigate()
    return (
        <div>
            <Box sx={{ml:{ sm:13}, p:2, width:{xs:"100%", sm:350}, borderRadius:3,  borderStyle:"solid", border:2, borderColor:"#ddbc48", backgroundColor:"#000000B5"}}>
            <Box>
            <Header/>
            </Box>
    
            </Box>
    
            <Button onClick={()=>navigate("/myaccount")} sx={{ml:{ sm:13}, mt:3, fontWeight:100}} variant="contained" color="doradoBtn" >
                <Typography variant="body1" color="black">MY ACCOUNT</Typography>

                </Button>
  
              
            
        </div>
    );
}

export default Unete;
