import React from 'react';
import {Link} from 'react-router-dom';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Icon } from '@iconify/react';

const Menu = (props) => {
    
    const itemc = ( texto, link, icon ) =>{
    
         return( 
            
            <Link to={link} style={{ textDecoration: 'none', color:"white"}}>    
             <Box  >     

                    
                    <ListItem alignItems='center'
                            
                              button 
                              onClick={props.doClose}
                              sx={{
                                pt:0.5,  
                                width:1,
                                '&:hover': {
                                color:"#ffffff",    
                                backgroundColor: '#ddbc48',
                               
                                },
                            }}
                    >
                        <Box   textAlign={"center"}   
                                sx={{
                                        width:1,
                                    }}
                        >
                      
                           {icon}
                       
                          <Typography variant="h6"  sx={{fontWeight:300, fontSize:14}} >{texto}</Typography>
                          </Box>
                       
                    </ListItem>    
                    
             </Box> 
            </Link>
       )
  
    }

    return (
        <div>
        <Toolbar />
        <Typography variant="h7">
        <Divider />
        </Typography>
            <Box sx={{mt:-6}}>
            <List sx={{pt:0}} >
                {itemc("Inicio","/", <Icon icon="mdi-light:home" width="24" height="24"/>)}
                {itemc("Registro","/signup", <Icon icon="ph:user-circle-plus" color="white" width="24" height="24"/>)}
                {itemc("Programas","/programs",<Icon icon="bi:book" color="white" width="24" height="24" />)}
                {itemc("Servicios","/services",<Icon icon="healthicons:gym-outline" color="white" width="26" height="26" />)}
                {itemc("Calendario","/calendar",<Icon icon="uiw:date" color="white" width="24" height="24" />)}
                {itemc("Nosotros","/about",<Icon icon="emojione-monotone:loudspeaker" color="white" width="24" height="24" />)}
                {itemc("Entrenadores","/teacher",<Icon icon="ph:chalkboard-teacher-light" color="white" width="24" height="24" />)}
                {itemc("Contactenos","/contact", <Icon icon="la:phone-volume" color="white" width="24" height="24" />)}
                {itemc("Login","/login", <Icon icon="carbon:login" color="white" width="26" height="26" />)}
            </List>
            </Box>
      </div>
    );
}

export default Menu;


