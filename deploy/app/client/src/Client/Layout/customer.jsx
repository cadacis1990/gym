import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import { makeStyles } from '@material-ui/core';
import Grid from '@mui/material/Grid'
import Menu from './menu/index.jsx'
import Typography from '@mui/material/Typography'
import Image from 'material-ui-image'


const drawerWidth = 130;
const useStyles = makeStyles({
       
        root: {
               backgroundColor:"black", 
               boxShadow:"0px 0px 0px 0px",
               color:"white",
              },
        drawer: {
          background: 'blue',
          color: 'white'
        }      


       });



function ResponsiveDrawer(props) {
  const classes = useStyles();
  const { window } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container = window !== undefined ? () => window().document.body : undefined;
  

  return (
    <div  >
    <Box  sx={{ display: 'flex', backgroundColor: "black", minHeight:"100vh"}}>
    <CssBaseline /> 
      <AppBar
          position="fixed"
      
          style={{backgroundColor:"black", boxShadow:"0px 0px 0px 0px", marginLeft:'auto'}} 
      >
        <Toolbar   style={{ boxShadow:"0px 0px 0px 0px"}} >
        <Box sx={{width:60, heigth:60 }}>     
            <Image
                src="/logo.jpg"
                />
        
        </Box>

        <Grid container  justifyContent="center" spacing={0}>
              <Grid item xs={12}>
               <Box sx={{width:"100%", textAlign:"end"}}> 
                <IconButton
                  color="menuBtnMovil"
                  aria-label="open drawer"
                  edge="start"
            
                  onClick={handleDrawerToggle}
                  sx={{ mr: 0, display: { sm: 'none' } }}
                >
                <MenuIcon />
              </IconButton>
              </Box>
              </Grid>
        </Grid>
          

        </Toolbar>
      </AppBar>
      <Box     
        component="nav"
        sx={{ flexShrink: { sm: 0 }}}
        aria-label="mailbox folders" 
      
      >
         
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          anchor="right"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          
          sx={{
            
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
            <Box height="100%"  style={{backgroundColor:"black"}}>
               <Menu updateRole={props.updateRole} doClose={handleDrawerToggle}/>
            </Box>

     
        </Drawer>
        <Drawer
          variant="permanent"
          anchor="right"
          classes={{root:classes.drawer}}
          sx={{
            marginRight:0,
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          open
        >  
        <Box textAlign={"center"} height="100%" style={{backgroundColor:"black"}}>
          <Menu updateRole={props.updateRole}/>
        </Box>

        </Drawer>
      </Box>
      <Box
        component="main"
        style={{backgroundColor:"black"}}
        sx={{ m:2, width: { sm: `calc(100% - ${drawerWidth}px)` }, backgroundcolor:"black" , minHeight:"100vh"}}
      >
     
        <Toolbar />
      
        

        {props.children}
    
        </Box>
       
      </Box>
      </div>
  );
}

ResponsiveDrawer.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default ResponsiveDrawer;