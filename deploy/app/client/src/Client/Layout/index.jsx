import React from 'react';
import Customer from './customer.jsx'
import Register from './register.jsx'
import Typography from '@mui/material/Typography'
const Layout = (props) => {
    return (
        <div>
           
            <Customer>{props.children}</Customer>
            
        </div>
    );
}

export default Layout;
