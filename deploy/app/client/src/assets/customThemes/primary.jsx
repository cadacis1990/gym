import { createTheme } from '@mui/material/styles';
import { lightGreen, lightBlue, red, grey, blueGrey } from '@mui/material/colors';

var primary = createTheme({
    palette: {
      editBtn: {
        main: lightBlue[500],
      },
      editBtn: {
        main: lightBlue[500],
      },
      stopBtnHrs: {
        main: lightGreen[500],
      }, 
      playBtnHrs: {
        main: red[300],
      },
      menuBtnMovil:{
          main: grey[500]
      },
      greyIcon:{
        main: grey[500]
      },
     greyLogin:{
      main: grey[500]
     },
     blueGrey:{
      main: blueGrey[500]
     },
     doradoBtn:{
      main: "#ddbc48"
     },
    

  
      white:{
        main:"#ffffff"
    }
 
    },
    typography:{
        fontFamily: [
          'Roboto Condensed', 'sans-serif'
        ].join(','),
    }
  });

  export default primary;