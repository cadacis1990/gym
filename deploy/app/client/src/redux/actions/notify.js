
const active = (status, color, message)=>(dispatch)=>{
     dispatch({type:"notify@active", payload:{status, color, message}})
} 

const off = (status, color, message)=>{
   return ({type:"notify@off", payload:{status, color, message}})
} 


export default {active, off};
