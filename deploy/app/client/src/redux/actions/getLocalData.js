import axios from 'axios'

const get = async(dispatch, getSate)=>{
    var fetch = await axios("/locals/");
    dispatch({type:"local@get", payload:fetch.data.locals});
} 

export default get;