import axios from 'axios'

const get = async(dispatch, getSate)=>{
    var fetch = await axios.get("/products");
  
    dispatch({type:"products@get", payload:fetch.data.data});
} 

export default get;