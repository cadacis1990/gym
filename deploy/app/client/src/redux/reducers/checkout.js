const defaultState = {
        country:"",
        state:"",
        city:"",
        firstname:"",
        lastname:"",
        address_line1:"",
        address_line2:"",
        sex:"",
        email:"",
        phone:"",
        birthday:new Date(),
        password:"",
        repeatPassword:"",
        showPassword:false,
        product:{
            _id:"",
            name:"",
            descriptons:"",
            price:"",
            section_id:"",
            type:""
        }
};

function reducer(state=defaultState, action){
    switch (action.type) {
        case "checkout":
            return action.payload
            break;
    
        default:
            return state
    }
}

export default reducer;