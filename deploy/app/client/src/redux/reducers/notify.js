const defaultState = {
   status:false,
   message:"",
   color:"success"
};

function reducer(state=defaultState, action){
    switch (action.type) {
        case "notify@active":{
            return action.payload
        }
            break;
        case "notify@off":{
            return action.payload
            }
            break;
        default:
            return state
    }
}

export default reducer;