import React from "react";
import {BrowserRouter as Router, Routes,  Route} from 'react-router-dom';
import Laydash from "../Component/Layout/admin.jsx";
import { ThemeProvider } from "@emotion/react";
import Dashboard from '../Component/dashboardView/dashboard.jsx'
import Locals from '../Component/dashboardView/locals.jsx'
import Users from '../Component/dashboardView/users.jsx'
import Products from '../Component/dashboardView/products.jsx'
import Suscriptions from '../Component/dashboardView/suscriptions.jsx'
import SingleLocal from "../Component/dashboardView/singelLocal.jsx";
import SingleProduct from "../Component/dashboardView/singelProduct.jsx";
import theme from "../assets/customThemes/primary.jsx";
import Login from "../Component/Forms/loginAdmin.jsx";
import Wait from  "../Component/utils/wait.jsx"
import SingleUser from '../Component/dashboardView/singelUser.jsx'
import NotFound from "../Component/dashboardView/404.jsx";
import jwtMiddleware from "../middleware/jwtSend.js"
import { useDispatch, useSelector } from "react-redux";
import actionUser from '../redux/actions/getUserData.js'
import actionUserById from '../redux/actions/getUserDataById.js'
import Notify from '../Component/notify/notify.jsx'
import getLocals from "../redux/actions/getLocalData.js";
import getProducts from "../redux/actions/getProduct.js";
import getSections from "../redux/actions/getSectionsActive.js";


import Layout      from '../Client/Layout/index.jsx'
import {Home}      from '../Client/index.jsx'
import {About}     from '../Client/index.jsx'
import {SignUp}    from '../Client/index.jsx'
import {Programs}  from '../Client/index.jsx'
import {Services}  from '../Client/index.jsx'
import {Calendar}  from '../Client/index.jsx'
import {Teachers}  from '../Client/index.jsx'
import {Contact}   from '../Client/index.jsx'
import {Logins}     from '../Client/index.jsx'
import {ChangePass}     from '../Client/index.jsx'
import {Recovery}     from '../Client/index.jsx'
import {Logout}     from '../Client/index.jsx'
import {Account}     from '../Client/index.jsx'


var CheckRole = (props)=>{

const dispatch = useDispatch();  

const role = useSelector(state => state.user.roles)
  for (let index = 0; index < role.length; index++) {
    if (role[index].name == "administrator") {
      return( <Laydash > 
                {props.children}
              </Laydash>
          )
              }
                
    if (role[index].name == "employer") {
         return( <Laydash > 
                 {props.children}
                        </Laydash>
                )
    }   
      return(<Login />)
   
  }
   
}

const App = () => {
const role = useSelector(state => state.user.roles)
const id = useSelector(state => state.user.id)
const dispatch = useDispatch();


dispatch(getProducts)
dispatch(getSections)
dispatch(getLocals)

//Esperar porque verifique el token en el backend 
if (role.length== 0) {
  dispatch(actionUser);

  return <Wait/>
}


  return (
    <div>
     <Notify/>
     <ThemeProvider theme={theme}>          
             <Router>
                  <Routes> 
                      
                    {/*   <Route path="/"                   element={<Layout><Home/></Layout>}/> */}
                      <Route path="/login"              element={<Layout><Logins/></Layout>}/>
                      <Route path="/myaccount"          element={<Layout><Account/></Layout>}/>
                      <Route path="/logout"             element={<Logout/>}/>
                      <Route path="/signup"             element={<Layout><SignUp/></Layout>}/>
                      <Route path="/programs"           element={<Layout><Programs/></Layout>}/>
                      <Route path="/services"           element={<Layout><Services/></Layout>}/>
                      <Route path="/calendar"           element={<Layout><Calendar/></Layout>}/>
                      <Route path="/about"              element={<Layout><About/></Layout>}/>
                      <Route path="/teacher"            element={<Layout><Teachers/></Layout>}/>
                      <Route path="/contact"            element={<Layout><Contact/></Layout>}/>
                      <Route path="/recovery"           element={<Layout><Recovery/></Layout>}/>
                      <Route path="/resetpassword/:id"  element={<Layout><ChangePass/></Layout>}/>
                     
                     {/*  <Route path="/" element={<CheckRole><Dashboard/></CheckRole>}/> */}
                      <Route path="/" element={<CheckRole><Dashboard/></CheckRole>}/>
                
                      <Route path="/dashboard" element={<CheckRole><Dashboard/></CheckRole>}/>
                      <Route path="/dashboard/locals" element={<CheckRole><Locals/></CheckRole>}/>  
                      <Route path="/dashboard/locals/:id" element={<CheckRole><SingleLocal/></CheckRole>}/>  
                      <Route path="/dashboard/products" element={<CheckRole><Products/></CheckRole>}/> 
                      <Route path="/dashboard/product/:id" element={<CheckRole><SingleProduct/></CheckRole>}/> 
                      <Route path="/dashboard/users" element={<CheckRole><Users/></CheckRole>}/> 
                      <Route path="/dashboard/users/:id/edit" element={<CheckRole><SingleUser/></CheckRole>}/> 
                      <Route path="/dashboard/suscripciones" element={<CheckRole><Suscriptions/></CheckRole>}/> 
                      <Route path="/dashboard/reportes" element={<CheckRole><h1>reportes</h1></CheckRole>}/> 
                      <Route path="*" element={<NotFound/>}/> 
                  </Routes>
          </Router> 
      
      </ThemeProvider>
     
    </div>
   
  );
}
export default App;
