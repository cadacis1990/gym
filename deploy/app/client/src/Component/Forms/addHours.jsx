import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Card from '@mui/material/Card';
import MenuItem from '@mui/material/MenuItem';
import { toInteger } from 'lodash';
import axios from "axios"
import "date-fns"
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import TimePicker from '@mui/lab/TimePicker';
import { useDispatch, useSelector } from "react-redux";
import notify from '../../redux/actions/notify.js'
import updateLocalById from '../../redux/actions/getLocalById.js'
import updateLocal from '../../redux/actions/getLocalData.js'



const theme = createTheme();

export default function SignUp(props) {
  const dispatch = useDispatch()
  const [time, setTime] = React.useState({
    ihrs:0,
    fhrs:0,
    imin:0,
    fmin:0,
    max:0,
    status:true,
    local:props.localId,
    day:1
  });
  const [start, setStart] = React.useState(new Date('2020-01-01 12:00'));
  const [end, setEnd] = React.useState(new Date('2020-01-01 12:00'));
  
  const handleChangeSh = (event) => {
    setTime({...time,ihrs:event.target.value, fhrs:event.target.value});
  };
  const handleChangeSm = (event) => {
    setTime({...time,imin:event.target.value});
  };
  const handleChangeFh = (event) => {
    setTime({...time,fhrs:event.target.value});
  };

  const handleChangeFm = (event) => {
    setTime({...time,fmin:event.target.value});
  };
  const handleChangeMax = (event) => {
    setTime({...time,max:event.target.value});
  };
  
  const handleSubmit = async () => {
  
    var fetchCreateSections = await axios.post("/sections/",{
      ...time,
      start_time:start,
      end_time:end
    }) 

    if (fetchCreateSections.data.status != "ok" ) {
      dispatch(notify.active(true, "error", fetchCreateSections.status.message))
       return
    }


    dispatch(updateLocalById(props.localId))
    dispatch(updateLocal)
    props.doClose();
};
   
   var renderHrs = (sh)=>{
       var arrHrs= [];
       for (let index = sh; index < 24; index++) {
         arrHrs.push(
          <MenuItem key={index} value={index}>{index}</MenuItem>
         )
         
       }
       return arrHrs
   }

   var renderHrsIni = (eh)=>{
     if (toInteger(time.ihrs) != 0 && toInteger(time.fhrs) != 0) {
      var arrHrs= [];
      for (let index = 0; index <= eh; index++) {
        arrHrs.push(
         <MenuItem key={index} value={index}>{index}</MenuItem>
        )
        
      }
      return arrHrs
     }
    var arrHrs= [];
    for (let index = 0; index <= 24; index++) {
      arrHrs.push(
       <MenuItem key={index} value={index}>{index}</MenuItem>
      )
      
    }
    return arrHrs
}
   var renderMin = (sm)=>{
    var arrMin= [];
    if (toInteger(time.ihrs) == toInteger(time.fhrs)) {
         
          for (let index = sm; index < 59; index++) {
            arrMin.push(
            <MenuItem key={index} value={index}>{index+1}</MenuItem>
            )
          }
          return arrMin
    }
        for (let index = 0; index <= 59; index++) {
          arrMin.push(
          <MenuItem key={index} value={index}>{index}</MenuItem>
          )
        }
        return arrMin
}
var renderMinIni = (im)=>{
  var arrMin= [];
  if (toInteger(time.ihrs) == toInteger(time.fhrs) &&  toInteger(time.fmin) != 0) {
        for (let index = 0; index < im; index++) {
          arrMin.push(
          <MenuItem key={index} value={index}>{index}</MenuItem>
          )
        }
        return arrMin
  }
      for (let index = 0; index <= 59; index++) {
        arrMin.push(
        <MenuItem key={index} value={index}>{index}</MenuItem>
        )
      }
      return arrMin
}
  return (
    <Card>
    <ThemeProvider theme={theme}>
      
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 2,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >          
          <Box  noValidate sx={{ mt: 3 }}>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Grid container spacing={1}>
                           
                            <Grid item xs={6} md={6}>
                             <TimePicker
                                renderInput={(params) => <TextField {...params} />}
                                value={start}
                                label="Inicio"
                                onChange={(newValue) => {
                                    setStart(newValue);
                                    setEnd(newValue);
                                }}
                         /*        minTime={new Date(0, 0, 0, 8)}
                                maxTime={new Date(0, 0, 0, 18, 45)} */
                            />
                             </Grid>
                            <Grid item xs={6} md={6}>
                            <TimePicker
                           
                                    renderInput={(params) => <TextField {...params} />}
                                    label="Final"
                                    value={end}
                                    onChange={(newValue) => {
                                        setEnd(newValue);
                                    }}
                                    minTime={start}
                                    />
                            </Grid>
                         </Grid>
                         <TextField
                            sx={{mt:2}}
                            required
                            fullWidth
                            id="max"
                            label="Maximo de Usuarios"
                            name="max"
                            onChange={handleChangeMax}
                          />
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              sx={{ mt: 3,mb:3 }}
            >
              Agregar Horario
            </Button>
           
            </LocalizationProvider>
          </Box>
         
        </Box>
      
      </Container>
  
    </ThemeProvider>
    </Card>
  );
}