import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Card from '@mui/material/Card';
import MuiPhoneNumber from 'material-ui-phone-number';
import axios from "axios"
import notify from '../../redux/actions/notify.js'
import {useDispatch, useSelector} from 'react-redux'
import updateLocal from "../../redux/actions/getLocalData.js";
const theme = createTheme();

export default function SignUp(props) {
const dispatch = useDispatch()

  const [data, setData] = React.useState({
    name:"",
    address:"",
    phone:"",
    email:"",
    description:""
  });


  const handleChangeName = (event) => {

    setData({...data,name:event.target.value});
  };
  const handleChangeDescription = (event) => {

    setData({...data,description:event.target.value});
  };
  const handleChangeAddress = (event) => {

    setData({...data, address:event.target.value});
  };
  const handleChangePhone = (event) => {

    setData({...data, phone:event});
  };
  const handleChangeEmail = (event) => {

    setData({...data, email:event.target.value});
  };

  const handleSubmit = async () => {
    var fetch = await axios.post("/locals/",data)
    if(fetch.data.status != "ok"){
      dispatch(notify.active(true, "error", fetch.status.message))
       return
    }
    dispatch(updateLocal)
    props.handleClose();
  };
  
 

  return (
    <Card>
    <ThemeProvider theme={theme}>
      
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 2,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >   
        
          <Box  noValidate sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                 
                  name="name"
                  required
                  fullWidth
                  id="name"
                  label="Nombre"
                  onChange={handleChangeName}
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  required
                  fullWidth
                  id="address"
                  label="Direccion "
                  name="address"
                  autoComplete="family-name"
                  onChange={handleChangeAddress}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email"
                  name="email"
                  autoComplete="email"
                  onChange={handleChangeEmail}
                />

              </Grid>
              <Grid item xs={12}>
                 <MuiPhoneNumber fullWidth defaultCountry={'us'} onChange={handleChangePhone}/>

              </Grid>
              <Grid item xs={12}>
              <TextField
                  required
                  fullWidth
                  id="descriptions"
                  label="Descripcion"
                  name="description"
                  onChange={handleChangeDescription}
                />
              </Grid>
             
            </Grid>
            <Button
              fullWidth
              variant="contained"
              sx={{ mt: 3 }}
              onClick={handleSubmit}
            >
              Registrar
            </Button>
           
        
          </Box>
          <Button
              color="error"
              fullWidth
              variant="contained"
              sx={{ mt: 1, mb: 2 }}
              onClick={props.handleClose}
            >
              Cancelar
            </Button>
        </Box>
      
      </Container>
  
    </ThemeProvider>
    </Card>
  );
}