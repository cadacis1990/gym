import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Card from '@mui/material/Card';
import MenuItem from '@mui/material/MenuItem';
import { toInteger } from 'lodash';
import axios from "axios"
import "date-fns"
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import TimePicker from '@mui/lab/TimePicker';

const theme = createTheme();

export default function SignUp(props) {
     
  return (
    <Card>
    
        <Box  sx={{m:3,  display: 'flex', flexDirection: 'column',   alignItems: 'center',  }}   >          
         
                <TextField
                          
                            required
                            fullWidth
                            id="max"
                            label="Maximo de Usuarios"
                            name="max"
                          
                          />
            <Button
           
              fullWidth
              variant="contained"
              sx={{ mt: 3 }}
            >
              Agregar 
            </Button>
   
        </Box>
      
  
  
 
    </Card>
  )
}