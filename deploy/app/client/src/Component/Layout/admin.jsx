import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import { makeStyles } from '@material-ui/core';
import Grid from '@mui/material/Grid'
import Menu from '../SideBar/menu.jsx'
import dashIcons from '../Icons/dashboard.jsx'
import Notify from "../Bar/notifyAppBar.jsx";
import AvatarName from "../Bar/avatarName.jsx";
import axios from 'axios';
import Cookies from 'js-cookie'
const drawerWidth = 160;
const useStyles = makeStyles({
       
        root: {
               backgroundColor:"white", 
               boxShadow:"0px 0px 0px 0px",
               color:"white",
              },
        drawer: {
          background: 'blue',
          color: 'white'
        }      


       });



function ResponsiveDrawer(props) {
  const classes = useStyles();
  const { window } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container = window !== undefined ? () => window().document.body : undefined;
  const icons = dashIcons;

  return (
    <div>
    <Box  sx={{ display: 'flex' }}>
    <CssBaseline /> 
      <AppBar
          position="fixed"
          sx={{
            borderBottomStyle:"solid",
            borderBottomWidth:"1px",
            borderBottomColor:"#cacaca",
            width: { sm: `calc(100% - ${drawerWidth}px)` },
            ml: { sm: `${drawerWidth}px` },
            p:0
          }}
          style={{backgroundColor:"white", boxShadow:"0px 0px 0px 0px", marginLeft:'auto'}} 
      >
        <Toolbar   style={{ boxShadow:"0px 0px 0px 0px"}} >
        <Grid container spacing={0}>
              <Grid item xs={2}>
                <IconButton
                  color="menuBtnMovil"
                  aria-label="open drawer"
                  edge="start"
                  onClick={handleDrawerToggle}
                  sx={{ mr: 0, display: { sm: 'none' } }}
                >
                <MenuIcon />
              </IconButton>
              </Grid>
              </Grid>
              
                <Box  style={{ borderRadius:"100px"}} sx={{display: 'flex', pr:2}} >
              
                  <AvatarName updateRole={props.updateRole}/> 
                </Box>

        </Toolbar>
      </AppBar>
      <Box     
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 }}}
        aria-label="mailbox folders"     
      >
         
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          
          sx={{
            
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
            <Box height="100%" style={{backgroundColor:"#252f3e"}}>
               <Menu updateRole={props.updateRole} doClose={handleDrawerToggle}/>
            </Box>

     
        </Drawer>
        <Drawer
          variant="permanent"
          classes={{root:classes.drawer}}
          sx={{
          
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          open
        >  
        <Box height="100%" style={{backgroundColor:"#252f3e"}}>
          <Menu updateRole={props.updateRole}/>
        </Box>

        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{ flexGrow: 1, pt:3,pl:1, width: { sm: `calc(100% - ${drawerWidth}px)` } }}
      >
        <Toolbar />
          
        {props.children}

        </Box>
       
      </Box>
      </div>
  );
}

ResponsiveDrawer.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default ResponsiveDrawer;