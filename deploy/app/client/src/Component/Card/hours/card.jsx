import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { Icon } from '@iconify/react';
import ActionsHrs from '../../Lists/actionHours.jsx'
import notify from '../../../redux/actions/notify.js'
import localUpdate from '../../../redux/actions/getLocalData.js'
import {useSelector, useDispatch} from 'react-redux'
import axios from "axios";

const Card = (props) => {
    const dispatch = useDispatch()
    
    var iconStatus = (status) => {
     if (status) {
              return(
                <Icon icon="clarity:success-standard-line" width="20" height="20" color="green" />
              )   
           }
            return(
                <Icon icon="fluent:block-16-filled" width="20" height="20" color="red" />
            )
     }

    var iconBtnStatus = (status) => {
        if (status) {
                 
            return(
                <Icon icon="akar-icons:block" color="white" width="20" height="20"/>
            )   
        }
        return(
                <Icon icon="clarity:success-standard-line" color="white" width="20" height="20"  />
        )
     }

     var stopBtnHrs = () => {
         return "stopBtnHrs"
     }
     var playBtnHrs = () => {
        return "playBtnHrs"
    }
    const handleDelete = async ()=>{
        var fetch = await axios.delete("/sections/",{
           data:{
            _id:props.id,
            local_id:props.localId
           }
        });
        
        if (fetch.data.status!="ok") {
            dispatch(notify.active(true, "error", fetch.data.message))
            return
         }
            dispatch(notify.active(true, "success", fetch.data.message))
            dispatch(localUpdate)

    }

    const handleStatus = async () => {
         var fetch = await axios.put("/sections/updstatus",{
             _id:props.id
         }) 
        
         if (fetch.data.status!="ok") {
            dispatch(notify.active(true, "error", fetch.data.message))
            return
         }
         dispatch(notify.active(true, "success", fetch.data.message))
         dispatch(localUpdate)
    }

    return (
        <div>
            <Box style={{ borderStyle:"solid", borderWidth:1, borderColor:"#c1c1c1", borderRadius:15 }} sx={{display:"flex",p:1, m:2}}>
                    <Box sx={{mt:1}}>
                        {iconStatus(props.status)}
                    </Box>
                    <Box sx={{  mt:1, ml:1, mr:2, pr:2}}  style={{borderStyle:"solid", borderRightWidth:1, borderTopWidth:0, borderLeftWidth:0, borderBottomWidth:0, borderColor:"#c1c1c1"}} >
                        {props.hour}
                    </Box>
                    <Box sx={{ flexGrow: 1/2, mt:1, ml:1}} style={{borderStyle:"solid", borderRightWidth:1, borderTopWidth:0, borderLeftWidth:0, borderBottomWidth:0, borderColor:"#c1c1c1"}} >
                        Clientes:{props.totalUser}
                    </Box>
                    <Box sx={{ flexGrow: 1/2, mt:1, ml:1}}  >
                        Max:{props.maxUser}
                    </Box>
                    <Box sx={{display:{xs:"none", md:"block"}}} >
                        <ButtonGroup color="white"  variant="contained" aria-label="outlined secondary button group">
                         
                            <Button onClick={handleStatus} color={props.status == true ? playBtnHrs() :stopBtnHrs() }>{iconBtnStatus(props.status)}</Button>
                            <Button  onClick={handleDelete} color="error"><Icon icon="clarity:close-line" width="20" height="20" color="white" /></Button>
                        </ButtonGroup>
                    </Box>
                    <Box sx={{display:{xs:"block", md:"none"}}}  >
                            <ActionsHrs totalUser={props.totalUser} handleStatus={handleStatus} handleDelete={handleDelete}  status={props.status}/>
                    </Box>
             </Box>
          
        </div>
    );
}


export default Card;
