import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import CardBoard from './customMyCard.jsx'
import Grid from '@mui/material/Grid'
import { Icon } from '@iconify/react';
import { faker } from "faker";
import  Actions  from "../Lists/actionList.jsx";
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';


const columns = [
  { field: 'id', headerName: 'No', width: 90 },
  {
    field: 'firstName',
    headerName: 'Nombre(s)',
    width: 150,
    editable: true,
  },
  {
    field: 'lastName',
    headerName: 'Apellidos',
    width: 150,
    editable: true,
  },
  {
    field: 'category',
    headerName: 'Categoria',
    width: 100,
    editable: true,
  },
  {
    field: 'local',
    headerName: 'Local',
    width: 100,
    editable: true,
  },
  {
    field: 'email',
    headerName: 'Email',
    width: 200,
    editable: true,
  },
  {
    field: 'status',
    headerName: 'Estado',
    width: 80,
    editable: true,
  },
  {
    field: 'pay_method',
    headerName: 'Tipo de Pago',
    width: 100,
    editable: true,
  },
  {
    field: 'accion',
    headerName: 'Accion',
    width: 180,
    renderCell: (params) => {
      return(

        <Box   sx={{ml:10, display: 'flex',  justifyContent: 'flex-end'}}>
             <Actions/>
        </Box>

        )
    },
    editable: true,
  }
];

function getData(){
    ///consultar la api aqui
    const rows = [
      { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35, accion:"23435dfdf" },
      { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42, accion:"23435dfdf" },
      { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45, accion:"23435dfdf" },

    ];
    return rows;
}

export default function DataGridDemo() {

  const [data, setData] = React.useState(getData());
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
 
  const updateData = ()=>{
    setData(getData());
  }
  const handleClickError = () => {
    setError(true);
  };
  const handleClickSuccess = () => {
    setSuccess(true);
  };

  const handleClose = () => {
    setError(false);
    setSuccess(false);
  };
  return (
  


    <div style={{ height: 400, width: '100%' }}>
            <Stack spacing={2} sx={{ width: '100%' }}>
              
              <Snackbar open={success} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                  This is a success message!
                </Alert>
              </Snackbar>
              <Snackbar open={error} autoHideDuration={3000} onClose={handleClose}>
                  <Alert severity="error">This is an error message!</Alert>
              </Snackbar>
          </Stack>
    
        <Box borderRadius={5}  sx={{p:2, backgroundColor:"#f2f2ff"}}> 
       
        <Box borderRadius={5} sx={{backgroundColor:"white", p:2}}>
      <DataGrid 
        hideFooterPagination = {false}
        hideFooter = {true}
        checkboxSelection = {false}
        autoHeight={true}
        rows={data}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[4]}
      />
      </Box>
      </Box>
    </div>
  );
}