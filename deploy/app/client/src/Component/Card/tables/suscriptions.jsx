import React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import  Actions  from "../../Lists/actionProduct.jsx";
import AvatarTable from "./avatarTable.jsx";
import {useDispatch, useSelector} from 'react-redux'
import {useParams} from 'react-router-dom'
const SuscriptionUsers = (props) => {
            const {id} = useParams();
            const product = useSelector((state)=>state.product)
              var filter = product.filter((value)=>{
                  if (value._id == id) {
                     return value
                  }
              })
                  filter = filter[0]
  
            var dataNew = [];
            filter.users.map((value, key)=>{
                    
                    if (!value.pay_status) {
                        var pay_status = "Pendiente"
                    }else{
                        var pay_status = "Pagado"
                    }
                    const weekDay = (value)=>{
                        var day = "";
                        switch (value) {
                            case 1:
                              day = "Lunes"  
                                break;
                            case 2:
                              day = "Martes"   
                                break;  
                            case 3:
                              day = "Miercoles"    
                                break;
                            case 4:
                                day = "Jueves"   
                                break;
                            case 5:
                                day = "Viernes"         
                                break;  
                            case 6:
                                day = "Sabado"         
                                break;
                            case 7:
                                day = "Domingo"  
                                break;                        
                            default:
                                break;
                        }
                        return day
                    }
                    var pay_string = []
                    value.pay_method.map((value)=>{
                        if (!pay_string.includes(value.pay_method)) {
                            pay_string.push("**** **** **** "+value.pay_method.last4)
                        }
                    })
                    
                    dataNew.push({  id:value._id, 
                                    suscriptions_id:filter._id,
                                    fullname:value.fullname, 
                                    avatar:value.avatar,
                                    email:value.email, 
                                    time:"no",
                                    paquete:"no",
                                    pay_status:pay_status,
                                    pay_method:pay_string,
                                    status:value.client_status,
                                    day:"no",
                                    createdAt:value.createdAt 
                                    }
                                    )
                
             })
           
             //ordenar
           
             var data = dataNew.sort(function (a, b) {
                if (a.createdAt > b.createdAt) {
                  return -1;
                }
                if (a.createdAt < b.createdAt) {
                  return 1;
                }
                // a must be equal to b
                return 0;
              });
              
           
      
          
         const columns = [
         {
           field: 'accion',
           headerName: 'Accion',
           width: 100,
           renderCell: (params) => {
             return(

               <Box   sx={{display: 'flex',  justifyContent: 'center'}}>
                   <Actions 
                            handleNotify={props.handleNotify} 
                            handleUpdate={props.handleUpdate}
                            data={{id:params.row.id, suscriptions_id:params.row.suscriptions_id, status:params.row.status, pay_status:params.row.pay_status}}
                
                  />
               </Box>
            
               )
           }

         },
         {
            field: 'present',
            headerName: '',
            width: 30,
            editable: true,
            renderCell: (params) => {
             
                    return(
                        <AvatarTable avatar={params.row.avatar} status={params.row.status} name={params.row.firstname} />
                    )
              },
          },
         {
           field: 'fullname',
           headerName: 'Nombre Completo',
           width: 200,
       
         },
         {
           field: 'email',
           headerName: 'Email',
           width: 300,
         
         },
  
        {
            field: 'pay_method',
            headerName: 'Metodo de Pago',
            width:150,
            
        },
        {
          field: 'pay_status',
          headerName: 'Pago',
          width: 100,
        },
       
         
       ];


    return (
        <div>
            
                    <DataGrid
                            pagination
                            columns={columns}
                            rows={data}
                            autoHeight={true}
                            rowsPerPageOptions={[5, 10, 20, 100]}
                            localeText={{
                            toolbarDensity: 'Size',
                            toolbarDensityLabel: 'Size',
                            toolbarDensityCompact: 'Small',
                            toolbarDensityStandard: 'Medium',
                            toolbarDensityComfortable: 'Large',
                            }}
                            components={{
                            Toolbar: GridToolbar,
                            }}
                        />
        </div>
          );
}


export default  SuscriptionUsers;
