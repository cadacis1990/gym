import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ButtonGroup from '@mui/material/ButtonGroup';
import {Link} from 'react-router-dom';
import axios from "axios"
import { useDispatch, useSelector } from "react-redux";
import notify from '../../../redux/actions/notify.js'
import updateLocalById from '../../../redux/actions/getLocalById.js'
import updateLocal from '../../../redux/actions/getLocalData.js'

export default function OutlinedCard(props) {
  var   localData  = useSelector(state=>state.local)
  var   dispatch = useDispatch()
  var fetchDelete = async ()=>{
   var fetch = await axios.delete("/locals/" + props.id,{
    })
  if (fetch.data.status != "ok") {

     dispatch(notify.active(true, "error",   fetch.data.message))
     return
  }
      dispatch(updateLocal)
  }
 
  return (
    <Box sx={{ minWidth: 275,boxShadow: 2 }}>
      <Card  variant="outlined" >
            <React.Fragment>
            <CardContent>
            <Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
                Nombre del Local
            </Typography>
            <Typography variant="h4" component="div">
                {props.name}
            </Typography>
            <Typography sx={{fontSize:16, mb: 1.5 }} color="text.secondary">
                {props.address}
            </Typography>
            <Typography sx={{ fontSize: 16 }} variant="body2">
                Clientes: {props.countUser}
                <br />
                Capacidad Total: {props.maxUserLocal}
                <br />
                Profesores: {props.teacher}
                <br />
                Abierto: {props.abierto}
            </Typography>
            </CardContent>
            <CardActions>
                <ButtonGroup variant="contained" aria-label="outlined primary button group" disableElevation>
                <Link style={{textDecoration:"none"}} to={"/dashboard/locals/" + props.id}><Button color="primary">Ver Local</Button></Link>
                <Button  sx={{ml:2}} color="error" onClick={fetchDelete}>Eliminar</Button>
                </ButtonGroup>
            </CardActions>
        </React.Fragment>
       


      </Card>
    </Box>
  );
}