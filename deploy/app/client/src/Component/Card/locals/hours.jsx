import React from 'react';
import Grid from '@mui/material/Grid'
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import axios from 'axios'
import "date-fns"
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import TimePicker from '@mui/lab/TimePicker';

const LocalOpen = (props) => {
    const [start, setStart] = React.useState(new Date('2020-01-01 12:00'));
    const [end, setEnd] = React.useState(new Date('2020-01-01 12:00'));
    const [query, setQuery] = React.useState([]);
    const [data, setData] = React.useState({
        day:"",
        statusEnd:true,
        statusStart:true,
        actions:false,
        textBtn:"Editar"
    })

React.useState(()=>{
  setQuery(props.getPropsHours)
},[])

const handleDay = (e)=>{
    
   for (let index = 0; index < query.length; index++) {
       if (query[index].day == e.target.value) {
           setStart(new Date(query[index].start))
           setEnd(new Date(query[index].end))
           setData({...data, day:e.target.value})
           return
       }
   } 
}    

const handleSubmit = async (e)=>{
   if (!data.actions) {
       setData({...data,statusEnd:false, statusStart:false, actions:true, textBtn:"Aceptar"})
       return
   }
     var fetch = await axios.put("/locals/updatehour",{
         day:data.day,
         _id:props.localId,
         start:start,
         end:end
     })

     if (fetch.data.status != "ok") {
         props.handleNotify({
             status:true,
             color:"error",
             msg:fetch.data.message
         })
         return
     }
     
     setQuery(fetch.data.hours)
     setData({...data,start:query.start, end:end.start ,statusEnd:true, statusStart:true, actions:false, textBtn:"Editar"})
     props.handleNotify({
        status:true,
        color:"success",
        msg:fetch.data.message
    })

}   

    return (
        <div>
          

    <LocalizationProvider dateAdapter={AdapterDateFns}>
             <Grid container spacing={1}>
                            <Grid item xs={4} md={4}>
                            <FormControl fullWidth >
                            <InputLabel id="demo-simple-select-disabled-label">Dia</InputLabel>
                            <Select
                                
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                fullWidth
                                label="Age"
                                value={data.day} 
                                onChange={handleDay}
                             >
                                <MenuItem value={1}>Lunes</MenuItem>
                                <MenuItem value={2}>Martes</MenuItem>
                                <MenuItem value={3}>Miercoles</MenuItem>
                                <MenuItem value={4}>Jueves</MenuItem>
                                <MenuItem value={5}>Viernes</MenuItem>
                                <MenuItem value={6}>Sabado</MenuItem>
                                <MenuItem value={7}>Domingo</MenuItem>
                            </Select>
                            </FormControl>
                            </Grid>
                            <Grid item xs={4} md={4}>
                             <TimePicker
                                disabled={data.statusStart}
                                renderInput={(params) => <TextField {...params} />}
                                value={start}
                                label="Inicio"
                                onChange={(newValue) => {
                                    setStart(newValue);
                                    setEnd(newValue);
                                }}
                         /*        minTime={new Date(0, 0, 0, 8)}
                                maxTime={new Date(0, 0, 0, 18, 45)} */
                            />
                             </Grid>
                            <Grid item xs={4} md={4}>
                            <TimePicker
                                    disabled={data.statusEnd}
                                    renderInput={(params) => <TextField {...params} />}
                                    label="Final"
                                    value={end}
                                    onChange={(newValue) => {
                                        setEnd(newValue);
                                    }}
                                    minTime={start}
                                    />
                            </Grid>
                         </Grid>
                 </LocalizationProvider>
             <Box sx={{display: 'flex', flexDirection: 'column',  alignItems: 'center', mt:3}}>
                <Button
                      variant={!data.actions ? "outlined":"contained"}
                      onClick={handleSubmit}
                >
                    {data.textBtn}
                </Button>
            </Box>
        </div>
    );
}

export default LocalOpen;
