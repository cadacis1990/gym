import * as React from 'react';
import { Link, useNavigate } from "react-router-dom";
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import ListItemIcon from '@mui/material/ListItemIcon';
import Logout from '@mui/icons-material/Logout';
import Cookies from 'js-cookie'
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux'

import actionUser from "../../redux/actions/getUserData.js";
import actionUserById from "../../redux/actions/getUserDataById.js";
import noti from "../../redux/actions/notify.js";

export default function PositionedMenu(props) {
  const user = useSelector(state => state.user);
  const id   = useSelector(state => state.user._id);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
   
  if (!user._id) {
     dispatch(actionUserById(id))
     return (<div></div>)
  }


  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const breakUser = ()=>{
       Cookies.remove('jwt')
       Cookies.remove("userId")
       dispatch(actionUser)
    // navigate("/login", { replace: true });
    // props.updateRole(1);
  }


  return (
    <div>
    <Box sx={{display:"flex"}}>
      <Button
        aria-controls="demo-positioned-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        sx={{"&.MuiButtonBase-root:hover": {bgcolor: "transparent"} }}
        disableRipple
      >
              <Box sx={{display:"flex"}}>
                <Box  style={{width:250}}> 
                <Typography align="right" variant="body1" color="gray" sx={{ fontSize:12}}>Bienvenido</Typography>
                <Typography align="right" variant="body1" color="initial" sx={{mt:-0.5, fontSize:15}}>{user.fullname}</Typography>
                </Box>
              </Box> 
      </Button>     
      <Button
        aria-controls="demo-positioned-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        sx={{"&.MuiButtonBase-root:hover": {bgcolor: "transparent"}}}
        disableRipple
      > 
                <Avatar 
                color = "secondary"
                alt="CCarlos Cis"
                src={user.avatar}
                sx={{ width: 35, height: 35 }}
                />
        </Button>        
    </Box>
     

      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: 'visible',
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            '&:before': {
              content: '""',
              display: 'block',
              position: 'absolute',
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: 'background.paper',
              transform: 'translateY(-50%) rotate(45deg)',
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >

        
          <MenuItem onClick={()=> {navigate("/dashboard/users/"+user._id+"/edit/", { replace: true }); handleClose()}}>
             Mi Cuenta
          </MenuItem>
       
        <MenuItem  onClick={breakUser} >
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>

      </Menu>
    </div>
  );
}