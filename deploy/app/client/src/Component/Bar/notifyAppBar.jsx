import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Fade from '@mui/material/Fade';
import { Badge} from '@mui/material';
import { Icon } from '@iconify/react';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';


/* 
const menu = ( altavatar, img, title, descriptions)=>{
    return (
 
      );
} */

export default function FadeMenu() {
  const [data, setData] = React.useState([])  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  var items = [];

  const handleClick = (event) => {
    if (data.length>0) {
        setAnchorEl(event.currentTarget);
    }  
  };

  const handleClose = () => {
    setAnchorEl(null);
    /// Update la DB en notificaciones y marcar como leido todas las notificaciones que pertenecen a este user 
    setData([])
  };

  const fetchApi = async () =>{
     //Consultar en la base de datos todas las notificaciones no leidas para este user 
     setData([4, 3, 3])
  }

  React.useEffect(()=>{
    fetchApi();
  }, [])
  
  for (let i = 0; i < data.length; i++) {
      items.push(
        <ListItem key={i} alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt={"altavatar"} src={null} />
          </ListItemAvatar>
          <ListItemText
            primary={"title"}
            secondary={
              <React.Fragment>
               
                {"descriptions"}
              </React.Fragment>
            }
          />
        </ListItem>
      )
      
  } 
  return (
    <div>
      <Button
        id="fade-button"
        aria-controls="fade-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
      <Box sx={{pt:2,mr:4}} >
          <Badge   badgeContent={data.length} color="error" > 
             <Icon  icon="mdi-light:message-processing" width="26" height="26" />
          </Badge> 
          </Box >
      </Button>

      <Menu
        id="fade-menu"
        MenuListProps={{
          'aria-labelledby': 'fade-button',
        }}
        elevation={1}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
     <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        {items}
      <Divider variant="inset" component="li" />
     </List>
    </Menu>
    </div>
  );
}