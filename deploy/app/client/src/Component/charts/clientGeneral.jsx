import React, { PureComponent } from 'react';
import ReactApexChart from 'react-apexcharts'

export default class ApexChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    
      series: [{
        colors:"red",
        name:"Registrados",
        data: [44, 55, 41, 64, 22, 43, 21]
      }, {
        name:"Suscritos",
        data: [53, 32, 33, 52, 13, 44, 32]
      }, {
        name:"Cancelados",
        data: [53, 32, 33, 52, 13, 44, 32]
      }],
      
      options: {
        chart: {
          type: 'bar',
          height: 430
        },
        plotOptions: {
          bar: {
            horizontal: false,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio"],
        },
      },
    
    
    };
  }



  render() {
    return (
      

<div id="chart">
<ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={360} />
</div>


    );
  }
}
