import React from 'react';
import { Icon } from '@iconify/react';
import Box from '@mui/material/Box';
const Led = (props) => {
    if (props.status) {
        return (
            <div>
          <Box sx={{display: 'flex',  justifyContent: 'center'}}>
              <Icon icon="akar-icons:circle-fill" color="green" width="15" height="15"  inline={true} />
              </Box>
            </div>
        );
    }
  
    return (
        <div>
             <Box sx={{display: 'flex',  justifyContent: 'center'}}>
              <Icon al icon="akar-icons:circle-fill" color="red" width="15" height="15"  inline={true} />
              </Box>
              
        </div>
    );
}

export default Led;
