import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { Icon } from '@iconify/react';

export default function PositionedMenu(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDelete = ()=>{
    props.handleDelete()
    setAnchorEl(null);
  }
  const handleStatus = ()=>{
    props.handleStatus() 
    setAnchorEl(null);
  }


  const status = ()=>{
      if (props.status) {
          
          return "Bloquear"
      }
          return "Habilitar"
  }

  return (
    <div>
      <Button
        style={{borderTopRightRadius:10, borderBottomRightRadius:10, borderTopLeftRadius:0, borderBottomLeftRadius:0 }}
        variant="contained"
        id="demo-positioned-button"
        aria-controls="demo-positioned-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        <Icon icon="bi:pencil-square" color="white" width="22" height="22" />
      </Button>
      
      <Menu
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
   
        <MenuItem onClick={handleStatus}>{status()}</MenuItem>
        <MenuItem color="error" onClick={handleDelete}>Eliminar</MenuItem>
      </Menu>
    </div>
  );
}