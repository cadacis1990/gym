
import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import EditIcon from '@mui/icons-material/Edit';
import Divider from '@mui/material/Divider';
import ArchiveIcon from '@mui/icons-material/Archive';
import Box from '@mui/material/Box';
import { Icon } from '@iconify/react';
import { useNavigate } from "react-router-dom";

const StyledMenu = styled((props) => (

  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0',
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity,
        ),
      },
    },
  },
}));




export default function CustomizedMenus(props) {
  var navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const delUser = async()=>{
    var fetch = await axios.delete("/user",{
      params:{
           _id:props.data.id
      }
   })
   if (fetch.data.status != "ok") {
     props.handleNotify({
         status:true,
         color:"error",
         msg:fetch.data.message
     })
     handleClose();
     return
   }
     props.handleNotify({
       status:true,
       color:"success",
       msg:fetch.data.message
     })
   handleClose();
   props.handleUpdate();
  }
  const goEdit = () => {
      navigate("/dashboard/users/id/edit", { replace: true });
  };



  return (
    <div>
      <Box > 
      <Button variant="text">
      <Icon icon="akar-icons:more-vertical-fill"
            width="24" 
            height="24" 
            onClick={handleClick}            
      />
      </Button>
      </Box>

      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >  
        <Box
      sx={{
        display: 'flex',
        '& > *': {
          m: 1,
        },
      }}
    >
      
    </Box>
        <MenuItem  onClick={goEdit} disableRipple>
          <EditIcon />
          Editar
        </MenuItem>
        <Divider sx={{ my: 0.5 }} />
        <MenuItem onClick={delUser} disableRipple>
        <ArchiveIcon />
           Eliminar
        </MenuItem>
      </StyledMenu>
    </div>
  );
}