import React from 'react';
import Box from '@mui/material/Box';
import { Icon } from '@iconify/react';
import Typography from '@mui/material/Typography'
import Grid from '@mui/material/Grid'
import CustomMyCard from "../Card/customMyCard.jsx";
import { useParams } from "react-router-dom";
import UsersSuscriptors from '../Card/tables/suscriptions.jsx'
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import axios from 'axios'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Dialog from '@mui/material/Dialog';
import Wait from '../utils/wait.jsx'
import {useDispatch, useSelector} from 'react-redux'
import productUpdate from '../../redux/actions/getProduct.js' 
import notify from '../../redux/actions/notify.js'

const Singelproduct = (props) => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const product = useSelector((state) => state.product)
    const [dataForm, setDataForm] =   React.useState({})
    const [alert, setAlert] = React.useState(false)
    const [edit, setEdit] = React.useState({
      tipeBtn:"outlined",
      status:true,
      name:"",
      price:"",
      type:"Mensual",
      action:1,
      nameBtn:"Editar"
  })
  if (product.length<1) {
    return(
        <Wait/>
    )
}
        const handleAlert = () => {
            setAlert(true);
        };
  const handleCloseAlert = (value) => {
     setAlert(false);
     };
     const handleChangeName = async (e) => {
        setEdit({...edit, name:e.target.value})
     }
     const handleChangePrice = async (e) => {
        setEdit({...edit, price:e.target.value})
    }
    const handleChangeType = async (e) => {
        setEdit({...edit, type:e.target.value})
    }
    const handleChangeAction = async (e) => {
        setEdit({...edit, action:e.target.value})
    }

      var filter = product.filter((value)=>{
          if (value._id == id) {
              return value
          }
      })

     var fetch = filter[0]

     var data = {}
     data.total = fetch.users.length, 
     data.active = 0;
     data.pending = 0;
     data.action=1;
     data.price=fetch.price
     data.name=fetch.name
     data.type=fetch.type

      for (let y = 0; y < fetch.users.length; y++) {
            if (fetch.users[y].client_status) {
                data.active = data.active + 1
            }
            if (!fetch.users[y].pay_status) {
                data.pending = data.pending + 1 
         }
      }
     

  
  const doUpdate = async()=>{
    var fetch = await axios.put("/products/byid",{
        id:id,
        name:edit.name,
        price:edit.price,
        type:edit.type,
        action:edit.action
    })
    if (fetch.data.status != "ok") {
         dispatch(notify.active(true,"error",fetch.data.message))
        return
    }
  
    dispatch(notify.active(true,"success",fetch.data.message))
    dispatch(productUpdate)
    handleCloseAlert()
    setEdit({...edit, status:true, nameBtn:"Editar"})
  }

  const handleSubmit= ()=>{
    if (edit.status) {
        setEdit({...edit, status:false, nameBtn:"Aceptar"})
        return
    }
    if (edit.action == 2) {
        handleAlert()
        return
    }
    doUpdate()
    
}
 


  return (
    <div>
        
          <Dialog onClose={handleCloseAlert} open={alert}>
                    <Typography textAlign={"center"} variant="h6"  sx={{ m:2, mb:4}}  color="initial">
                        Esta modificacion es posible que afecte el pago de los usuarios suscritos actualmente.
                     Desea Continuar?
                    </Typography>
                    <Stack style={{justifyContent: 'center'}} sx={{mb:3}} fullWidth alignContent={"center"} spacing={2} direction="row">
                                <Button variant="contained" onClick={doUpdate}>
                                     Aceptar
                                </Button>
                                <Button  color="error"  variant="contained"   onClick={handleCloseAlert}>
                                     Cancelar
                                </Button>
                     </Stack>
          </Dialog>
        <Box sx={{p:1}}>
                <Grid container spacing={4}>
                <Grid item  xs={12} md={6} xl={6}>
                    <CustomMyCard tag="Nombre del Paquete" value={fetch.name} variantValues="h6" iconboxcolor="info" iconsview={<Icon icon="clarity:house-line" color="white" width="26" height="26" />}>
                     <Box sx={{p:2,minHeight:"320px"}} >
                     <Box sx={{display:"flex"}}>
                            <Typography variant="h6"  sx={{fontSize:16, mt:1.5}}  color="initial">
                                Precio del Paquete /     
                            </Typography>
                            <Typography variant="h4" color="initial">
                                   ${data.price}
                            </Typography>
                         </Box>  
                         <Box sx={{display:"flex"}}>

                            <Typography  variant="h6" sx={{fontSize:16, mt:1.5}} color="initial">
                                Total de Clientes /    
                            </Typography>
                            <Typography variant="h4" color="initial">
                                {data.total}
                            </Typography>
                         </Box>  
                        
                         <Box sx={{display:"flex"}}>
                            <Typography variant="h6"  sx={{fontSize:16, mt:1.5}}  color="initial">
                                Pendientes de Pago /     
                            </Typography>
                            <Typography variant="h4" color="initial">
                                   {data.pending}
                            </Typography>
                         </Box>  
                       
                        
                      </Box>
                    </CustomMyCard> 
                
                </Grid>
                <Grid item  xs={12} md={6} xl={6}>
                    <CustomMyCard tag="Importante" value="Seccion para Editar" variantValues="h6" iconboxcolor="warning" iconsview={<Icon icon="uit:stopwatch" color="white" width="30" height="30" />}>
                     <Box sx={{p:2, minHeight:158}}>
                     <Grid container spacing={2}>
                        <Grid item xs={12} sm={12}>
                            <TextField
                                disabled={edit.status}
                                value={edit.name}
                                onChange={handleChangeName}
                                name="name"
                                required
                                fullWidth
                                id="firstName"
                                label="Nombre"
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={6} sm={6}>
                        <TextField
                           onChange={handleChangePrice}
                            disabled={edit.status}
                            value={edit.price} 
                            required
                            fullWidth
                            id="price"
                            label="Precio"
                            name="price"
                            />
                        </Grid>
                        <Grid item xs={6} sm={6}>
                        <FormControl fullWidth>
                              
                                <InputLabel id="demo-simple-select-label">Tipo</InputLabel>
                                <Select
                               onChange={handleChangeType}
                                disabled={edit.status}
                                value={edit.type}
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                label="Tipo"
                                
                                >
                                <MenuItem value={"Mensual"}>Mensual</MenuItem>
                                <MenuItem value={"Semanal"}>Semanal</MenuItem>                         
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">Accion</InputLabel>
                                <Select
                                onChange={handleChangeAction}
                                value={edit.action}
                                disabled={edit.status}
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                label="Tipo"
                                >
                                <MenuItem value={1}>Cambiar precio para nuevos usuarios</MenuItem> 
                                <MenuItem value={2}>Cambiar precio para todos</MenuItem>                      
                                </Select>
                            </FormControl>
                        </Grid>
              </Grid>
                     </Box>
                     <Box sx={{display: 'flex', flexDirection: 'column',  alignItems: 'center', mt:3, mb:3}}>
                            <Button
                               variant={edit.status ? "outlined":"contained"}
                               onClick={handleSubmit}
                            >
                            
                             {edit.nameBtn}
                            </Button>
                        </Box>
                 </CustomMyCard> 
                </Grid>
                    <Grid item  xs={12} md={12} xl={12}>
                        <CustomMyCard tag="Usuarios suscritos en:" value={fetch.name} variantValues="h6" iconboxcolor="success" iconsview={<Icon icon="ph:users-three-light" color="white" width="30" height="30" />}>
                            <UsersSuscriptors />
                        </CustomMyCard> 
                    </Grid>
                </Grid>
            </Box>
    </div>
  );
}

export default Singelproduct;
