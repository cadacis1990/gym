import React from 'react';
import Box from '@mui/material/Box';
import { Icon } from '@iconify/react';
import Typography from '@mui/material/Typography'
import Grid from '@mui/material/Grid'
import CustomMyCard from "../Card/customMyCard.jsx";
import Hours from "../Card/hours/index.jsx";
import { useParams } from "react-router-dom";
import Table from "../Card/tables/singleLocal.jsx";
import LocalOpen from '../Card/locals/hours.jsx' 
import Wait from '../utils/wait.jsx'
import {useDispatch, useSelector} from 'react-redux'
import actionLocals from '../../redux/actions/getLocalData.js' 
import Products from './products.jsx'

const Singellocal = () => {
    const local = useSelector(state=>state.local)
    const dispatch = useDispatch();
    const { id } = useParams();

    if (local.length<1) {
        return(
            <Wait/>
        )
    }
      

   

       var filter= local.filter((value)=>{
           if (value._id == id) {
               return value
           }
       })
           filter = filter[0]
     
       var data = {}
       var total= 0
       for (let x = 0; x < filter.sections.length; x++) {
          total = total + filter.sections[x].users.length    
       }
       data.total = total
       data.active = 0;
       data.pending = 0;
       for (let i = 0; i < filter.sections.length; i++) {
           for (let y = 0; y < filter.sections[i].users.length; y++) {
               
            if (filter.sections[i].users[y].client_status) {
                data.active = data.active + 1
           }
           if (!filter.sections[i].users[y].pay_status) {
              data.pending = data.pending + 1 
           }
        }
         
       }
   
    return (
        <div>
    

         <Box sx={{p:1}}>
            <Grid container spacing={4}>
                <Grid item  xs={12} md={6} xl={6}>
                    <CustomMyCard tag="Nombre del Local" value={filter.name} variantValues="h6" iconboxcolor="info" iconsview={<Icon icon="clarity:house-line" color="white" width="26" height="26" />}>
                     <Box sx={{p:2}}>
                         <Box sx={{display:"flex"}}>
                            <Typography  variant="h6" sx={{fontSize:16, mt:1.5}} color="initial">
                                Total de Clientes /    
                            </Typography>
                            <Typography variant="h4" color="initial">
                                {data.total}
                            </Typography>
                         </Box>  
                         <Box sx={{display:"flex"}}>
                            <Typography  variant="h6"  sx={{fontSize:16, mt:1.5}}  color="initial">
                                Activos /      
                            </Typography>
                            <Typography variant="h4" color="initial">
                                {data.active}
                            </Typography>
                            </Box>  
                         <Box sx={{display:"flex"}}>
                            <Typography variant="h6"  sx={{fontSize:16, mt:1.5}}  color="initial">
                                Pendientes de Pago /     
                            </Typography>
                            <Typography variant="h4" color="initial">
                                   {data.pending}
                            </Typography>
                         </Box>  
                        
                      </Box>
                    </CustomMyCard> 
                
                </Grid>
                <Grid item  xs={12} md={6} xl={6}>
                    <CustomMyCard tag="Apertura" value="Horarios de Apertura" variantValues="h6" iconboxcolor="warning" iconsview={<Icon icon="uit:stopwatch" color="white" width="30" height="30" />}>
                     <Box sx={{p:2, minHeight:158}}>
                        <LocalOpen getPropsHours={filter.hours}  localId={id}/>
                     </Box>
                 </CustomMyCard> 
                </Grid>


                <Grid item xs={12} md={12} xl={12}>
                    <CustomMyCard tag="Seccion" value="Control de Secciones" variantValues="h6" iconboxcolor="primary" iconsview={<Icon icon="healthicons:gym-outline" color="white" width="30" height="30" />}>
                       <Hours getPropsSections={filter.sections}   localId={id}/>
                    </CustomMyCard> 
                </Grid>
                <Grid item xs={12} md={12} xl={12}>
                    <CustomMyCard tag="Products" value="Create your products here" variantValues="h6" iconboxcolor="warning" iconsview={<Icon icon="carbon:shopping-cart-arrow-down" color="white" width="30" height="30" />}>
                        <Products/>
                    </CustomMyCard> 
                </Grid>
                <Grid item  xs={12} md={12} xl={12}>
                 <CustomMyCard tag="Usuarios" value="Control de Usuarios" variantValues="h6" iconboxcolor="success" iconsview={<Icon icon="ph:users-three-light" color="white" width="30" height="30" />}>
                 
                      <Table getPropsTable={filter.sections} id={id}  />
                    
                    </CustomMyCard> 
                </Grid>
            </Grid>
        </Box>
        </div>
    );
}


export default Singellocal;
