import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography'

export default function CircularIndeterminate() {
  return (
    <Container component="main" maxWidth="xs">  
    <Box display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
    <Card   sx={{p:3}}>
    <Box display="flex" justifyContent="center" alignItems="center" sx={{mb:2}}>
      <CircularProgress sx={{color:"blue"}} />
    </Box>
  
    </Card>  
    </Box>
    </Container>
  );
}