//server.js
import express from 'express';
import path from 'path';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser'
import multer from 'multer'
import { v4 as uuidv4 } from 'uuid';
const app = express();
require('../src/dbconnect.js');


//Setting
app.set('port', process.env.PORT || 8000);
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../../../build/uploads/avatar'));
  },
  filename: function (req, file, cb) {
    cb(null, uuidv4() +  path.extname(file.originalname).toLocaleLowerCase()); //Appending .jpg
  }
})

//Midelware
app.use(cors());
app.use(multer({
      storage:storage,
}).single('avatar'));

app.use(bodyParser.json());
app.use(express.json());
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, '../../../build')));

//Routes

app.use('/api/user' ,require('../src/routes/user.routes'));
app.use('/api/suscriptions', require('../src/routes/subscriptions.routes'));
app.use('/api/roles', require('../src/routes/roles.routes'));
app.use('/api/locals', require('../src/routes/locals.routes'));
app.use('/api/sections', require('../src/routes/sections.routes'));
app.use('/api/record', require('../src/routes/record.routes'));
app.use('/api/payments', require('../src/routes/payments.routes'));
app.use('/api/products', require('../src/routes/products.routes'));
app.use('/api/statistics', require('../src/routes/statistics.routes'));

app.use("*",function(req, res) {

  res.sendFile(path.resolve(__dirname, '../../../build', 'index.html'));
}
);

app.listen(app.get('port'), async () => {
  console.log('Server runing on port ' + app.get('port'));
  require('../src/migrations/initial_db');

   })


