const { config } = require('dotenv');
config();
const config_data = {};
config_data.db_url = process.env.DB_URL;
config_data.stripe_pk = process.env.STRIPE_PK;
config_data.salt = process.env.SALT;
config_data.secret_token = process.env.SECRET_TOKEN;
config_data.url_site = process.env.URL_SITE;
config_data.name_app = process.env.NAME_APP;
config_data.fetch_server = process.env.FETCH_SERVER;

config_data.email={
    from:process.env.NAME_APP,
    host:process.env.EMAIL_HOST,
    user:process.env.EMAIL_USER,
    password:process.env.EMAIL_PASSWORD,
    port:process.env.EMAIL_PORT

}
module.exports = config_data;

