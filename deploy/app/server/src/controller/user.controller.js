const userController = {};
const User     = require('../models/user');
const Roles    = require('../models/roles');
const Locals   = require('../models/locals');
const Records  = require('../models/records');
const Sections = require('../models/sections');
const Products = require('../models/products');
const Payments = require('../models/payments');
const { loginJwtToken, emailConfirmToken, decodeToken } = require('../services/token');
const emailServices = require('../services/email');
const bcrypt = require('bcrypt');
const fs = require('fs').promises;
const path = require('path');
const services = require('../services/token');
import config from '../config.js'
import { countDocuments } from '../models/user';
const stripe = require('stripe')(config.stripe_pk);



//crear user tipo cliente.
userController.createUser = async (req, res) => {
    var user_role = await Roles.findOne({ name: 'register' });
        user_role = user_role._id;
    var user_info = req.body.user;
    var product   = req.body.user.product;
    var stripe_info = req.body.stripe.token;
   
    var verify_email= await User.countDocuments({email:user_info.email})
    if (verify_email > 0) {
        res.json({
            status:"error",
            message: "Email alredy exist"
        })
        return
    }
   


    try {
        const customer = await stripe.customers.create({
            name:user_info.firstname + " " + user_info.lastname,
            email:user_info.email,
            source:stripe_info.id,
            address:{
                city:user_info.city,
                country:user_info.city,
                line1:"",
                line2:"",
                state:user_info.state
            }
        });
    
        const charge = await stripe.charges.create({
            amount: parseFloat(user_info.product.price) * 100,
            currency: 'usd',
            customer:customer.id
          });
        console.log(charge);
        var pay_met = new Payments({pay_method:charge.source})
            pay_met = await pay_met.save()
           
        const newuser = new User({ 
            firstname:user_info.firstname, 
            lastname:user_info.lastname, 
            suscriptions:[{product:product._id, section:product.section_id}],
            email:user_info.email, 
            phone:user_info.phone, 
            address_line1:user_info.address_line1, 
            address_line2:user_info.address_line2, 
            confirm: '', 
            avatar:"false", 
            pay_method:pay_met._id, 
            password:user_info.password, 
            roles: [user_role],
            avatar:"", 
            products:product._id,
            sections:product.section_id 
           });
        const confirm = emailConfirmToken(newuser._id);
              newuser.confirm = confirm;
        await newuser.save();
        await Sections.updateOne({_id:product.section_id },{$push:{users:newuser._id}})
        await Products.updateOne({_id:product._id },{$push:{users:newuser._id}})
        res.json({
            status:"ok",
            message:"User " + newuser.email + " has been created " 
        })
    } catch (error) {
        console.log(error);
        res.json({
            status:"error",
            message:error.raw.message 
        })
    }
    
    
    
    return 
    const { fullname, email, phone, address, avatar, paymethod, password, roles, subscriptions } = req.body;
    const newuser = new User({ fullname, email, phone, address, confirm: '', avatar, paymethod, password, roles: user_role,avatar:"", subscriptions });

    try {
        var dbresult = await newuser.save();
        const confirm = emailConfirmToken(dbresult._id);
        await User.updateOne({ _id: dbresult._id }, { confirm: confirm });
        var byEmail = await User.findOne({ _id: dbresult._id });

        byEmail = await emailServices.emailConfirm(byEmail, req);
        //Crear aqui JWT basado en id y responderlo
        res.json({
            status: 'ok',
            message: 'User Created'
        });
    } catch (error) {
        if (error.code == 11000) {
            errorcode = 'Email ' + email + ' Alredy Exist';
           
            res.json({
                status: 'error',
                message: errorcode
            })
            return
        }
        res.json({
            status: 'error',
            message: error
        })
       
        return
    }


}
//crear usuario desde administrador.
userController.createUserByAdmin = async (req, res) => {
    //obtener body por destructuring

    var user = {} 
    if (req.body.firstname ) {
        user.firstname = req.body.firstname
    }else{
        user.firstname = ""
    }
    if (req.body.lastname ) {
        user.lastname = req.body.lastname
    }else{
        user.lastname = ""
    }
    if (req.body.email) {
        user.email = req.body.email
    }else{
        user.email = ""
    }
    if (req.body.phone) {
        user.phone = req.body.phone
    }else{
        user.phone = ""
    }
    if (req.body.address) {
        user.address = req.body.address
    }else{
        user.address = ""
    }
    if (req.body.pay_method == "cash") {
        var payment = await Payments.find()
        console.log(payment);
        for (let index = 0; index < payment.length; index++) {
            if (payment[index].pay_method.last4 == "Cash") {
                user.pay_method =payment[index]._id
                
            }
        }
      
        
    }else{
        user.pay_method =[]
    }

    if (req.body.password) {
        user.password = req.body.password
    }else{
        user.password = ""
    }
    if (req.body.avatar) {
        user.avatar = req.body.avatar
    }else{
        user.avatar = ""
    }
    if (req.body.roles) {
        user.roles = req.body.roles
    }else{
        user.roles = ""
    }
    if (req.body.client_status) {
        user.client_status = req.body.client_status
    }else{
        user.client_status = true
    }
    if (!req.body.roles) {
       var clientRole = await Roles.findOne({name:"register"})
       user.roles = [clientRole._id]
    }else{
        user.roles = [req.body.roles]
    }
    if (req.body.payStatus) {
        user.pay_status = req.body.payStatus
     }else{
        user.pay_status = false
     }

    if (req.body.product) {
        user.products = req.body.product
    }else{
        user.products = []
    }

    if (req.body.section) {
        user.sections = req.body.section
    }else{
        user.sections = []
    }
    user.avatar = ""
    user.fullname = user.firstname + " " + user.lastname;
    

    try {
             
            var findClient = await User.findOne({email:user.email})
            if (findClient) {
                res.json({
                    status:"error",
                    message:"El usuario con este email ya existe"
                })
                return
            }
            var price = await Products.findOne({_id:req.body.product})
                price = price.price * 100
                type  = price.type 
            const record = new Records({local:req.body.local_id, section:req.body.section, product:req.body.product, charge:price, type:type});
         
            user.subscriptions = record
            const newuser = new User(user);
            var dbresult = await newuser.save();
            record.user = newuser._id
            await record.save()

            if (!dbresult) {
                res.json({
                status:"error",
                message:"Imposible crear el usuario en el server"
                })
                return
            }

            const findSection = await Sections.findById(user.sections)
            if (!findSection.status) {
                res.json({
                    status:"error",
                    message:"This schedule is full or blocked by the administrator"
                    })
                    return
            }
            const arraySection = findSection.users
            arraySection.push(newuser._id)
            const updated = await Sections.updateOne({_id:user.sections},{users:arraySection})
            if (!updated) {
                res.json({
                status:"error",
                message:"Imposible asociar el cliente a seccion"
                })
                return
            }

            const findProduct = await Products.findById(req.body.product)
            const arrayUsers = findProduct.users
            arrayUsers.push(newuser._id)
            const updated_ = await Products.updateOne({_id:req.body.product},{users:arrayUsers})
            if (!updated_) {
                res.json({
                status:"error",
                message:"Imposible asociar el cliente a un paquete"
                })
                return
            }
             if (findSection.users.length - 1 >= parseInt(findSection.maxUser)) {
                await Sections.updateOne({_id:user.sections},{status:false})
             }
            res.json({
                status:"ok",
                message:"El cliente " + user.email + "se ha creado correctamente"
            })

    } catch (error) {
        console.log(error);
        res.json({
            status:"error",
            message:"Error al crear el cliente en el servidor"
        })
    }
}
//actualizar usuario y no permitir roles.
userController.updateUsertes = async (req, res) => {
    var id = req.body.id;
    const { fullname, email, phone, address, password} = req.body;
    var update_user = {};
    //craer opjeto para update

  
    if (fullname) {
        update_user.fullname = fullname;
    }
    if (email) {
        var getmyemail = await User.findOne({ _id: id });
            getmyemail = getmyemail.email;
        var findOneMail = await User.countDocuments({ email: email });

        if ((findOneMail > 0) && (email != getmyemail)) {
            error = 'Email ' + email + ' Alredy Exist';
            res.json({
                status: 'error',
                message: error
            })
            return
        }
        if ((findOneMail == 0) && (email != getmyemail)) {
            await User.updateOne({ _id: id }, { email: '' });
            update_user.confirm = emailConfirmToken({ id });
            update_user.email = email;
        }



    }
    if (phone) {
        update_user.phone = phone;
    }
    if (address) {
        update_user.address = address;
    }
  
    if (password) {
        if (lastpass) {
            try {
                var validatePass = await User.findOne({ _id: id });
                validatePass = await bcrypt.compare(lastpass, validatePass.password);

                if (validatePass) {
                    var salt = await bcrypt.genSalt(10);
                    hash = await bcrypt.hash(password, salt);
                    update_user.password = hash;
                  
                } else {
                    res.status(401).json({
                        status: 'error',
                        message: 'Last Password is Wrong '
                    })
                    return

                }
            } catch (error) {
                console.log(error);
            }
        } else {
            res.status(404).json({
                status: 'error',
                message: 'Last Password is Required'
            })
            return
        }

    }

    try {
        var result = await User.updateOne({ _id: id }, update_user);
        res.json({
            status: 'ok',
            message: 'Updated User'
        })
    } catch (error) {
        console.log(error)
    }

}

//actualizar usuario y permitir roles. 
userController.updateUser = async (req, res) => {
    const id = req.body.id;
 
    var update_user = {};
    //craer opjeto para update
    if (req.body.fullname) {
        update_user.fullname = req.body.fullname;
    }
    if (req.body.roles) {
        update_user.roles = req.body.roles;
    }
    if (req.body.email) {
        var getmyemail = await User.findOne({ _id: id });
            getmyemail = getmyemail.email;
        var findOneMail = await User.countDocuments({ email: req.body.email });

        if ((findOneMail > 0) && (req.body.email != getmyemail)) {
            error = 'Email ' + req.body.email + ' Alredy Exist';
            res.json({
                status: 'error',
                message: error
            })
            return
        }
        if ((findOneMail == 0) && (req.body.email != getmyemail)) {
          
            update_user.confirm = emailConfirmToken({ id });
            update_user.email = req.body.email;
        }
    }
    if (req.body.phone) {
        update_user.phone = req.body.phone;
    }
    if (req.body.address) {
        update_user.address = req.body.address;
    }
    if (req.body.pay_method == "Cash") {
        var payment = Payments.findOne({pay_method:req.body.pay_method})
            payment = payment._id
        update_user.pay_method = userActionById;
    }
    if (req.body.password) {
        update_user.password = req.body.password;
    }
    if (req.body.subscriptions) {
        update_user.subscriptions = req.body.subscriptions;
    }

   
    try {
        update_user = await User.updateOne({ _id: id }, update_user);

        res.json({
            status: 'ok',
            message: 'Usuario Actualizado'
        });
    } catch (error) {
        console.log(error);
        res.status(404).json({
            status: 'error',
            message: 'Error al Actualizar el usuario'
        });
    }
}
// responder todos los usuarios si no se pasa el parametro id de lo contrario responder un solo user
userController.getUser = async (req, res) => {


    if (!req.query._id) {
        try {
            var alluser = await User.find().populate("products subscriptions roles sections pay_method").sort({createdAt: -1})
          
            //eliminar hash de los password antes de enviar respuestas
            for (let index = 0; index < alluser.length; index++) {
                alluser[index].password = "";
            }

            //Responder con arreglo de todos los usuarios    
            res.json({
                status: 'ok',
                users: alluser
            });
            return
             } catch (error) {
                console.log(error);
                res.json({
                    status: 'error',
                    message: "Error de proceso en el server"
            });
        }

    }

    try {
        var users = await User.findById({ _id:req.query._id}).populate([

            {
                path:"products roles sections pay_method",
            },
            {
                path:"subscriptions",
                populate:{
                     path:"local section product"
                } 
             }
                
        ]);
        //eliminar hash  password antes de enviar respuestas
        users.avatar = "/uploads/avatar/"+users.avatar
        users.password = "";
        //Responder solo usuario
        res.json({
            status: 'ok',
            users
        });
    } catch (error) {
        res.status(404).json({
            status: 'error',
            message: error.message
        });
        console.log(error);
        console.log('Is not posible get user by Id: ' + req.params.id);
    }


}
// responder todos los usuarios si no se pasa el parametro id de lo contrario responder un solo user
userController.getUserByLocal = async (req, res) => {
      
        try {
            var _id = req.query.id;
            var locals = await Locals.
                              find({_id:_id})
                              .populate({
                                         path : 'sections',
                                         populate : {path : 'users',
                                                     select: "_id email pay_status pay_method createdAt client_status products sections fullname",
                                                     populate:{path:"products sections pay_method"

                                                     }
                                                    }})
                              .sort({createdAt: -1})
            var users = []                  
            locals.map((value, key)=>{
                value.sections.map((value, key)=>{
                    value.users.map((value, key)=>{
                        users.push(value)
                    })
                })
             })
             //ordenar
           
             users = users.sort(function (a, b) {
                if (a.createdAt > b.createdAt) {
                  return -1;
                }
                if (a.createdAt < b.createdAt) {
                  return 1;
                }
                // a must be equal to b
                return 0;
              });
            //eliminar hash de los password antes de enviar respuestas
            res.json({
                status:"ok",
                users

            })
            return
            for (let index = 0; index < users.length; index++) {
        
               locals[index].sections.users.password = "";

            }

            //Responder con arreglo de todos los usuarios    
            res.json({
                status: 'ok',
                users: us
            });
            return
             } catch (error) {
                console.log(error);
                res.json({
                    status: 'error',
                    message: "Error de proceso en el server"
            });
        }

  



}
userController.getStatus = async (req, res) => {
  try {
    if (req.headers.jwt == "undefined") {

        res.json({
            status:"error",
            message:"Invalid Token",
            user:{
                roles:[{name:"client"}]},
        })
        return
   }
    var data = services.decodeToken(req.headers.jwt)
    if (!data.id) {
        res.json({
            status:"error",
            message:"Invalid Token",
            user:{
                roles:[{name:"client"}]},
        })
        return
    }
    
    var user = await User.findById({ _id:data.id}).populate("products roles sections pay_method");
    //eliminar hash  password antes de enviar respuestas
 
    if (!user) {
        res.json({
            status:"ok",
            user:{
                roles:[{name:"client"}]},
        })
        return
    }

    user.avatar = "/uploads/avatar/"+user.avatar
    user.password = "";
    
        res.json({
            status:"ok",
            user:user,
            
        })
      
  } catch (error) {
    res.json({
        status:"error",
        message:"Server Error",
        user:{
            roles:[{name:"client"}]},
    })
    console.log(error);
    return
    
  }
   

}
userController.getUserBySections = async (req, res) => {

    try {
        var section = await Sections.findOne({ _id: req.body.sections_id }).populate('users');
        var usersarr = [];
        var start = new Date();
        start.setHours(0, 0, 0, 0);
        var end = new Date();
        end.setHours(23, 59, 59, 999);

        for (let i = 0; i < section.users.length; i++) {

           var obj = {};
           var result = await User.findOne({ _id: section.users[i]._id }).select('fullname').select('avatar').select('email').select('attendance');
           check_record = await Record.findOne({ user: section.users[i]._id, section: section._id, createdAt: { "$gte": start, "$lt": end } });

            if (check_record) {
                result.attendance = 1;

            } else {
                result.attendance = 0;

            }

            usersarr.push(result);
        }
        res.json({
            status: 'ok',
            users: usersarr
        })
    } catch (error) {
        console.log(error);
    }
}
//delete user
userController.deleteUser = async (req, res, id) => {

    try {
       
        var _id = req.query._id
        var count = await User.countDocuments({ _id:_id });
        var user = await User.findById(_id);
     
      
        if (count <= 0) {
            res.json({
                status: 'error',
                message: 'No se encuentra el usuario'
            })
            return
        }

        if (user.products.length>0) {
            res.json({
                status: 'error',
                message: 'El usuario esta activo en una suscripcion debe cancelarla antes'
            })
            return
        }

            await Sections.updateMany({},{$pull:{users:_id}})
            await Products.updateMany({},{$pull:{users:_id}})
            await User.deleteOne({ _id:_id });
            res.json({
                status: 'ok',
                message: 'Usuario Eliminado'
            })

    } catch (error) {
        console.log('Delete User is not posible this is the error: ' + error);
        res.json({
            status: 'error',
            message: 'Is not posible deleted user by Id: ' + _id
        })
    }

}
userController.userStatusSwitch = async (req, res) => {
    var _id = req.query._id; 
    
    var user = await User.findById(_id);
    var status = user.client_status
  
    if (!user) {
        res.json({
            status: 'error',
            message: 'Este usuario puede haber sido eliminado'
        })
        return
    }
    await User.updateOne({_id:_id},{client_status:!status})
    res.json({
        status: 'ok',
        message: status ? "El usuario "+user.fullname+" termino su entrenamiento" : "El usuario "+user.fullname+" comenzo su entrenamiento"
    })
}
userController.userPaySwitch = async (req, res) => {
    var _id = req.query._id; 
    
    var user = await User.findById(_id);
    var pay_status = user.pay_status;

    if (!user) {
        res.json({
            status: 'error',
            message: 'Este usuario puede haber sido eliminado'
        })
        return
    }
    await User.updateOne({_id:_id},{pay_status:!pay_status})
    res.json({
        status: 'ok',
        message: pay_status ? "El usuario "+user.fullname+" ahora esta pendiente de pago" : "El usuario "+user.fullname+" ahora esta como pagado"
    })
}
userController.userRemoveSuscriptions = async (req, res) => {
    console.log(req.query);
    if (!req.query.id || !req.query.product) {
        res.json({
            status:"error",
            message:"Id no existe"
        })
    return
    }
    var user = req.query.user
    var suscription = req.query.product
    var idRecord = req.query.id
    console.log(req.query);
    try {
        
        await Records.deleteOne({_id:idRecord})
        
        await Products.findOneAndUpdate({_id:suscription},{$pull:{users:user}})
        await User.findOneAndUpdate({_id:user},{$pull:{products:suscription}})
        await User.findOneAndUpdate({_id:user},{$pull:{subscriptions:idRecord}})
        
        var resultdos = await User.updateOne({_id:user},{$pull:{products:user}})
        res.json({
            status:"ok",
            message:"Suscripcion Eliminada "
        })

    } catch (error) {
        res.json({
            status:"error",
            message:"Error en el server"
        })
    }
   
}
userController.userUploadFile = async (req, res) => {

    var id = req.body.id
    var file=req.file
    console.log(file);
           try {

            var query = await User.findOne({ _id: id });
            var lastavatar = query.avatar;
           
            if (lastavatar != "false") {
                var result =  await User.updateOne({_id:id},{avatar:req.file.filename})
                res.json({
                     status:"ok",
                     message:"Avatar actualizado con exito"
                 })
                 await fs.unlink(path.join(__dirname, '../../../../build/uploads/avatar/' + lastavatar));
         
            }
          
           } catch (error) {
             console.log(error);  
            res.json({
                status:"error",
                message:"Error al actualizar el avatar"
            })
           }
          

        }
userController.deletePayment = async (req, res) => {
   
   if (!req.query._id || !req.query.user_id) {
    res.json({
        status:"error",
        message:"Parametros Incorrectos"
    })   
    return
   }
   
   var user_id = req.query.user_id;
   var method  = req.query._id;
   var user = await User.findOne({_id:user_id});

   if (!user) {
    res.json({
        status:"error",
        message:"El usuario ya no existe"
    })   
    return
   }
   //Eliminar de stripe esperar respuesta y luego eliminar de la db

   var delPayment = await User.updateOne({_id:user_id},{$pull:{pay_method:method}});

   if (delPayment.modifiedCount < 1) {
        res.json({
            status:"error",
            message:"Imposible actualizar en el server"
        })   
        return
   }
        res.json({
            status:"ok",
            message:"Metodo de Pago Eliminado"
            })  
        }

userController.addProduct = async (req, res) => {
    const {section, local, product, id} = req.body
    try {
        var section_db = await Sections.findOne({_id:section})
        var user_db =await User.findOne({_id:id})
        
        if (section_db.users.length >= section_db.maxUser) {
            res.json({
                status:"error",
                message:"This training hour is full"
            })
          return
        }
        if (user_db.pay_method.length < 1) {
            res.json({
                status:"error",
                message:"You need at least one payment method"
            })
          return
        }
     
        const countRecordSimilar = await Records.countDocuments({user:id, product:product, section:section})
        if (countRecordSimilar > 0) {
            res.json({
                status:"error",
                message:"ya tiene una suscripcion con el mismo producto en la misma seccion"
            })
          return
        }
        var price = await Products.findOne({_id:req.body.product})
            price = price.price * 100
            type  = price.type 
        const record = new Records({user:id, local:local, product:product, section:section, charge:price, type:type})
        await record.save();

        await User.updateOne({_id:id},{$push:{sections:section, products:product, subscriptions:record._id}})
        await Sections.updateOne({_id:id},{$push:{users:id}})
        await Products.updateOne({_id:product},{$push:{users:id}})
        res.json({
            status:"ok",
            message:"Section added successfully"
        })

    } catch (error) {
        res.json({
            status:"error",
            message:"Server Error"
        })
    }
 
 }      
module.exports = userController;