const productsController = {};
import config from "../config";
import Products from "../models/products";
import Locals from "../models/locals";

productsController.createProduct = async (req, res) => {
    console.log(req.body);
    var product = {}
  
    if (req.body.name) {
        product.name=req.body.name
    }
    if (req.body.type) {
        product.type=req.body.type
    }
    if (req.body.descriptions) {
        product.descriptions=req.body.descriptions
    }
    if (req.body.price) {
        product.price=req.body.price
    }
    
    try {
        var newproduct = new Products(product);
        await newproduct.save();
        await Products.updateOne({_id:newproduct._id},{$push:{locals:req.body.local_id}})
        await Locals.updateOne({_id:req.body.local_id},{$push:{products:newproduct._id}})
        res.json({
            status:"ok",
            message:"Paquete creado correctamente"
        })
    } catch (error) {
        console.log(error);
        res.json({
            status:"error",
            message:"Error de Server no se ha creado el paqute"
        })
    }
   
} 
productsController.getProduct = async (req, res) => {
   try {
      const result = await Products.find().populate({
        path:"users",
        select:"client_status createdAt email fullname pay_method pay_status _id avatar",
        populate:{
            path:"pay_method"
        }
    })
       if (!result) {
        res.json({
           status:"error",
           message:"No se encontraron productos" 
        })
        return
      }
      res.json({
        status:"ok",
        data:result
     })
   } catch (error) {
    res.json({
        status:"error",
        message:"Error en el Server" 
     })
   }
}
productsController.removeProduct = async (req, res) => {
    try {
        var findproducts = await Products.findOne({_id:req.params.id});
        if (!findproducts) {
            res.json({
                status: 'error',
                message: 'Producto no Encontrado'
            })
            return
        }

       var result = await Products.deleteOne({ _id:req.params.id })
      
        res.json({
            status: 'ok',
            message: 'Producto Borrado'
        })
        return
    } catch (error) {
        console.log(error);
        res.status(200).json({
            status: 'error',
            message: 'Error de Servidor'
        })
    }
}
productsController.getProductById = async (req, res)=>{
    if (!req.query._id) {
        res.json({
            status:"error",
            message:"Id de producto incorrecto"
        })
        return
    }
    var _id = req.query._id
    var findProducts = await Products
                             .findOne({_id:_id})
                             .populate({
                                        path:"users",
                                        select:"client_status createdAt email fullname pay_method pay_status _id avatar"
                                    })
    if (!findProducts) {
        res.json({
            status:"error",
            message:"Id de producto no existe"
        })
        return
    }
    res.json({
        status:"ok",
        products:findProducts
    })
}
productsController.updateProductById = async (req, res)=>{
    var id = req.body.id
    var product={}
    if (req.body.name) {
        product.name=req.body.name
    }
    if (req.body.price) {
        product.price=req.body.price
    }
    if (req.body.type) {
        product.type=req.body.type
    }
  

   try {
       var result = await Products.updateOne({_id:id},product)
       if (result.modifiedCount < 1) {
        res.json({
            status:"error",
            message:"No se ha actualizado el paquete"
        })
        return 
       }
       res.json({
           status:"ok",
           message:"El paquete ha sido acxtualizado"
       })
   } catch (error) {
    res.json({
        status:"error",
        message:"Error en el server al actualizar el paquete"
    })
   }
}

module.exports = productsController;