const authController = {};
const User = require('../models/user');
const Roles = require('../models/roles');
const config = require('../config');
const { loginJwtToken, emailConfirmToken, decodeToken } = require('../services/token');
const emailServices = require('../services/email');
const bcrypt = require('bcrypt');

//confirmar usuario despues de registro
authController.confirmUser = async (req, res) => {
    var { token } = req.params;
    var resultId = decodeToken(token);
    result = await User.countDocuments({ _id: resultId.id });
    if (result == 0) {
        res.status(404).json({
            status: 'error',
            message: 'Confirmation link invalid or has expired'
        })
    } else {
        var updateResult = await User.updateOne({ _id: resultId.id }, { confirm: 'confirmed' });
        var jwt = loginJwtToken(resultId.id);

        res.json({
            status: 'ok',
            jwt: jwt,
            message: 'User Confirmed'
        })
    }
}
//recuperar usuario (enviar email)
authController.userRecovery = async (req, res) => {
    var user = req.body.email;
    user = await User.findOne({ email: user });
    if (user) {
        var jwt = await emailConfirmToken(user._id);
        var updateRecovery = await User.updateOne({ _id: user._id }, { recovery: jwt });
        user = await User.findOne({ _id: user._id });
        emailServices.emailRecovery(user, req);
        res.json({
            status: 'ok',
            message: 'Sending Email Recovery'
        })
    } else {
        res.status(404).json({
            status: 'error',
            message: 'User not Found'
        })
    }

}
//recuperar usuario (mediante url ya enviada )
authController.chagePassRecovery = async (req, res) => {

    try {
        var { lastpass } = req.body;
        var { newpass } = req.body;
        var { token } = req.body;
        var resultId = decodeToken(token);

        var result = await User.countDocuments({ _id: resultId.id });
        var result1 = await User.countDocuments({ recovery: token });
        if (result == 0 || result1 == 0) {
            res.status(404).json({
                status: 'error',
                message: 'Unauthorized Token'
            })
        } else {

            var user = await User.findOne({ _id: resultId.id });
            var validatePass = await bcrypt.compare(lastpass, user.password);

            if (validatePass) {
                await User.updateOne({ _id: resultId.id }, { password: newpass, recovery: '' });

                res.json({
                    status: 'ok',
                    message: 'Updated Password'
                })
            } else {
                res.status(401).json({
                    status: 'error',
                    message: 'wrong current password'
                })
            }
        }
    } catch (error) {
        console.log(error)
    }


}

authController.login = async (req, res) => {
    var { email } = req.body;
    var { password } = req.body;
    var findUser = await User.findOne({ email:email });
    
    // Si no encuentra el usuario responde user no encontrado  --- si lo encuentra pasa a la siguiente condicion.  
    if (!findUser) {
        res.status(200).json({
            status: 'error',
            message: 'User not Found'
        })
    } else {
        //si el pass no es correcto responde pass incorrecto sino pasa a la otra condicion   
        var validatePass = await bcrypt.compare(password, findUser.password);
        if (!validatePass) {
            res.status(200).json({
                status: 'error',
                message: 'Wrong Password'
            })
        } else {
            //si el usuario no ha sido confirmado pasa a otra condicion de lo contrario devuelve ok y jwt
            if (findUser.confirm != 'confirmed') {

                var user = await User.findOne({ _id: findUser._id });
                var url = 'http://' + config.url_site + '/user/resendmail/' + user._id;
                var token = await decodeToken(findUser.confirm);
                //si el token es existe pero es invalido crea un nuevo token y envia un nuevo email de confirmacion y responde  nuevo email enviado confirme si no 


                res.status(200).json({
                    status: 'error',
                    urlResendMail: url,
                    message: 'User not Confirmed'
                })


            } else {

                var jwt = await loginJwtToken(findUser._id);
                res.json({
                    status: 'ok',
                    jwt: jwt,
                    userId:findUser._id,
                    message: 'User Logged'
                }) 
            }

        }
    }
}

authController.resendMail = async (req, res) => {
    try {
        var user = req.params.user;
        user = await User.findOne({ _id: user });
    } catch (error) {
        res.status(401).json({
            status: 'error',
            message: 'forbidden'
        });
        return
    }

    if (user.confirm == "confirmed") {
        res.status(404).json({
            status: 'error',
            message: 'This user is confirmed'
        })
    } else {
        var confirm = await emailConfirmToken(user._id);
        await User.updateOne({ _id: user._id }, { confirm: confirm });
        user = await User.findOne({ _id: user._id });
        emailServices.emailConfirm(user, req);
        res.json({
            status: 'ok',
            message: 'Sending Email Confirmation'
        })
    }



    var token = await emailConfirmToken(user._id);
}
module.exports = authController;