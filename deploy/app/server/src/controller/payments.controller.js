const paymentsController = {};
import Payments from "../models/payments";
import Users from "../models/user";

paymentsController.registerPaymentCash = async (req, res) => {

    transaction = new Payments({user_id:req.body.user_id, method:'cash'});
    try {
        transaction.save();
        res.json({
            status:'ok',
            message:'Registered cash payment',
        })
    } catch (error) {
        console.log(error);

    }

 
} 

paymentsController.getPayments = async (req, res) => {
    try {
        var query = {};
        if (req.body.user_id) {
            query.user_id = req.body.user_id
        }
        if (req.body.method) {
            query.method = req.body.method
        }
        var payments = await Payments.find(query);
        res.json({
            status:'ok',
            paymentsLog:payments
        })
    } catch (error) {
        res.json({
            status: 'error',
            message:"No se pudieron obtener los usuarios"
        })
        
        console.log(error);
        
    }
}



paymentsController.getPaymentsById = async (req, res) => {
    try {
        var query = {};
            query.user_id = req.body.user_id
        var payments = await Payments.find(query).sort({createdAt:-1});

        res.json({
            status:'ok',
            paymentsLog:payments
        })
    } catch (error) {
        console.log(error);
        
    }
}
module.exports = paymentsController;