import { Schema, model } from "mongoose";

const products = new Schema({

    name:{},
    descriptions:{},
    price:{},
    type:{},
    users: [{
        type: Schema.Types.ObjectId,
        ref: 'Users'
    }],
    locals: [{
        type: Schema.Types.ObjectId,
        ref: 'Locals'
    }]


}, {
        timestamps: true,
        versionKey: false
    });

module.exports = model('Products', products);