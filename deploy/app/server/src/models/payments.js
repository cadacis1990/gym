
import {Schema, model} from "mongoose";
const payments = new Schema({

status:{
        type:Boolean,
        default:true
},
message:{
        type:String,
        message:"Active"
},
pay_method:{}

}, {
        timestamps: true,
        versionKey: false
});

module.exports = model('Payments', payments);