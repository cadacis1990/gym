 
// modelo para local
const {Schema, model} = require('mongoose');

const Sections  = new Schema({
    weekDay:{},
    maxUser:{},
    noUser:{},
    start_time:{type:Date},
    status:{},
    end_time:{type:Date},
    locals: {
        type : Schema.Types.ObjectId,
        ref: 'Locals'
    },
    users: [{
        type : Schema.Types.ObjectId,
        ref: 'Users'
    }],
},{
    timestamps:true,
    versionKey:false
});

module.exports = model('Sections', Sections );