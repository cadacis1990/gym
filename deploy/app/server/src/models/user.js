
// modelo para user
const { Schema, model } = require('mongoose');
const bcrypt = require('bcrypt');
const { emailConfirmToken } = require('../services/token');
const Users = new Schema({
    firstname:{},
    lastname:{},
    fullname: {},
    notify:{
        type:String,
        default:""
    },
    subscriptions:[{
        type: Schema.Types.ObjectId,
        ref: 'Records'
    }],
    email: {
        type: String,
        unique: true,
        index: true
    },
    confirm: {},
    recovery: {},
    phone: {},
    attendance: {},
    address_line1: {},
    address_line2: {},
    avatar: {},
    pay_method:[{
        type: Schema.Types.ObjectId,
        ref: 'Payments'
    }],
    pay_status:{},
    client_status:{},
    password: {},
    lastlogin: {},
    loged: {},
    status: {},

    roles: [{
        type: Schema.Types.ObjectId,
        ref: 'Roles'
    }],

    products: [{
        type: Schema.Types.ObjectId,
        ref: 'Products'
    }],

    local: {},

    sections: [{
        type: Schema.Types.ObjectId,
        ref: 'Sections'
    }],

},{
        timestamps: true,
        versionKey: false
});
//encriptar password en el save
Users.pre('save', async function save(next) {
    if (!this.isModified('password')) return next();
    try {
        var salt_ = await bcrypt.genSalt(10);
        this.password = await bcrypt.hash(this.password, salt_);
        return next();
    } catch (err) {
        return next(err);
    }
});

Users.pre('updateOne', async function save(next) {
    const data = this.getUpdate()
    if (!data.password) return next();

    try {
        var salt_ = await bcrypt.genSalt(10);
        data.password = await bcrypt.hash(data.password, salt_);
        this.update({}, data)
        next()
    } catch (err) {
        return next(err);
    }
});

module.exports = model('Users', Users);