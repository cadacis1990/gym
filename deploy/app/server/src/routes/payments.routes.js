import express from "express";
const router = express.Router();

const {registerPaymentCash, getPayments, getPaymentsById} = require('../controller/payments.controller');


router.post('/cash',  (req, res) => {
   registerPaymentCash(req, res);
});

router.get('/',  (req, res) => {
  getPayments(req, res);
});
router.get('/byid',  (req, res) => {
    getPaymentsById(req, res);
});

router.put('/',  (req, res) => {
   
});

router.delete('/',  (req, res) => {
   
});
module.exports = router;