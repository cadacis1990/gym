const express = require('express');
const router = express.Router();
const {createRecord} = require('../controller/record.controller');


router.post('/',  (req, res) => {
  createRecord(req, res);
});

module.exports = router;