import express from "express";
const router = express.Router();
import { createUser, 
        createUserByAdmin, 
        getUser, 
        deleteUser, 
        updateUser,
        getUserBySections, 
        getUserByLocal, 
        getStatus, 
        userStatusSwitch,
        userPaySwitch,
        userRemoveSuscriptions,
        userUploadFile,
        deletePayment,
        addProduct
      } from "../controller/user.controller";
import { confirmUser, 
         userRecovery,
         chagePassRecovery, 
         login, 
         resendMail} from "../controller/auth.controller";

import { body, validationResult } from "express-validator";
import validate from "../validate/user";

const stripe = require('stripe')('sk_test_51IDcSKBg5CrIbIRKbqqwTCDD763sXz5hHKAR4iGTPy1m1qxmqiUtz5TgqRg7Su3saOSDDOpvP1Pps62PKFJMdWc6006OYkei5u');

//obtener usuarios
router.get('/verify', (req, res) => {

  getStatus(req, res);
});

router.put('/addproduct', (req, res) => {
    addProduct(req,res)
})
//obtener usuarios
router.get('/all/', (req, res) => {
    getUser(req, res);
});
//obtener usuarios por id
router.get('/profile/', (req, res) => {
    getUser(req, res);
});
router.get('/confirm/:token', (req, res) => {
    confirmUser(req, res);
});
router.get('/resendmail/:user', (req, res) => {
    resendMail(req, res);
});
router.get('/getbysections/',(req, res) => {
  getUserBySections(req, res);
})
router.get('/getbylocal/',(req, res) => {
  
  getUserByLocal(req, res);
})
router.post('/login', (req, res) => {
   
  /*   const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    } */
   login(req, res);
});
router.post('/recovery',validate.recoveryPass, (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    userRecovery(req, res);
});
router.post('/changepassrecovery', (req, res) => {
    chagePassRecovery(req, res);
});
//crara usuarios
router.post('/', (req, res) => {
    createUser(req, res);
});
//crara usuarios by administrador
router.post('/userbyadmin', (req, res) => {
    createUserByAdmin(req, res);
});
//actualizar usuarios
router.put('/', async (req, res) => {
  updateUser(req, res);
});

router.get("/statususer", async(req, res) => {
  userStatusSwitch(req, res)
})

router.get("/statuspay", async(req, res) => {
  userPaySwitch(req, res)
})

//eliminar usuarios
router.delete('/', (req, res) => {
    deleteUser(req, res);
});

router.delete('/suscriptions', (req, res) => {
  userRemoveSuscriptions(req, res);
});

router.post('/upload',(req, res)=>{
  userUploadFile(req, res)
});


router.delete('/deletepayment', async (req, res) => {
  deletePayment(req, res);
});

module.exports = router;