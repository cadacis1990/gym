const express = require('express');
const router = express.Router();
import {createProduct, getProduct, removeProduct, getProductById, updateProductById} from '../controller/products.controller'

router.post('/', (req, res) => {
  createProduct(req, res)
});

router.get('/', (req, res) => {
  getProduct(req, res)
});
router.get('/byid', (req, res) => {
  getProductById(req, res)
});
router.put('/byid', (req, res) => {
  updateProductById(req, res)
});
router.delete('/:id', (req, res) => {

  removeProduct(req, res)
});
module.exports = router;

