const jwt = require('jsonwebtoken');
const config = require('../config');
var services = {};
//crear token
services.loginJwtToken = (id) => {
    var jwt_ = jwt.sign({ id: id }, config.secret_token, {
        expiresIn: 60 * 60 * 24
    });
    return jwt_;
};

//crear token
services.emailConfirmToken = (id) => {
    var jwt_ = jwt.sign({ id: id }, config.secret_token, {
        expiresIn: 60 * 60 
    });
    return jwt_;
};
//decode token
services.decodeToken = (token) => {
    try {
        var jwt_ = jwt.verify(token, config.secret_token);
        return jwt_;
    } catch (error) {
        return 0;
    }
   
}

module.exports = services;