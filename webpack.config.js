const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin").default;

const config = {
  devtool: "source-map",
  entry: {
    app: path.resolve(__dirname, "app/client/public/index.js"),
  },
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "[name].bundled.js",
    publicPath: "/",
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./app/client/public/index.html",
      favicon: "./app/client/public/favicon.ico",
    }),
    new MiniCssExtractPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          /*  options:{
              presets:["@babel/preset-env", "@babel/preset-react"]
            } */
        },
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: "file-loader",
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        loader: "svg-react-loader",
      },
    ],
  },

  devServer: {
    //publicPath: "/",
    //contentBase: "/app",
    hot: true,
    //overlay: true,
    port: 4000,
    //inline: true,
    //progress:true,
    open: "http://localhost:4000",
  },
};

module.exports = config;
